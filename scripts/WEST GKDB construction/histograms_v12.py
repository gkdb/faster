#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 26 13:22:42 2023

@author: EV273809
"""


#This version is the final version: I have to:
# add a save version
# add the possibility to compute Fevre 
# add commentary in english
# correct the definition of variable + give correct name for variable with a specific denomination
# fix the x  = r/a pb and add the r/R hist

# COMMENTAIRE TEMPORAIRES:  je dois comparer les résultats avec la v10 sans Fèvre pour voir si les fonctions
#erase_nan donnent les memes resultats


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox, Button
from matplotlib.animation import FuncAnimation
import scipy.interpolate as sp
#useful for save data  
import json as js
#useful for the matlab code (Fevre)
import subprocess
import io
import os
#useful for imas
import imas_west as iw
import statistics_west as sw


class Histograms_():
    def __init__(self, campaign_name = 'C4', rayons_norm = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], name_json_file = None, nbr_input_param = 1):
        self.campaign_name = campaign_name
        self.dico_mean_std = {}
        self.dico_all_data = {} #contient {shot: shot_data}
        self.nombre_plateaus_total = 0
        self.rayons_norm = rayons_norm
        self.nbr_rayon_utile = len(self.rayons_norm)
        self.nbr_input_param = nbr_input_param #s'adapte plus tard
        self.plateau_problematic = {}
        self.tableau_erreur_fevre = []
        self.erreur_fevre = False
        self.nbr_plateau_fevre = 0
        self.plateau_number = 0
        self.plateau_number_init = 0
        self.numeros_plateaus_utiles_fevre = []
        

        self.shot_Te_pb = []

        self.nbr_shot = 1

        self.ind_line_hist = 0
        self.ind_col_hist = 0
        self.line_hist, self.col_hist = 0, 0 #s'adapte plus tard
        self.fig, self.axs = None, None


        self.color_list = ['red', 'blue', 'green', 'yellow', 'orange', 'purple', 'pink', 'brown', 'gray','cyan', 'magenta', 'gold', 'silver', 'indigo', 'violet', 'teal', 'olive', 'maroon']
        self.bins_hist = 250 #précision des barres des hist

        #initialisation des valeurs physiques:
        self.me = 9.109382e-31 # en kg
        self.qe = -1.602176e-19 # en C
        self.kb = 1.380649e-23 # en m2 kg s-2 K-1
        self.mu_zero = 12.56637e-7 #kg m A-2 s-2

        #bound QLKNN core: [min, max]
        rlte_qlknn_core = [0, 14]
        rlti_qlknn_core = [0, 14]
        rlne_qlknn_core = [-5, 6]
        q_qlknn_core = [0.66, 15]
        magn_shear_qlknn_core = [-1, 5]
        r_on_R_qlknn_core =  [0.03, 0.33]
        Ti_on_Te_qlknn_core = [0.25, 2.25]
        collisionality_qlknn_core = [np.log(1e-5), np.log(1)]
        z_eff_qlknn_core = [1, 3]
        #
        triangularity_up_core = [0, 1]
        triangularity_low_core = [0, 1]
        beta_norm_core = [0]

        r_on_R_core = [0.03, 0.33]

        self.core_bound_dico = {"0" : rlte_qlknn_core, "2" : rlti_qlknn_core, "1": rlne_qlknn_core, "3":Ti_on_Te_qlknn_core,
                                "4": q_qlknn_core, "5": magn_shear_qlknn_core, "10": z_eff_qlknn_core, "11": collisionality_qlknn_core,
                                 "7": triangularity_up_core, "8": triangularity_low_core, "9": beta_norm_core,
                                 "14" : rlne_qlknn_core, "15" : beta_norm_core, "13" : r_on_R_core }


        #bound QLKNN edge:
        rlte_qlknn_edge = [5, 150]
        rlti_qlknn_edge = [5, 150]
        rlne_qlknn_edge = [2, 120]
        q_qlknn_edge = [2, 30]
        magn_shear_qlknn_edge = [1, 40]
        r_on_R_qlknn_edge =  [0.95]
        Ti_on_Te_qlknn_edge = [1]
        collisionality_qlknn_edge = [np.log(1e-1), np.log(3)]
        z_eff_qlknn_edge = [1, 2.5]
        #
        triangularity_up_edge = [0, 1]
        triangularity_low_edge = [0, 1]
        beta_norm_edge = [0]
        r_on_R_edge = [0.95]

        self.edge_bound_dico = {"0" : rlte_qlknn_edge, "1" : rlti_qlknn_edge, "2": rlne_qlknn_edge, "3":Ti_on_Te_qlknn_edge,
                                "4": q_qlknn_edge, "5": magn_shear_qlknn_edge, "10": z_eff_qlknn_edge, "11": collisionality_qlknn_edge,
                                 "7": triangularity_up_edge, "8": triangularity_low_edge, "9": beta_norm_edge,
                                 "14" : rlne_qlknn_edge, "15" : beta_norm_edge, "13" : r_on_R_edge }

        
    #give the shot plateau and begin/end plateau + t_igni
    def initialisation_stat(self):  
        #On récupère tous les plateaus de ce shot
        stats = sw.read_stats(self.campaign_name)
        

        #On filtre les datas comme recommandé sur le west portal user
        statsf = stats[(stats['eq_q_95_mean_plto'] < 20)
                    & (stats['ne3_mean_plto'] > 0)
                    & (stats['P_COND_mean_plto'] > 0)
                    & (stats['eq_w_mhd_mean_plto'] > 0)
                    & ((stats['contr_nelMax_mean_plto'] / stats['neVol_mean_plto']) < 0.2) # Remove large errors in W_mhd computation
                    & ((stats['shot'] < 55625) | (stats['shot'] > 55643)) # Remove ECE wrong pulses because thunderstorm
                    & ((stats['shot'] < 56876) | (stats['shot'] > 56891))] # Remove shots without DCN laser available (interferometry)
                    # & (stats['H_98y2_mean_plto'] < 10) \
                    # & (stats['contr_bpolMax_mean_plto'] / (1E-6*stats['Ip_mean_plto']) < 0.03) # Remove large Bpol probe errors
                    # & (stats['contr_faMax_mean_plto'] != 0)] # Remove shots without polarimetry

        # Helium shots:
        helium_shots = [(55230, 55498), (55827, 55987)]

        # Remove helium pulses:
        for iishot in helium_shots:
            statsf = statsf[(stats['shot'] < iishot[0]) | (stats['shot'] > iishot[1])]

        #On va stocker tous les shot dans une liste
        self.shot_list = np.array((statsf['shot']))
        self.shot_list = np.unique(self.shot_list)
        self.shot_list = self.shot_list.astype(int)

        #On stocke les temps ini et fin des plateaux de tous les shots
        self.time_ini =statsf['tIni_plto']
        self.time_fin =statsf['tEnd_plto']
        self.time_igni = statsf['t_ignitron']
        self.plateau = statsf['plateau']

    
    #####_______________________________________________#####
    #####______________Utilitary functions______________#####
    #####_______________________________________________#####


    # fonction qui renvoit les shots ayant une certaine valeur pour un certain paramètres adimensionnel
    def quel_shot(self, value, input_param, dico_mean_std):
        dico_value_corresponding = {}
        for key, mean_std in dico_mean_std.items(): #à faire: remplacer key par shot pour une meilleure lecture
            
            i=-1
            for rayons in mean_std[input_param]:
                i+=1
                mean = rayons[0]
                num_plateau = 0
                for plateau in mean:
                    num_plateau+=1
                    if plateau <= value:
                        texte = "rayon:" + str(i) + ";plateau:" + str(num_plateau) + "; ;"
                        if key in dico_value_corresponding:
                            val_existante = dico_value_corresponding[key]
                            new_text = val_existante + texte
                        else:
                            new_text = texte

                        dico_value_corresponding[key] = new_text
        print(dico_value_corresponding)

    # fonction utile pour automatiquement définir le nombre de colonnes et de lignes de graphe affiché
    def decoupage_ligne_colonne(self, dimension):
        racine_carree = int(dimension**0.5)
        facteur = racine_carree
        colonne = int(dimension / racine_carree)
        if dimension % facteur != 0:
            colonne += 1

        return facteur, colonne
        
        # if dimension % 2 != 0:
        #     dimension += 1
        # while dimension % facteur != 0:
        #     facteur -= 1
        # lignes = facteur
        # colonnes = dimension//facteur

        # return lignes, colonnes

    # fonction qui doit retourner les indices des plateaus correspondant à un shot en pour un vecteur time d'un ids
    def indicePlateaus(self, shot_number, time): #donne les indices correspondants aux plateaux d'un vecteur time pour un shot donné
        self.plateau_problematic = {}
        tigni =  self.t_igni
        t0 = time[0]
        real_time = []
        ecart = abs(tigni-t0)
        for el in time:
            #real_time += [round(el + ecart, 2)]
            real_time += [round(el, 2)] #pas sûr de l'écart
        
        ind_ini_plt_core_profile = []
        ind_fin_plt_core_profile = []
        
        self.numeros_plateaus_utiles_fevre = []
        
        for plat in range(self.plateau_number):
            try:
                ind_ini = self.get_closest_index(self.t_ini[plat],real_time)
                ind_fin = self.get_closest_index(self.t_fin[plat],real_time)
                if ind_ini == ind_fin: #cas où on est en dehors du vecteur temporel
                    if self.plateau_problematic.get(str(shot_number)) is not None:
                        self.plateau_problematic[str(shot_number)] += [str(plat)]
                    else:
                        self.plateau_problematic.update({str(shot_number) : [str(plat)]})
                    self.plateau_number -= 1 #On supprime un plateau
                else:
                    ind_ini_plt_core_profile += [ind_ini]
                    ind_fin_plt_core_profile += [ind_fin]
                    self.numeros_plateaus_utiles_fevre += [plat]
            except:
                if self.plateau_problematic.get(str(shot_number)) is not None:
                    self.plateau_problematic[str(shot_number)] += [str(plat)]
                    self.plateau_number -= 1 #On supprime un plateau
                else:
                    self.plateau_problematic.update({str(shot_number) : [str(plat)]})
                    self.plateau_number -= 1 #On supprime un plateau
        #Indices correspondant au début et fin des plateaus
        indice_plateaus = []
        
        for plat in range(self.plateau_number):
            indice_plateaus += [[ind for ind in range(ind_ini_plt_core_profile[plat] , ind_fin_plt_core_profile[plat])]]
        
        return indice_plateaus
    
    # fonction qui retourne l'indice d'une liste correspondant à la valeur la plus proche de la valeur en entrée
    def get_closest_index(self, value, input_list):
        # "Récupère l'indice le plus proche d'une valeur dans une liste stictement croissante."
        lst = np.abs(np.array(input_list)-value)
        min = np.min(lst)
        
        return np.where(lst == min)[0][0]
    
    # fonction qui retourne les indices des rayons les plus proches de ceux voulus par l'utilisateur 
    def indices_des_rayons_utiles(self, rho_tor_norm, normalisation_param):
        ind_liste = []
        for i in range(self.nbr_rayon_utile):
            ind_liste += [self.get_closest_index(self.rayons_norm[i], rho_tor_norm/normalisation_param)] #on prend les rayons 0, 0.1, 0.2 ... 1.0

        return ind_liste

    # fonction qui prend en entrée le mean_std de structure
    def total_mean(self, mean_std):
        mean_tot = []
        for rayon in mean_std:
            mean_tot += rayon[0]
        return mean_tot 

    # meme fonction qu'avant mais avec les std (s'avère inutile mais bon...)
    def total_std(self, mean_std):
        std_tot = []
        for rayon in mean_std:
            std_tot += rayon[1]

        return std_tot


    #####_______________________________________________#####
    #####_______Display of parameters in time_______#####
    #####_______________________________________________#####

    def comparaison_ne(self):
        time_core =  self.t_core
        time_fevre = self.t_fevre
        t_init_core = time_core[0]
        t_init_fevre = time_fevre[0]
        
        plateaus_core = np.array(indicePlateaus(self, shot_number, time_core)).flatten()
        plateaus_fevre = np.array(indicePlateaus(self, shot_number, time_fevre)).flatten()
        
        r_core = self.r_core
        r_fevre = self.r_fevre
        ne_core = self.ne_core
        ne_fevre = self.ne_profile_fevre

        grad_ne_core = []
        for i in range(len(time_core)):
            grad_ne_core += [np.gradient(ne_core[:,i], r_core[:,i])]
        grad_ne_core = np.transpose(np.array(grad_ne_core))

        grad_ne_fevre = []
        for i in range(len(time_fevre)):
            grad_ne_fevre += [np.gradient(ne_fevre[:,i], r_fevre[:,i])]
        grad_ne_fevre = np.transpose(np.array(grad_ne_fevre))

        fig_core, (ax1_core, ax2_core) = plt.subplots(1, 2, figsize=(10,5))
        line1_core, = ax1_core.plot(r_core[:,0], ne_core[:,0])
        line2_core, = ax2_core.plot(r_core[:,0], -self.R_core[:,0]*grad_ne_core[:,0]/ne_core[:,0])
        
        def update_core(frame):
            line1_core.set_data(r_core[:, frame], ne_core[:,frame])
            ax1_core.relim()
            ax1_core.autoscale()
            ax1_core.set_xlabel(r'$\r_{minor}$')
            ax1_core.set_ylabel(r'$n_e$ core_profile [eV]')
            
            if frame in plateau_core:
                ax1_core.set_title(r'PLATEAU $n_e$ at time = ' + str(time_core[frame] - t_init_core) +  ' (s)')
            else:
                ax1_core.set_title(r'$n_e$ at time = ' + str(time_core[frame] - t_init_core) +  ' (s)')
            
            line2_core.set_data(r_core[:, frame], r_core[:,0], -self.R_core[:][frame]*grad_ne_core[:][frame]/ne_core[:][frame])
            ax2_core.relim()
            ax2_core.autoscale()
            ax2_core.set_xlabel(r'$\rho_{tor}$')
            ax2_core.set_ylabel(r'$-R\nabla n_e$/$n_e$')
            ax2_core.set_title(r'$-R\nabla n_e$/$n_e$ at time = ' + str(time_fevre[frame] - t_init_fevre) + ' (s)')
            

            return line1_core, line2_core
        
        fig_fevre, (ax1_fevre, ax2_fevre) = plt.subplots(1, 2, figsize=(10,5))
        line1_fevre, = ax1_fevre.plot(r_fevre[:,0], ne_fevre[:,0])
        line2_fevre, = ax2_fevre.plot(r_fevre[:,0], -self.R_fevre[:,0]*grad_ne_fevre[:,0]/ne_fevre[:,0])
        
        def update_fevre(frame):
            line1_fevre.set_data(r_fevre[:, frame], ne_fevre[:,frame])
            ax1_fevre.relim()
            ax1_fevre.autoscale()
            ax1_fevre.set_xlabel(r'$\r_{minor}$')
            ax1_fevre.set_ylabel(r'$n_e$ core_profile [eV]')
            ax1_fevre.set_title(r'$n_e$ at time = ' + str(time_fevre[frame] - t_init_fevre) +  ' (s)')
            
            line2_fevre.set_data(r_fevre[:, frame], r_fevre[:,0], -self.R_fevre[:][frame]*grad_ne_fevre[:][frame]/ne_fevre[:][frame])
            ax2_fevre.relim()
            ax2_fevre.autoscale()
            ax2_fevre.set_xlabel(r'$\rho_{tor}$')
            ax2_fevre.set_ylabel(r'$-R\nabla n_e$/$n_e$')
            ax2_fevre.set_title(r'$-R\nabla n_e$/$n_e$ at time = ' + str(time_fevre[frame] - t_init_fevre) + ' (s)')
            

            return line1_fevre, line2_fevre



        ani_core = FuncAnimation(fig_core, update_core, frames = np.arange(time_core), interval = 50)

        #ani_fevre = FuncAnimation(fig_fevre, update_fevre, frames = np.arange(time_fevre), interval = 50)
        plt.show()
        
    def comparaison_ne_profile(self, x, shot):
        time_core =  self.t_core
        time_fevre_raw = self.t_fevre
        
        r_core = self.r_core
        r_fevre_raw = self.r_fevre
        ne_core = self.ne_core
        ne_fevre_raw = self.ne_profile_fevre
        
        r_fevre , ne_fevre, time_fevre = self.erase_nan_zeros(r_fevre_raw , ne_fevre_raw, time_fevre_raw)

        grad_ne_core = []
        for i in range(len(time_core)):
            grad_ne_core += [np.gradient(ne_core[:,i], r_core[:,i])]
        grad_ne_core = np.transpose(np.array(grad_ne_core))

        grad_ne_fevre = []
        for i in range(len(time_fevre)):
            grad_ne_fevre += [np.gradient(ne_fevre[:,i], r_fevre[:,i])]
        grad_ne_fevre = np.transpose(np.array(grad_ne_fevre))
        
        fig, axs = plt.subplots(2, 3)
        ax1, ax2, ax3, ax4, ax5 = axs[0, 0], axs[0, 1], axs[0, 2], axs[1, 0], axs[1, 2]
        
        ax1.plot(time_fevre, ne_fevre[x, :])
        ax2.plot(time_core, ne_core[x, :])
        ax3.plot(time_core, grad_ne_core[x, :])
        ax4.plot(time_fevre_raw, ne_fevre_raw[x, :])
        ax5.plot(time_fevre, grad_ne_fevre[x, :])
        
        ax1.set_xlabel('time fevre')
        ax1.set_ylabel(r'$n_e$ Fevre')
        
        ax2.set_xlabel('time core_profile')
        ax2.set_ylabel(r'$n_e$ core_profile')
        
        ax3.set_xlabel('time core')
        ax3.set_ylabel(r'$RLn_e$ core_profile')
        
        ax4.set_xlabel('time reflecto fevre')
        ax4.set_ylabel(r'raw (without Nan and 0 filtering)$n_e$ Fevre')
        
        ax5.set_xlabel('time reflecto fevre')
        ax5.set_ylabel(r'$RLn_e$ Fevre')
        
        #bounds of plateaus:
        
        for i in range(len(self.t_ini)):
            
            coloration =self.color_list[i]
            ax1.axvline(self.t_ini[i], color = coloration, linestyle = '--')
            ax1.axvline(self.t_fin[i], color = coloration, linestyle = '--')
            
            ax2.axvline(self.t_ini[i], color = coloration, linestyle = '--')
            ax2.axvline(self.t_fin[i], color = coloration, linestyle = '--')
            
            ax3.axvline(self.t_ini[i], color = coloration, linestyle = '--')
            ax3.axvline(self.t_fin[i], color = coloration, linestyle = '--')
            
            ax4.axvline(self.t_ini[i], color = coloration, linestyle = '--')
            ax4.axvline(self.t_fin[i], color = coloration, linestyle = '--')
            
            ax5.axvline(self.t_ini[i], color = coloration, linestyle = '--')
            ax5.axvline(self.t_fin[i], color = coloration, linestyle = '--')
            
        fig.suptitle("ne over time for x = r/a = " + str(np.round(0.1*(x+1),2)) +  " for the shot: " + str(shot))
            
        plt.show()
        
        
    def comparaison_ne_profile_new(self, x, shot):
        dico_useful = self.dico_all_data[str(shot)]
        try:
            time_core =  np.array(dico_useful['t_core'])
            time_equi = np.array(dico_useful['t_equi'])
            time_fevre_raw = np.array(dico_useful['t_fevre'])
            rho_tor_norm_fevre = np.array(dico_useful['rho_tor_norm_fevre'])
            rho_tor_norm_core = np.array(dico_useful['rho_tor_norm_core'])
            
            r_equi= np.array(dico_useful['r_equi'])
            r_core = self.interpolation_2D(time_equi, time_core, r_equi, rho_tor_norm_core)
            r_fevre_raw = self.interpolation_2D(time_equi, time_fevre_raw, r_equi, rho_tor_norm_fevre)
            ne_core = np.array(dico_useful['ne_core'])
            ne_fevre_raw = np.array(dico_useful['ne_profile_fevre'])
    
            t_ini_plto = self.time_ini[shot].tolist()
            t_fin_plto = self.time_fin[shot].tolist()
            
            r_fevre , ne_fevre, time_fevre = self.erase_nan_zeros(r_fevre_raw , ne_fevre_raw, time_fevre_raw)
    
            grad_ne_core = []
            for i in range(len(time_core)):
                grad_ne_core += [np.gradient(ne_core[:,i], r_core[:,i])]
            grad_ne_core = np.transpose(np.array(grad_ne_core))
    
            grad_ne_fevre = []
            for i in range(len(time_fevre)):
                grad_ne_fevre += [np.gradient(ne_fevre[:,i], r_fevre[:,i])]
            grad_ne_fevre = np.transpose(np.array(grad_ne_fevre))
            
            fig, axs = plt.subplots(2, 3)
            
            ax1, ax2, ax3, ax4, ax5 = axs[0, 0], axs[0, 1], axs[0, 2], axs[1, 0], axs[1, 2]
            
            ax1.plot(time_fevre, ne_fevre[x, :])
            ax2.plot(time_core, ne_core[x, :])
            ax3.plot(time_core, grad_ne_core[x, :])
            ax4.plot(time_fevre_raw, ne_fevre_raw[x, :])
            ax5.plot(time_fevre, grad_ne_fevre[x, :])
            
            ax1.set_xlabel('time fevre')
            ax1.set_ylabel(r'$n_e$ Fevre')
            
            ax2.set_xlabel('time core_profile')
            ax2.set_ylabel(r'$n_e$ core_profile')
            
            ax3.set_xlabel('time core')
            ax3.set_ylabel(r'$RLn_e$ core_profile')
            
            ax4.set_xlabel('time reflecto fevre')
            ax4.set_ylabel(r'raw (without Nan and 0 filtering)$n_e$ Fevre')
            
            ax5.set_xlabel('time reflecto fevre')
            ax5.set_ylabel(r'$RLn_e$ Fevre')
            
            #bounds of plateaus:
            
            for i in range(len(t_ini_plto)):
                
                coloration =self.color_list[i]
                ax1.axvline(t_ini_plto[i], color = coloration, linestyle = '--')
                ax1.axvline(t_fin_plto[i], color = coloration, linestyle = '--')
                
                ax2.axvline(t_ini_plto[i], color = coloration, linestyle = '--')
                ax2.axvline(t_fin_plto[i], color = coloration, linestyle = '--')
                
                ax3.axvline(t_ini_plto[i], color = coloration, linestyle = '--')
                ax3.axvline(t_fin_plto[i], color = coloration, linestyle = '--')
                
                ax4.axvline(t_ini_plto[i], color = coloration, linestyle = '--')
                ax4.axvline(t_fin_plto[i], color = coloration, linestyle = '--')
                
                ax5.axvline(t_ini_plto[i], color = coloration, linestyle = '--')
                ax5.axvline(t_fin_plto[i], color = coloration, linestyle = '--')
                
            fig.suptitle("ne over time for x = r/a = " + str(np.round(0.1*(x+1),2)) +  " for the shot: " + str(shot))
                
            plt.show()
        except:
            print("Problème sur ce shot à cause d'une absence de data depuis le code de Fèvre")
        
            


    #####_______________________________________________#####
    #####_______Calcul des adimensional parameter_______#####
    #####_______________________________________________#####


    def R_on_L_Te_plateaus_Aaron(self, shot_number):
        #Temps du shot
        time = self.t_core
        time = time.tolist()
        
        #Rayons normalisés pour ce shot
        r = self.r_core


       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        Te = self.Te_core


        r, Te, time = self.erase_nan(r, Te, time)
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)

        grad_Te = [] 

        for i in range(time_size):
            grad_Te += [np.gradient(Te[:,i], r[:,i])]

        grad_Te = np.transpose(np.array(grad_Te))
        #récupération des plateaus pour R/L Te:
                    
        r_l_te_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que les points sélectionnées par l'utilisateur
            r_l_te_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    r_l_te_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_te_plateaus_r_j += [-self.R_core[ind_rayon[i]][ind_plat]*grad_Te[ind_rayon[i]][ind_plat]/Te[ind_rayon[i]][ind_plat]]
                    r_l_te_plateaus_r += [r_l_te_plateaus_r_j]
            r_l_te_plateaus += [r_l_te_plateaus_r]


               
        #Calcul des moyennes des plateaus
        mean_std_r_l_te = []

        for r in range(self.nbr_rayon_utile):
            mean_r_l_te_r = []
            std_r_l_te_r = []
            for plat in r_l_te_plateaus[r]:
                mean_r_l_te_r+= [np.mean(plat)]
                std_r_l_te_r += [np.std(plat)]
            mean_std_r_l_te += [[mean_r_l_te_r, std_r_l_te_r]]

        return mean_std_r_l_te

    def R_on_L_ne_plateaus(self, shot_number):
        #denss du shot
        time = self.t_core
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_core

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
       
        #Récupération des densératures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les densités élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de denss de calcul)
        ne = self.ne_core
        grad_ne = []

        for i in range(time_size):
            grad_ne += [np.gradient(ne[:,i], r[:,i])]

        grad_ne = np.transpose(np.array(grad_ne))

        #récupération des plateaus pour R/L Te:

        r_l_ne_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_l_ne_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    r_l_ne_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_ne_plateaus_r_j += [-self.R_core[ind_rayon[i]][ind_plat]*grad_ne[ind_rayon[i]][ind_plat]/ne[ind_rayon[i]][ind_plat]]
                    r_l_ne_plateaus_r += [r_l_ne_plateaus_r_j]
            r_l_ne_plateaus += [r_l_ne_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_r_l_ne = []

        for r in range(self.nbr_rayon_utile):
            mean_r_l_ne_r = []
            std_r_l_ne_r = []
            for plat in r_l_ne_plateaus[r]:
                mean_r_l_ne_r+= [np.mean(plat)]
                std_r_l_ne_r += [np.std(plat)]
            mean_std_r_l_ne += [[mean_r_l_ne_r, std_r_l_ne_r]]

        return mean_std_r_l_ne
    
    def R_on_L_Ti_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_core
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_core

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        Ti = self.Ti_core
        grad_Ti = []

        for i in range(time_size):
            grad_Ti += [np.gradient(Ti[:,i], r[:,i])]

        grad_Ti = np.transpose(np.array(grad_Ti))

        #récupération des plateaus pour R/L Te:
        
        r_l_ti_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_l_ti_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    r_l_ti_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_ti_plateaus_r_j += [-self.R_core[ind_rayon[i]][ind_plat]*grad_Ti[ind_rayon[i]][ind_plat]/Ti[ind_rayon[i]][ind_plat]]
                    r_l_ti_plateaus_r += [r_l_ti_plateaus_r_j]
            r_l_ti_plateaus += [r_l_ti_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_r_l_ti = []

        for r in range(self.nbr_rayon_utile):
            mean_r_l_ti_r = []
            std_r_l_ti_r = []
            for plat in r_l_ti_plateaus[r]:
                mean_r_l_ti_r+= [np.mean(plat)]
                std_r_l_ti_r += [np.std(plat)]
            mean_std_r_l_ti += [[mean_r_l_ti_r, std_r_l_ti_r]]

        return mean_std_r_l_ti

    def Ti_on_Te_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_core
        time = time.tolist()
        
        #Rayon normalisé pour ce shot
        r = self.r_core

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        Te = self.Te_core
        Ti = self.Ti_core


        r, Te, [Ti], time = self.erase_nan_multi_vec(r, Te, [Ti], time)
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)

        #récupération des plateaus pour R/L Te:
       
        ti_te_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            ti_te_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    ti_te_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        if np.isnan(Ti[ind_rayon[i]][ind_plat]):
                            print("Shot: ", shot_number, " has Ti full of Nan.")
                        ti_te_plateaus_r_j += [Ti[ind_rayon[i]][ind_plat]/Te[ind_rayon[i]][ind_plat]]
                    ti_te_plateaus_r += [ti_te_plateaus_r_j]
            ti_te_plateaus += [ti_te_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_ti_te = []

        for r in range(self.nbr_rayon_utile):
            mean_ti_te_r = []
            std_ti_te_r = []
            for plat in ti_te_plateaus[r]:
                mean_ti_te_r+= [np.mean(plat)]
                std_ti_te_r += [np.std(plat)]
            mean_std_ti_te += [[mean_ti_te_r, std_ti_te_r]]

        return mean_std_ti_te

    def q_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        q_equi = self.q_equi


        #récupération des plateaus pour R/L Te:

        q_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            q_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    q_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        q_plateaus_r_j += [np.abs(q_equi[ind_rayon[i]][ind_plat])]
                    q_plateaus_r += [q_plateaus_r_j]
            q_plateaus += [q_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_q = []

        for r in range(self.nbr_rayon_utile):
            mean_q_r = []
            std_q_r = []
            for plat in q_plateaus[r]:
                mean_q_r+= [np.mean(plat)]
                std_q_r += [np.std(plat)]
            mean_std_q += [[mean_q_r, std_q_r]]

        return mean_std_q
    
    def magnetic_shear_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        magn_shear_equi = self.magn_shear_equi


        #récupération des plateaus pour R/L Te:

        magn_shear_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            magn_shear_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    magn_shear_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        magn_shear_plateaus_r_j += [-magn_shear_equi[ind_rayon[i]][ind_plat]]
                    magn_shear_plateaus_r += [magn_shear_plateaus_r_j]
            magn_shear_plateaus += [magn_shear_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_magn_shear = []

        for r in range(self.nbr_rayon_utile):
            mean_magn_shear_r = []
            std_magn_shear_r = []
            for plat in magn_shear_plateaus[r]:
                mean_magn_shear_r+= [np.mean(plat)]
                std_magn_shear_r += [np.std(plat)]
            mean_std_magn_shear += [[mean_magn_shear_r, std_magn_shear_r]]

        return mean_std_magn_shear

    def elongation_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        elong_equi = self.elong_equi


        #récupération des plateaus pour R/L Te:

        elong_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            elong_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    elong_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        elong_plateaus_r_j += [elong_equi[ind_rayon[i]][ind_plat]]
                    elong_plateaus_r += [elong_plateaus_r_j]
            elong_plateaus += [elong_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_elong = []

        for r in range(self.nbr_rayon_utile):
            mean_elong_r = []
            std_elong_r = []
            for plat in elong_plateaus[r]:
                mean_elong_r+= [np.mean(plat)]
                std_elong_r += [np.std(plat)]
            mean_std_elong += [[mean_elong_r, std_elong_r]]

        return mean_std_elong   

    def triang_up_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        triang_up_equi = self.triang_up_equi


        #récupération des plateaus pour R/L Te:

        triang_up_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            triang_up_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    triang_up_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        triang_up_plateaus_r_j += [triang_up_equi[ind_rayon[i]][ind_plat]]
                    triang_up_plateaus_r += [triang_up_plateaus_r_j]
            triang_up_plateaus += [triang_up_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_triang_up = []

        for r in range(self.nbr_rayon_utile):
            mean_triang_up_r = []
            std_triang_up_r = []
            for plat in triang_up_plateaus[r]:
                mean_triang_up_r+= [np.mean(plat)]
                std_triang_up_r += [np.std(plat)]
            mean_std_triang_up += [[mean_triang_up_r, std_triang_up_r]]

        return mean_std_triang_up  
  
    def triang_low_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        triang_low_equi = self.triang_low_equi


        #récupération des plateaus pour R/L Te:

        triang_low_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            triang_low_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    triang_low_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        triang_low_plateaus_r_j += [triang_low_equi[ind_rayon[i]][ind_plat]]
                    triang_low_plateaus_r += [triang_low_plateaus_r_j]
            triang_low_plateaus += [triang_low_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_triang_low = []

        for r in range(self.nbr_rayon_utile):
            mean_triang_low_r = []
            std_triang_low_r = []
            for plat in triang_low_plateaus[r]:
                mean_triang_low_r+= [np.mean(plat)]
                std_triang_low_r += [np.std(plat)]
            mean_std_triang_low += [[mean_triang_low_r, std_triang_low_r]]

        return mean_std_triang_low   
    
    #fonction filtrage: erase_nan suffisant pour Te, erase_nan_zeros nécessaire pour Fevre ne
    def erase_nan(self, rho_tor, ne, time):
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
    
        
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
        return rho_tor, ne, time

    def erase_nan_multi_vec(self, rho_tor, ne, lst_vec, time):
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
    
        
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            for el in lst_vec:
                if np.ndim(el) == 1:
                    el= np.delete(el, ind, 0)
                elif np.ndim(el) ==2:
                    el= np.delete(el, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
        return rho_tor, ne, lst_vec, time
    
    def erase_nan_zeros(self, rho_tor, ne, time): 
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
    
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
            
        masque_zeros = np.all(ne == 0., axis = 0)
        col_zeros = np.where(masque_zeros)[0]
        for ind in col_zeros:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            time = np.delete(time, ind, 0)
            col_zeros -=1
        
        time_size = len(time)
        ne_copy = np.copy(ne)
        col_soustraction = 0
        
        for ind in range(time_size):
            if not np.all(ne_copy[:, ind]):
                rho_tor = np.delete(rho_tor, ind-col_soustraction, 1)
                ne= np.delete(ne, ind-col_soustraction, 1)
                time = np.delete(time, ind-col_soustraction, 0)
                col_soustraction +=1
        # plt.plot(rho_tor, ne)
        return rho_tor, ne, time

    def erase_nan_zeros_multi_vec(self, rho_tor, ne, lst_vec, time): 
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
        print("col des nan: ", col_nan)
        print(len(time))
        print(np.shape(ne))
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            for el in lst_vec:
                if np.ndim(el) == 1:
                    el= np.delete(el, ind, 0)
                elif np.ndim(el) ==2:
                    el= np.delete(el, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
            
        masque_zeros = np.any(ne == 0., axis = 0)
        col_zeros = np.where(masque_zeros)[0]
        print("col des zeros: ", col_zeros)
        for ind in col_zeros:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            for el in lst_vec:
                if np.ndim(el) == 1:
                    el= np.delete(el, ind, 0)
                elif np.ndim(el) ==2:
                    el= np.delete(el, ind, 1)
            time = np.delete(time, ind, 0)
            col_zeros -=1
        
        time_size = len(time)
        ne_copy = np.copy(ne)
        col_soustraction = 0
        print(time_size)
        print(np.shape(ne))
        for ind in range(time_size):
            if not np.all(ne_copy[:, ind]):
                rho_tor = np.delete(rho_tor, ind-col_soustraction, 1)
                ne= np.delete(ne, ind-col_soustraction, 1)
                for el in lst_vec:
                    if np.ndim(el) == 1:
                        el= np.delete(el, ind-col_soustraction, 0)
                    elif np.ndim(el) ==2:
                        el= np.delete(el, ind-col_soustraction, 1)
                time = np.delete(time, ind-col_soustraction, 0)
                col_soustraction +=1
        # plt.plot(rho_tor, ne)
        print("tailel de ne à la fin:", np.shape(ne))
        self.verif_zeros(ne)
        return rho_tor, ne, lst_vec, time
    
    def verif_zeros(self, mat):
        print("verification des zeros:")
        (n, m) = np.shape(mat)
        for i in range(n):
            for j in range(m):
                if mat[i,j] == 0:
                    print(i, j)
                    
    def R_on_L_ne_FEVRE_plateaus(self, shot_number):
        #temps du shot pour l'ids utile au calcul du param adimensionnel 
        #time = self.t_refl 
        time = self.t_fevre#c'est le temps utilisé par fèvre normalement
        
        time = time.tolist()
        
        #Rayon normalisé pour ce shot
        r= self.r_fevre
       
        #Récupération des densité électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les densités élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de denss de calcul)
        ne = self.ne_fevre
        a = self.a_fevre
        r, ne, [a], time= self.erase_nan_zeros_multi_vec(r, ne, [a] , time)
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
        grad_ne = []
        
        
        for i in range(time_size):
            grad_ne += [np.gradient(ne[:,i], r[:,i])]
        
        grad_ne = np.transpose(np.array(grad_ne))
        
        #récupération des plateaus pour R/L Te:

        r_l_ne_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_l_ne_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    r_l_ne_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], a[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_ne_plateaus_r_j += [-self.R_fevre[ind_rayon[i]][ind_plat]*grad_ne[ind_rayon[i]][ind_plat]/ne[ind_rayon[i]][ind_plat]]
                    r_l_ne_plateaus_r += [r_l_ne_plateaus_r_j]
            r_l_ne_plateaus += [r_l_ne_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_r_l_ne = []

        for r in range(self.nbr_rayon_utile):
            mean_r_l_ne_r = []
            std_r_l_ne_r = []
            for plat in r_l_ne_plateaus[r]:
                mean_r_l_ne_r+= [np.mean(plat)]
                std_r_l_ne_r += [np.std(plat)]
            mean_std_r_l_ne += [[mean_r_l_ne_r, std_r_l_ne_r]]

        return mean_std_r_l_ne

    def check_evolution_psin(self,i):
        psin = self.psin_fevre
        time = self.t_fevre
        indice_plateaus = self.indicePlateaus(shot_number, time)
        plt.plot(time[indice_plateaus[0]], psin[i,indice_plateaus[0]])
        plt.show()


    def R_on_L_ne_FEVRE_corrected_plateaus(self, shot_number):
        #temps du shot pour l'ids utile au calcul du param adimensionnel 
        #time = self.t_refl 
        time = self.t_fevre#c'est le temps utilisé par fèvre normalement
        
        time = time.tolist()
        
        psin_fevre = self.psin_fevre
        psin_equi = self.psin_equi
       
        #Récupération des densité électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les densités élec existent déjà en plateaus, regarder si calculer ju

        time_size = np.size(time)

        #ne = self.ne_DREFRAP_fit_fevre
        #Récupération des indices des plateaus
        a = self.a_fevre
        r_equi = self.r_equi
        R_equi = self.R_equi
        a_equi = self.a_equi
        psin_fevre, ne, [a], time= self.erase_nan_zeros_multi_vec(psin_fevre, ne, [a] , time)
        
        indice_plateaus = self.indicePlateaus(shot_number, time)
        
        num_plateau_init = self.numeros_plateaus_utiles_fevre[0]

        
        print("psin:", np.shape(psin_fevre), np.shape(psin_equi), np.shape(r_equi), np.shape(ne))
        ###############################
        #transformation des psin en r:#
        ###############################       
        self.r_fevre_plat = []
        self.R_fevre_plat = []
        grad_ne = []
        print("num plte utile:", self.numeros_plateaus_utiles_fevre)
        for plat in self.numeros_plateaus_utiles_fevre:
            ind_equi = self.get_closest_index(self.t_ini[plat], self.t_equi)
            self.r_fevre_plat += [self.interpolation_1D(psin_equi[:,ind_equi],psin_fevre[:,indice_plateaus[plat][0]], r_equi[:,ind_equi])]#les valeurs qui dépassent un psin de 1 seront extrapolées et fausses mais on s'en fout vu qu'on ne considerera que les r tel que psin soit inférieur à 1 ie dans le core
            self.R_fevre_plat += [self.interpolation_1D(psin_equi[:,ind_equi],psin_fevre[:,indice_plateaus[plat][0]], R_equi[:,ind_equi])]
            print("tps plat pour fevre:", time[indice_plateaus[plat][0]] , "tps pour equi:", self.t_equi[ind_equi], "tps plat fevre", indice_plateaus[plat][0])
            grad_ne_plat = []
            for t_ind in indice_plateaus[plat-num_plateau_init]:
                grad_ne_plat += [np.gradient(ne[:,t_ind], self.r_fevre_plat[plat - num_plateau_init][:])]
            grad_ne += [np.transpose(grad_ne_plat)]
        
        print(np.shape(grad_ne[0]))
        
        r_l_ne_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_l_ne_plateaus_r = []
            for plat in range(len(self.numeros_plateaus_utiles_fevre)):
                ind_rayon = self.indices_des_rayons_utiles(self.r_fevre_plat[plat][:], a[indice_plateaus[plat-num_plateau_init][0]])
                r_l_ne_plateaus_r_j = []
                for ind_plat in indice_plateaus[plat]:
                    b = self.R_fevre_plat[plat][ind_rayon[i]]
                    r_l_ne_plateaus_r_j += [-self.R_fevre_plat[plat][ind_rayon[i]]*grad_ne[plat][ind_rayon[i]][ind_plat-indice_plateaus[plat][0]]/ne[ind_rayon[i]][ind_plat]]
                r_l_ne_plateaus_r += [r_l_ne_plateaus_r_j]
            r_l_ne_plateaus += [r_l_ne_plateaus_r]
        ##############################

        #Calcul des moyennes des plateaus
        mean_std_r_l_ne = []

        for r in range(self.nbr_rayon_utile):
            mean_r_l_ne_r = []
            std_r_l_ne_r = []
            for plat in r_l_ne_plateaus[r]:
                mean_r_l_ne_r+= [np.mean(plat)]
                std_r_l_ne_r += [np.std(plat)]
            mean_std_r_l_ne += [[mean_r_l_ne_r, std_r_l_ne_r]]

        return mean_std_r_l_ne


    #subtilité de ce param: pas de rayons à prendre, pour simplifier je peux calculer 10 fois la même chose, ce que j'ai fait pour pas avoir à modifier tout le code 
    def beta_norm_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        rho_tor_norm = self.rho_tor_norm_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        beta_norm = self.beta_norm_equi

        #récupération des plateaus pour R/L Te:
        
        beta_norm_plateaus = []
        
        for plat in range(self.plateau_number):
            beta_norm_value_on_plateau = [] #pour le plateau j 
            for ind in indice_plateaus[plat]:
                beta_norm_value_on_plateau += [beta_norm[ind]]
            beta_norm_plateaus += [beta_norm_value_on_plateau]

        #Calcul des moyennes des plateaus
        mean_std_beta_norm = []

        #simulation des 10 rayon qui n'ont aucune valeur réelle:
        for i in range(self.nbr_rayon_utile):
            mean_beta_norm = []
            std_beta_norm = []
            for plat in beta_norm_plateaus:
                mean_beta_norm+= [np.mean(plat)]
                std_beta_norm += [np.std(plat)]
            mean_std_beta_norm += [[mean_beta_norm, std_beta_norm]]

        return mean_std_beta_norm
    
    def beta_tor_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        rho_tor_tor = self.rho_tor_norm_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        beta_tor = self.beta_tor_equi

        #récupération des plateaus pour R/L Te:
        
        beta_tor_plateaus = []
        
        for plat in range(self.plateau_number):
            beta_tor_value_on_plateau = [] #pour le plateau j 
            for ind in indice_plateaus[plat]:
                beta_tor_value_on_plateau += [beta_tor[ind]]
            beta_tor_plateaus += [beta_tor_value_on_plateau]

        #Calcul des moyennes des plateaus
        mean_std_beta_tor = []

        #simulation des 10 rayon qui n'ont aucune valeur réelle:
        for i in range(self.nbr_rayon_utile):
            mean_beta_tor = []
            std_beta_tor = []
            for plat in beta_tor_plateaus:
                mean_beta_tor+= [np.mean(plat)]
                std_beta_tor += [np.std(plat)]
            mean_std_beta_tor += [[mean_beta_tor, std_beta_tor]]

        return mean_std_beta_tor
    
    def beta_tor_calcul_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        r = self.r_core

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        Bmag = self.Bmag_core #Bref
        ne = self.ne_core #ne_ref
        Te = self.Te_core #Te_ref

        #récupération des plateaus pour R/L Te:
        
        beta_tor_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que les points sélectionnées par l'utilisateur
            beta_tor_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    beta_tor_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        if Bmag[ind_plat] == 0 or np.isnan(Bmag[ind_plat]):
                            print("Pb avec shot: ", shot_number, " : Bmag: ", Bmag[ind_plat])
                        beta_tor_plateaus_r_j += [2*self.mu_zero * ne[ind_rayon[i]][ind_plat] * Te[ind_rayon[i]][ind_plat]*np.abs(self.qe)/(Bmag[ind_plat])**2] #attention au passage d'eV en J pour la température
                    beta_tor_plateaus_r += [beta_tor_plateaus_r_j]
            beta_tor_plateaus += [beta_tor_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_beta_tor = []

        #simulation des 10 rayon qui n'ont aucune valeur réelle:
        for i in range(self.nbr_rayon_utile):
            mean_beta_tor = []
            std_beta_tor = []
            for plat in beta_tor_plateaus[i]:
                mean_beta_tor+= [np.mean(plat)]
                std_beta_tor += [np.std(plat)]
            mean_std_beta_tor += [[mean_beta_tor, std_beta_tor]]
            

        return mean_std_beta_tor
    
       
    def beta_tor_calcul_fevre_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_fevre
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        r = self.r_fevre

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        Bmag = self.Bmag_fevre #Bref
        ne = self.ne_fevre #ne_ref
        Te = self.Te_fevre #Te_ref

        #récupération des plateaus pour R/L Te:
        
        beta_tor_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que les points sélectionnées par l'utilisateur
            beta_tor_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    beta_tor_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        if Bmag[ind_plat] == 0 or np.isnan(Bmag[ind_plat]):
                            print("Pb avec shot: ", shot_number, " : Bmag: ", Bmag[ind_plat])
                        beta_tor_plateaus_r_j += [2*self.mu_zero * ne[ind_rayon[i]][ind_plat] * Te[ind_rayon[i]][ind_plat]*np.abs(self.qe)/(Bmag[ind_plat])**2] #attention au passage d'eV en J pour la température
                    beta_tor_plateaus_r += [beta_tor_plateaus_r_j]
            beta_tor_plateaus += [beta_tor_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_beta_tor = []

        #simulation des 10 rayon qui n'ont aucune valeur réelle:
        for i in range(self.nbr_rayon_utile):
            mean_beta_tor = []
            std_beta_tor = []
            for plat in beta_tor_plateaus[i]:
                mean_beta_tor+= [np.mean(plat)]
                std_beta_tor += [np.std(plat)]
            mean_std_beta_tor += [[mean_beta_tor, std_beta_tor]]
            

        return mean_std_beta_tor
  

    def z_eff_res_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        rho_tor_norm = self.rho_tor_norm_core

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        z_eff_res = self.z_eff_res_core

        #récupération des plateaus pour R/L Te:
        
        z_eff_res_plateaus = []
        
        for plat in range(self.plateau_number):
            z_eff_res_value_on_plateau = [] #pour le plateau j 
            for ind in indice_plateaus[plat]:
                z_eff_res_value_on_plateau += [z_eff_res[ind]]
            z_eff_res_plateaus += [z_eff_res_value_on_plateau]

        #Calcul des moyennes des plateaus
        mean_std_z_eff_res = []

        #simulation des 10 rayon qui n'ont aucune valeur réelle:
        for i in range(self.nbr_rayon_utile):
            mean_z_eff_res = []
            std_z_eff_res = []
            for plat in z_eff_res_plateaus:
                mean_z_eff_res+= [np.mean(plat)]
                std_z_eff_res += [np.std(plat)]
            mean_std_z_eff_res += [[mean_z_eff_res, std_z_eff_res]]

        return mean_std_z_eff_res
    
    def collisionality_plateaus(self, shot_number):
        #Temps du shot
        time = self.t_core
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        rho_tor_norm = self.rho_tor_norm_core

        # #Récupération des indices des plateaus
        # indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des valeurs utiles au calcul de la colisionalité

        Te = self.Te_core #2D
        ne = self.ne_core #faudra faire la même version avec Fèvre 2D
        z_eff_res = self.z_eff_res_core # 1D
        q = self.q_core # 2D
        Rmag = self.Rmag_equi #0D : revoir la définition
        r = self.r_core #2D
        me = self.me #0D
        qe = self.qe #0D
        kb = self.kb #0D
        a= self.a_core

        r, Te, [ne, z_eff_res, q, a], time = self.erase_nan_zeros_multi_vec(r, Te, [ne, z_eff_res, q, a], time)
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
        

        #Calcul de la colisionalité directement aux endroits des plateaus:

        collisionality =  0
        Lambda = 0
        tau_b = 0
        
        collisionality_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            collisionality_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    collisionality_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], a[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind in indice_plateaus[plat]:
                        if Te[ind_rayon[i]][ind] <= 0:
                            print("Te est nul pour ce shot et ce rayon: ", shot_number, i, "et vaut: ", Te[ind_rayon[i]][ind])
                            collisionality = 0
                        else:
                            Lambda = 15.2 -0.5*np.log(ne[ind_rayon[i]][ind]/(10**20)) + np.log(Te[ind_rayon[i]][ind]/((10**3)))
                            tau_b = -q[ind_rayon[i]][ind] * Rmag * ((r[ind_rayon[i]][ind]/Rmag)**(-3/2)) * ((-qe*Te[ind_rayon[i]][ind]/me)**(-1/2))
                            collisionality = np.log(9174*(ne[ind_rayon[i]][ind]/(10**20) )* ((Te[ind_rayon[i]][ind]/((10**3)))**(-3/2)) * z_eff_res[ind] * Lambda * tau_b)
                        collisionality_plateaus_r_j += [collisionality]
                    collisionality_plateaus_r += [collisionality_plateaus_r_j]
            collisionality_plateaus += [collisionality_plateaus_r]
        
        
        #Calcul des moyennes des plateaus
        mean_std_collisionality = []

        for r in range(self.nbr_rayon_utile):
            mean_collisionality_r = []
            std_collisionality_r = []
            for plat in collisionality_plateaus[r]:
                mean_collisionality_r+= [np.mean(plat)]
                std_collisionality_r += [np.std(plat)]
            mean_std_collisionality += [[mean_collisionality_r, std_collisionality_r]]

        return mean_std_collisionality
    
    def collisionality_plateaus_FEVRE(self, shot_number):
        #Temps du shot
        time = self.t_core
        time = time.tolist()
        time_size = np.size(time)
        
        #Rayons normalisés pour ce shot
        rho_tor_norm = self.rho_tor_norm_core

        # #Récupération des indices des plateaus
        # indice_plateaus = self.indicePlateaus(shot_number, time) #fonction qui va en plus que de donner les indices des plateaus sur les vecteurs temps, réadapter le nombre de plateau que l'on a rééllement
       
        #Récupération des valeurs utiles au calcul de la colisionalité

        Te = self.Te_core #2D
        ne = self.ne_profile_fevre #2D
        z_eff_res = self.z_eff_res_core # 1D
        q = self.q_core # 2D
        Rmag = self.Rmag_equi #0D : revoir la définition
        r = self.r_core #2D
        me = self.me #0D
        qe = self.qe #0D
        kb = self.kb #0D
        a= self.a_core
        r, ne, [Te, z_eff_res, q, a], time = self.erase_nan_zeros_multi_vec(r, ne, [Te, z_eff_res, q, a], time)
        print(np.shape(ne), np.shape(Te))
        print(np.any(ne==0))
        
        self.verif_zeros(ne)
        ne = self.interpolation_2D(self.t_fevre, self.t_core, ne, self.rho_tor_norm_core)  
        print(np.any(ne==0))
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
        

        #Calcul de la colisionalité directement aux endroits des plateaus:
        collisionality =  0
        Lambda = 0
        tau_b = 0
        
        collisionality_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            collisionality_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    collisionality_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], a[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind in indice_plateaus[plat]:
                        if Te[ind_rayon[i]][ind] <= 0:
                            print("Te est nul pour ce shot et ce rayon: ", shot_number, i, "et vaut: ", Te[ind_rayon[i]][ind])
                            collisionality = 0
                        else:
                            Lambda = 15.2 -0.5*np.log(ne[ind_rayon[i]][ind]/(10**20)) + np.log(Te[ind_rayon[i]][ind]/((10**3)))
                            tau_b = -q[ind_rayon[i]][ind] * Rmag * ((r[ind_rayon[i]][ind]/Rmag)**(-3/2)) * ((-qe*Te[ind_rayon[i]][ind]/me)**(-1/2))
                            collisionality = np.log(9174*(ne[ind_rayon[i]][ind]/(10**20) )* ((Te[ind_rayon[i]][ind]/((10**3)))**(-3/2)) * z_eff_res[ind] * Lambda * tau_b)
                        collisionality_plateaus_r_j += [collisionality]
                    collisionality_plateaus_r += [collisionality_plateaus_r_j]
            collisionality_plateaus += [collisionality_plateaus_r]
        
        
        #Calcul des moyennes des plateaus
        mean_std_collisionality = []

        for r in range(self.nbr_rayon_utile):
            mean_collisionality_r = []
            std_collisionality_r = []
            for plat in collisionality_plateaus[r]:
                mean_collisionality_r+= [np.mean(plat)]
                std_collisionality_r += [np.std(plat)]
            mean_std_collisionality += [[mean_collisionality_r, std_collisionality_r]]

        return mean_std_collisionality

    def x_plateaus(self, shot_number):
        #denss du shot
        time = self.t_equi
        time = time.tolist()
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)
       
        #Récupération des densératures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les densités élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de denss de calcul)
        a = self.a_equi

        x = r/a

        #récupération des plateaus pour R/L Te:

        x_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            x_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    x_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        x_plateaus_r_j += [x[ind_rayon[i]][ind_plat]]
                    x_plateaus_r += [x_plateaus_r_j]
            x_plateaus += [x_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_x = []

        for r in range(self.nbr_rayon_utile):
            mean_x_r = []
            std_x_r = []
            for plat in x_plateaus[r]:
                mean_x_r+= [np.mean(plat)]
                std_x_r += [np.std(plat)]
            mean_std_x += [[mean_x_r, std_x_r]]

        return mean_std_x
    
    def rR_plateaus(self, shot_number):
        #denss du shot
        time = self.t_equi
        time = time.tolist()
        
        #Rayon normalisé pour ce shot
        r = self.r_equi
        R = self.R_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.indicePlateaus(shot_number, time)

        #récupération des plateaus pour R/L Te:

        r_on_R_plateaus = []
        for i in range(self.nbr_rayon_utile): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_on_R_plateaus_r = []#pour le rayon i
            for plat in range(self.plateau_number):
                    r_on_R_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.indices_des_rayons_utiles(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_on_R_plateaus_r_j += [r[ind_rayon[i]][ind_plat]/R[ind_rayon[i]][ind_plat]]
                    r_on_R_plateaus_r += [r_on_R_plateaus_r_j]
            r_on_R_plateaus += [r_on_R_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_r_on_R = []

        for r in range(self.nbr_rayon_utile):
            mean_r_on_R_r = []
            std_r_on_R_r = []
            for plat in r_on_R_plateaus[r]:
                mean_r_on_R_r+= [np.mean(plat)]
                std_r_on_R_r += [np.std(plat)]
            mean_std_r_on_R += [[mean_r_on_R_r, std_r_on_R_r]]

        return mean_std_r_on_R
    
    def interpolation_1D(self, time_ids1, time_ids2, lst_key_ids1):#ids1: ids de départ, ids2: ids d'arrivée
        #ex: time_equi, time_core, beta_equi => donne beta_core avec une interpolation
        r_interp = sp.interp1d(time_ids1 , lst_key_ids1, fill_value="extrapolate") #the "extrapolate" parameter is if we have a time_ids2 with larger ranger than time_ids1

        return r_interp(time_ids2)

    def interpolation_2D(self, time_ids1, time_ids2, lst_key_ids1, lst_exemple_ids2):#ids1: ids de départ, ids2: ids d'arrivée
        #ex: time_equi, time_core, r_equi, rho_tor_core => donne r_core avec une interpolation
        dim_ids1 = np.shape(lst_key_ids1)
        dim_ids2 = np.shape(lst_exemple_ids2) #cette liste ne sert qu'à avoir des dimensions cohérentes avec l'ids 2

        lst_y_ids1 = [i for i in range(dim_ids1[0])]
        lst_y_ids2 = [i for i in range(dim_ids2[0])]

        r_interp = sp.interp2d(lst_y_ids1,time_ids1 , np.transpose(lst_key_ids1))

        return np.transpose(r_interp(lst_y_ids2, time_ids2))
    
    def write_script_m(self, shot):
        content = """
        
        out = INTREF2ne_perfect______ok_not_perfect_but_pretty_good(""" + str(shot) +""", 'polarimetry map', 1, 'cutcompression', 'on', 'frap', 1)
        
        ne_fevre_txt = 'ne_fevre_from_matlab.txt'
        t_txt = 't_from_matlab.txt'
        psin_txt = 'psin_from_matlab.txt'
        
        dlmwrite(ne_fevre_txt, out.REFL_tot_fluc.ne, 'delimiter', '\t', 'precision', '%.4f')
        dlmwrite(t_txt, out.param.t, 'delimiter', '\t', 'precision', '%.4f')
        dlmwrite(psin_txt, out.REFL_tot_fluc.PSIN, 'delimiter', '\t', 'precision', '%.4f')
        
        """
    
        file_name = "script_test.m"
        with io.open(file_name, 'w', encoding = 'utf-8') as file:
            file.write(content)
            
    def reception_in_txt(self):
        ne_fevre_txt = 'ne_fevre_from_matlab.txt'
        t_txt = 't_from_matlab.txt'
        psin_txt = 'psin_from_matlab.txt'
        
        try:
        
            
            self.ne_fevre = np.loadtxt(ne_fevre_txt, delimiter = '\t')
            self.t_fevre = np.add(np.loadtxt(t_txt, delimiter = '\t'), self.t_igni) #vérifier l'addition de ce code
            self.psin_fevre_1d = np.loadtxt(psin_txt, delimiter = '\t')
            self.test = self.t_fevre
            
            ind_separatrix = self.get_closest_index(1, self.psin_fevre_1d)
            self.psin_fevre_1d = self.psin_fevre_1d[:ind_separatrix]
            self.psin_fevre = []
            for i in range(len(self.t_fevre)):
                self.psin_fevre +=[self.psin_fevre_1d]
            self.psin_fevre = np.transpose(np.array(self.psin_fevre))
            self.ne_fevre = self.ne_fevre[:ind_separatrix,:]
            # if np.shape(self.ne_profile_fevre)[1] != np.shape(self.t_fevre)[0]:
            #     len_max = np.size(self.ne_profile_fevre[0,:])
            #     self.t_fevre = self.t_fevre[:len_max]
            #     self.rho_tor_norm_fevre = self.rho_tor_norm_fevre[:,:len_max]
            
            if os.path.exists(t_txt):
                os.remove(ne_fevre_txt)
                os.remove(t_txt)
                os.remove(psin_txt)
            self.erreur_fevre = False
                
        except: 
            self.erreur_fevre = True
            print("Echec du code de Fevre")
       
        #plt.plot(self.rho_tor_norm_fevre, self.ne_profile_fevre)
    
        

    #reception of data from the IMAS database
    #Data stucture: 1D : time or r
    #               2D : r x time
    def receive_shot_imas_data(self, shot_number):
        #core_profile_imas_simu : Useful pour Ti
        core_profile_imas_simu = iw.get(shot_number, 'core_profiles', 0, 0, 'imas_simulation', 'west_simu_preparation')
        #core_profile_imas_public: Useful pour Te, ne, t, rho_tor_norm
        core_profile_imas_public = iw.get(shot_number, 'core_profiles', 0, 0, 'imas_public', 'west')
        #equilibrium: Useful for ne, q, R, r, a, rho_tor_norm
        equilibrium = iw.get(shot_number, 'equilibrium', 0, 1)
        #Useful for ne, t, rho_tor_norm
        reflectometer = iw.get(shot_number, "reflectometer_profile")

        # Récupération des plateaus de ce shot et son t_ignitron

        self.plateau_number = np.size(self.plateau[shot_number])
        
        
        #TIME RECEPTION FOR EACH USEFUL IDS
        t_core = core_profile_imas_public.time
        t_equi = equilibrium.time
        t_refl = reflectometer.time
        
        self.t_igni = self.time_igni[shot_number].values.tolist()[0]
        self.t_ini = self.time_ini[shot_number].values.tolist()
        self.t_fin = self.time_fin[shot_number].values.tolist()

        #RADIAL (r, rho_tor_norm) RECEPTION FOR EACH USEFUL IDS
        rho_tor_norm_core = []
        rho_tor_core = []
        #rho_tor_norm_refl = [] #n'existe pas dans la base
        for i in range(np.size(t_core)):
            rho_tor_norm_core+= [core_profile_imas_public.profiles_1d[i].grid.rho_tor_norm] #same as the one for imas_simu (JE SUPPOSE, A VERIFIER)
            rho_tor_core+= [core_profile_imas_public.profiles_1d[i].grid.rho_tor]
        rho_tor_norm_core = np.transpose(np.array(rho_tor_norm_core))
        rho_tor_core = np.transpose(np.array(rho_tor_core))

        rho_tor_norm_equi = np.transpose(equilibrium.profiles_1d.rho_tor_norm)


        r_equi = np.transpose((equilibrium.profiles_1d.r_outboard - equilibrium.profiles_1d.r_inboard)/2)
        R_equi = np.transpose((equilibrium.profiles_1d.r_inboard + equilibrium.profiles_1d.r_outboard)/2)
        r_refl = reflectometer.position.r #check if it is the same definition as the r_equi above
        #interpolation du r pour core_profile, on pourrait aussi faire _a pour rho_tor_norm_refl mais bon...

        Te_core = []
        Ti_core = []
        ne_core = []
        for i in range(np.size(t_core)):
            Te_core+= [core_profile_imas_public.profiles_1d[i].electrons.temperature]
            Ti_core+= [core_profile_imas_simu.profiles_1d[i].ion[0].temperature] 
            ne_core+= [core_profile_imas_public.profiles_1d[i].electrons.density]
        Te_core = np.transpose(np.array(Te_core))
        Ti_core = np.transpose(np.array(Ti_core))
        ne_core = np.transpose(np.array(ne_core))

        q_equi = np.transpose(equilibrium.profiles_1d.q)
        magn_shear_equi = np.transpose(equilibrium.profiles_1d.magnetic_shear)
        Rmag_equi = equilibrium.vacuum_toroidal_field.r0 #a vérifier: pas utile à priori, c'est une constante
        Bmag_equi = equilibrium.vacuum_toroidal_field.b0
        #B_tor_equi = equilibrium.b_tor_val
        a_equi = equilibrium.boundary.minor_radius

        
        
        elong_equi = np.transpose(equilibrium.profiles_1d.elongation) #difference with equilibrium.profiles_1d.elongation ???
        triang_up_equi = np.transpose(equilibrium.profiles_1d.triangularity_upper)
        triang_low_equi = np.transpose(equilibrium.profiles_1d.triangularity_lower)
        beta_norm_equi = np.transpose(equilibrium.global_quantities.beta_normal)
        beta_tor_equi = np.transpose(equilibrium.global_quantities.beta_tor)
        psi_equi = equilibrium.profiles_1d.psi
        #psin_equi = np.transpose(np.divide(psi_equi, psi_equi[:,0][:,np.newaxis])) #on normalise par la première colonne de la matrice
        psin_equi = np.transpose(np.divide((psi_equi - psi_equi[:,[0]]), (psi_equi[:,[100]]-psi_equi[:,[0]])))
        z_eff_res_core = np.transpose(core_profile_imas_simu.global_quantities.z_eff_resistive)
        
        ne_refl = reflectometer.n_e.data
        psi_refl = reflectometer.position.phi
        
        #Fevre: à ajouter plus tard dans le dico
        cmd = 'module load tools_dc;matlab -nodisplay -nosplash -nodesktop -r "run(\'script_test.m\');";exit;'
        
        self.write_script_m(shot_number)
        subprocess.run(cmd, shell = True, text = True)
        self.reception_in_txt()
        print('ERREUR FEVRE: ', self.erreur_fevre)
        if self.erreur_fevre:
            self.tableau_erreur_fevre += [shot_number]
            self.t_fevre = np.array([])
            self.ne_fevre = np.array([])
            self.psin_fevre = np.array([])
            self.a_fevre = []
            self.r_fevre = []
            self.R_fevre = []
            Bmag_fevre = []
            Te_fevre = []
        else:      
            self.a_fevre = self.interpolation_1D(t_equi, self.t_fevre, a_equi)
            self.r_fevre = self.interpolation_2D(t_equi, self.t_fevre, r_equi, self.psin_fevre)
            self.R_fevre = self.interpolation_2D(t_equi, self.t_fevre, R_equi, self.psin_fevre)
            
            Bmag_fevre  = self.interpolation_1D(t_equi, self.t_fevre, Bmag_equi)
            
            Te_fevre = self.interpolation_2D(t_core, self.t_fevre, Te_core, self.psin_fevre)
        
        #dico qui contiendra toute la data utile aux caculs des valeurs adim : le dico n'est pas utile en lui même, seulement si on veut stocker et optimiser la mémoire
        #dans le futur: stocker cette data dans des json pour ne pas perdre de temps à les récupérer
        #faut aussi considérer les plateaus donc pas si simple que ça le stockage...
        #je pense que juste les plateaus comme je faisais avant sont suffisant, ici on gagne juste du temps de calcul
        #peut être encore plus optimiser avec iw.partialGet(shot, 'data_path')...



        self.dico_data =  {
        "plateau_number" : self.plateau_number,
        "t_igni" : self.t_igni,
        "t_ini" : self.t_ini,
        "t_fin" : self.t_fin,
        "erreur_fevre" : self.erreur_fevre,
        "t_core": t_core.tolist(), 
        "t_equi": t_equi.tolist(), 
        "t_refl": t_refl.tolist(), 
        "rho_tor_norm_core": rho_tor_norm_core.tolist(), 
        "rho_tor_norm_equi": rho_tor_norm_equi.tolist(), 
        "rho_tor_core": rho_tor_core.tolist(),
        "r_equi" : r_equi.tolist(),
        "R_equi" : R_equi.tolist(),
        "r_refl" : r_refl.tolist(),
        "Te_core" : Te_core.tolist(),
        "Ti_core" : Ti_core.tolist(),
        "ne_core" : ne_core.tolist(),
        "q_equi" : q_equi.tolist(),
        "Rmag_equi" : Rmag_equi,
        "Bmag_equi" : Bmag_equi.tolist(),
        #"B_tor_equi" : B_tor_equi.tolist(),
        "a_equi" : a_equi.tolist(),
        "magn_shear_equi" : magn_shear_equi.tolist(),
        "elong_equi" : elong_equi.tolist(),
        "triang_up_equi" : triang_up_equi.tolist(),
        "triang_low_equi" : triang_low_equi.tolist(),
        "beta_norm_equi" : beta_norm_equi.tolist(),
        "beta_tor_equi" : beta_tor_equi.tolist(),
        "z_eff_res_core" : z_eff_res_core.tolist(),
        "ne_refl" : ne_refl.tolist(),
        "ne_fevre": self.ne_fevre.tolist(),
        "psin_fevre" : self.psin_fevre.tolist(),
        "psin_equi" : psin_equi.tolist(),
        "t_fevre": self.t_fevre.tolist() 
    
        }
        
        r_core = self.interpolation_2D(t_equi, t_core, r_equi, rho_tor_norm_core)
        R_core = self.interpolation_2D(t_equi, t_core, R_equi, rho_tor_norm_core)
        q_core = self.interpolation_2D(t_equi, t_core, q_equi, rho_tor_norm_core)
        a_core = self.interpolation_1D(t_equi, t_core, a_equi)     
        Bmag_core = self.interpolation_1D(t_equi, t_core, Bmag_equi)
        
        # ,
        # "ne_DREFRAP_fevre" : self.ne_DREFRAP_fevre,
        # "ne_DREFRAP_fit_fevre" : self.ne_DREFRAP_fit_fevre,
        # "t_fevre" : self.t_fevre,
        # "psin_fevre" : self.psin_fevre,
        # "rho_tor_norm_fevre" : self.rho_tor_norm_fevre,
        # "ne_profile_fevre" : self.ne_profile_fevre


        self.t_core = t_core
        self.t_equi = t_equi
        self.t_refl = t_refl
        self.rho_tor_norm_core = rho_tor_norm_core
        self.rho_tor_norm_equi = rho_tor_norm_equi
        self.rho_tor_core = rho_tor_core
        self.r_core = r_core
        self.R_core = R_core
        self.r_equi = r_equi
        self.R_equi = R_equi
        self.r_refl = r_refl
        self.psin_equi = psin_equi
        self.Te_core = Te_core
        self.Ti_core = Ti_core
        self.ne_core = ne_core
        self.q_equi = q_equi
        self.q_core = q_core
        self.Rmag_equi = Rmag_equi
        self.Bmag_equi = Bmag_equi
        self.Bmag_core = Bmag_core
        #self.B_tor_equi = B_tor_equi
        self.a_equi = a_equi
        self.a_core = a_core
        self.magn_shear_equi = magn_shear_equi
        self.elong_equi = elong_equi
        self.triang_up_equi = triang_up_equi
        self.triang_low_equi = triang_low_equi
        self.ne_refl = ne_refl
        self.beta_norm_equi = beta_norm_equi
        self.beta_tor_equi = beta_tor_equi
        self.z_eff_res_core = z_eff_res_core
        self.Te_fevre = Te_fevre
        self.Bmag_fevre = Bmag_fevre

        
        # pas utile car déjà fait dans la fonction reception_in_txt()
        # self.ne_DREFRAP_fevre = ne_DREFRAP_fevre
        # self.ne_DREFRAP_fit_fevre = ne_DREFRAP_fit_fevre
        # self.t_fevre = t_fevre 
        # self.r_fevre = r_fevre
        # self.rho_tor_norm_fevre = rho_tor_norm_fevre
    
        # return dico_data

    def receive_shot_json_data(self, shot_number,  dico_data):
        
        self.plateau_number = dico_data['plateau_number']
        self.t_igni = dico_data['t_igni']
        self.t_ini = dico_data['t_ini']
        self.t_fin = dico_data['t_fin']
        self.t_core =  np.array(dico_data['t_core'])
        self.t_equi =  np.array(dico_data['t_equi'])
        self.t_refl =  np.array(dico_data['t_refl'])
        self.erreur_fevre = dico_data['erreur_fevre']
        self.rho_tor_norm_core =  np.array(dico_data['rho_tor_norm_core'])
        self.rho_tor_norm_equi =  np.array(dico_data['rho_tor_norm_equi'])
        self.rho_tor_core =  np.array(dico_data['rho_tor_core'])
        self.r_equi = np.array(dico_data['r_equi'] )
        self.R_equi = np.array(dico_data['R_equi'] )
        self.r_refl =  np.array(dico_data['r_refl'])
        self.Te_core = np.array(dico_data['Te_core'] )
        self.Ti_core = np.array(dico_data['Ti_core'] )
        self.ne_core = np.array(dico_data['ne_core'] )
        self.q_equi = np.array(dico_data['q_equi']) 
        self.Rmag_equi = dico_data['Rmag_equi'] 
        self.Bmag_equi = np.array(dico_data['Bmag_equi'])
        #self.B_tor_equi = np.array(dico_data['B_tor_equi'])
        self.a_equi = np.array(dico_data['a_equi']) 
        self.magn_shear_equi = np.array(dico_data['magn_shear_equi'] )
        self.elong_equi = np.array(dico_data['elong_equi'] )
        self.triang_up_equi = np.array(dico_data['triang_up_equi'] )
        self.triang_low_equi = np.array(dico_data['triang_low_equi']) 
        self.ne_refl = np.array(dico_data['ne_refl'] )
        self.beta_norm_equi = np.array(dico_data['beta_norm_equi'])
        self.beta_tor_equi = np.array(dico_data['beta_tor_equi'])
        self.z_eff_res_core = np.array(dico_data['z_eff_res_core'])
        self.ne_fevre = np.array(dico_data['ne_fevre'])
        self.t_fevre = np.array(dico_data['t_fevre'])
         #self.ne_DREFRAP_fit_fevre = np.array(dico_data['ne_DREFRAP_fit_fevre'])
        self.psin_fevre = np.array(dico_data['psin_fevre']) 
        #self.rho_tor_norm_fevre = np.array(dico_data['rho_tor_norm_fevre'])
        self.psin_equi = np.array(dico_data['psin_equi'])
        

        self.r_core = self.interpolation_2D(self.t_equi, self.t_core, self.r_equi, self.rho_tor_norm_core)
        self.R_core = self.interpolation_2D(self.t_equi, self.t_core, self.R_equi, self.rho_tor_norm_core)
        self.q_core = self.interpolation_2D(self.t_equi, self.t_core, self.q_equi, self.rho_tor_norm_core)
        self.a_core = self.interpolation_1D(self.t_equi, self.t_core, self.a_equi)
        self.Bmag_core = self.interpolation_1D(self.t_equi, self.t_core, self.Bmag_equi)
        self.Bmag_fevre  = self.interpolation_1D(self.t_equi, self.t_fevre, self.Bmag_equi)
        print(self.erreur_fevre)
        if self.erreur_fevre:
            self.tableau_erreur_fevre += [shot_number]
            self.t_fevre = np.array([])
            self.ne_fevre = np.array([])
            self.psin_fevre = np.array([])
            self.a_fevre = []
            self.r_fevre = []
            self.R_fevre = []
        
            self.Bmag_fevre = []
            self.Te_fevre = []
        else:      
            self.a_fevre = self.interpolation_1D(self.t_equi, self.t_fevre, self.a_equi)
            self.r_fevre = self.interpolation_2D(self.t_equi, self.t_fevre, self.r_equi, self.psin_fevre)
            self.R_fevre = self.interpolation_2D(self.t_equi, self.t_fevre, self.R_equi, self.psin_fevre)
            self.Bmag_fevre  = self.interpolation_1D(self.t_equi, self.t_fevre, self.Bmag_equi)
            self.Te_fevre = self.interpolation_2D(self.t_core, self.t_fevre, self.Te_core, self.psin_fevre)

    def data_computation(self, shot_number):

        self.plateau_number_init = self.plateau_number
        self.nombre_plateaus_total += self.plateau_number
        print("numéro du shot: ", shot_number)
        #calcul des valeurs adim
        mean_std_r_l_te_Aaron = self.R_on_L_Te_plateaus_Aaron(shot_number)
        mean_std_r_l_ne = self.R_on_L_ne_plateaus(shot_number, )
        mean_std_r_l_ti = self.R_on_L_Ti_plateaus(shot_number)
        mean_std_ti_te = self.Ti_on_Te_plateaus(shot_number)
        mean_std_q = self.q_plateaus(shot_number)
        mean_std_magn_shear = self.magnetic_shear_plateaus(shot_number)
        mean_std_elong = self.elongation_plateaus(shot_number)
        mean_std_triang_up = self.triang_up_plateaus(shot_number)
        mean_std_triang_low = self.triang_low_plateaus(shot_number)
        mean_std_beta_tor = self.beta_tor_calcul_plateaus(shot_number)
        mean_std_z_eff_res = self.z_eff_res_plateaus(shot_number)
        mean_std_x = self.x_plateaus(shot_number)
        mean_std_collisionality = self.collisionality_plateaus(shot_number)
        mean_std_rR = self.rR_plateaus(shot_number)

        print("nombre de plateau: ",self.plateau_number)
        
        if not self.erreur_fevre:   
            print("FEVRE")
            mean_std_r_l_ne_fevre = self.R_on_L_ne_FEVRE_plateaus(shot_number)
            #mean_std_r_l_ne_fevre_corrected = self.R_on_L_ne_FEVRE_corrected_plateaus(shot_number)
            
            mean_std_beta_tor_fevre = self.beta_tor_calcul_fevre_plateaus(shot_number)
            #mean_std_collisionality_fevre = self.collisionality_plateaus_FEVRE(shot_number)
            
            #mise à jour du nombre de plateaus avec Fèvre:
            self.nbr_plateau_fevre += self.plateau_number
            print("nombre de plateau fevre: ",self.plateau_number)
            self.plateau_number = self.plateau_number_init

            self.erreur_fevre = False
        else:
            mean_std_r_l_ne_fevre = [[[np.nan , np.nan] for plat in range(self.plateau_number)] for r in range(self.nbr_rayon_utile)]
            mean_std_beta_tor_fevre = [[[np.nan , np.nan] for plat in range(self.plateau_number)] for r in range(self.nbr_rayon_utile)]
            #mean_std_collisionality_fevre = [[[np.nan , np.nan] for plat in range(self.plateau_number)] for r in range(self.nbr_rayon_utile)]
            
        
        #stockage des moyennes/écart-types
        mean_std = []
        mean_std += [mean_std_r_l_te_Aaron]
        mean_std += [mean_std_r_l_ne]
        mean_std += [mean_std_r_l_ti]
        mean_std += [mean_std_ti_te]
        mean_std += [mean_std_q]
        mean_std += [mean_std_magn_shear]
        mean_std += [mean_std_elong]
        mean_std += [mean_std_triang_up]
        mean_std += [mean_std_triang_low]
        mean_std += [mean_std_beta_tor]
        mean_std += [mean_std_z_eff_res]
        mean_std += [mean_std_collisionality]
        mean_std += [mean_std_x]
        mean_std += [mean_std_rR]
        
        mean_std += [mean_std_r_l_ne_fevre]
        #mean_std += [mean_std_r_l_ne_fevre_corrected]
        mean_std += [mean_std_beta_tor_fevre]
        #mean_std += [mean_std_collisionality_fevre]


        self.nbr_input_param = len(mean_std)

        return mean_std

    def different_rayons_hist(self, dico_mean_std, int_adimensional_parameter):
        mean_std = self.total_plateaus_par_rayon_dico(dico_mean_std)
        lines, cols = self.decoupage_ligne_colonne(self.nbr_rayon_utile)
        fig, axs = plt.subplots(lines,cols)
        ind_liste = self.indices_des_rayons_utiles(np.array(self.rayons_norm), 1) #ceci est une approximation, je suppose que je suis sûr les mêmes rayons peu importe la calcul du paramètre adim calculé
        if lines !=1:
            for i in range(lines):
                for j in range(cols):
                    k = i*cols + j
                    if k < self.nbr_rayon_utile:
                        axs[i, j].hist(mean_std[int_adimensional_parameter][k][0], edgecolor = "k")
                        axs[i, j].set_title(r"$r_{minor}$= " + str(self.rayons_norm[k]))
                        self.print_bound_qlknn(axs[i, j], int_adimensional_parameter)

        else:
            for k in range(cols):
                axs[k].hist(mean_std[int_adimensional_parameter][k][0], edgecolor = "k")
                titre = r"$\x  $= " + str(self.rayons_norm[k])
                axs[k].set_title(titre)
                self.print_bound_qlknn(axs[k], int_adimensional_parameter)


        
        fig.suptitle("Histogrammes des différents plateaus mean pour les "+str(self.nbr_rayon_utile)+" rayons\n" + self.name_hist[int_adimensional_parameter])

        plt.show()

    def different_rayons_en_un_hist(self, dico_mean_std, int_adimensional_parameter):
        mean_std = self.total_plateaus_par_rayon_dico(dico_mean_std)
        tot_mean = []
        tot_std = []
        for rayon in range(self.nbr_rayon_utile):
            tot_mean += mean_std[int_adimensional_parameter][rayon][0]
            tot_std += mean_std[int_adimensional_parameter][rayon][1]
        max_mean = np.max(tot_mean)
        min_mean = np.min(tot_mean)
        fig, ax = plt.subplots()
        #plot les hist en stackant les datas de chaque rayons
        ax.hist([mean_std[int_adimensional_parameter][i][0] for i in range(self.nbr_rayon_utile)], stacked = True, histtype = 'barstacked', range = (min_mean, max_mean), bins= self.bins_hist, alpha= 1., label= ['$x_{' + str(i) + '}$' for i in range(self.nbr_rayon_utile)], color=[self.color_list[i] for i in range(self.nbr_rayon_utile)], edgecolor = "k")
        #bound printing:
        self.print_bound_qlknn(ax, int_adimensional_parameter)

        # ax.xlabel(self.name_hist[int_adimensional_parameter])
        # ax.ylabel(r'# account')
        fig.suptitle(self.name_hist[int_adimensional_parameter])

        ax.legend(loc='upper right')
        # ax.tight_layout() #faudrait trouver l'équivalent de plt.tight_layout avec ax

        plt.show()

    def print_all_hist(self, dico_mean_std):

        self.line_hist, self.col_hist = self.decoupage_ligne_colonne(self.nbr_input_param)
        self.fig, self.axs = plt.subplots(self.line_hist, self.col_hist, 
                                          gridspec_kw= {'height_ratios' : [1.5 for i in range(self.line_hist)]},
                                          figsize = (8,6))
        mean_std = self.total_plateaus_dico(dico_mean_std)
        
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

        [mean_rlte_Aaron, std_rlte_Aaron] = mean_std[0]
        [mean_rlne, std_rlne] = mean_std[1]
        [mean_rlti, std_rlti] = mean_std[2] 
        [mean_ti_te, std_ti_te] = mean_std[3]   
        [mean_q, std_q] = mean_std[4] 
        [mean_magn_shear, std_magn_shear] = mean_std[5]
        [mean_elong, std_elong] = mean_std[6] 
        [mean_triang_up, std_triang_up] = mean_std[7] 
        [mean_triang_low, std_triang_low] = mean_std[8] 
        [mean_beta_tor, std_beta_tor] = mean_std[9]
        [mean_z_eff_res, std_z_eff_res] = mean_std[10]
        [mean_collisionality, std_collisionality] = mean_std[11]
        [mean_x, std_x] = mean_std[12]
        [mean_rR, std_rR] = mean_std[13]
        
        [mean_rlne_fevre, std_rlne_fevre] = mean_std[14]
        
        #[mean_rlne_fevre_corrected, std_rlne_fevre_corrected] = mean_std[15]
        #[mean_beta_fevre, std_beta_fevre] = mean_std[16]

        
        self.name_hist = [r'$\frac{-R*\nabla T_{e}}{T_e}$:' + '\n imas_public grad with r', 
                     r'$\frac{-R*\nabla n_{e}}{n_e}$' +'\nimas_public',
                     r'$\frac{-R*\nabla T_{i}}{T_i}$' +'\nimas_simu',
                     r'$\frac{T_i}{T_e}$' +'\nimas public + imas_simu',
                     r'$q$' +'\nequilibrium',
                     r'magnectic shear' +'\nequilibrium',
                     r'elongation' +'\nequilibrium',
                     r'triangularity up' +'\neqnbruilibrium',
                     r'triangularity low' +'\nequilibrium',
                     r'beta tor' +'\nequilibrium',
                     r'$Z_{eff}$ resistif' +'\ncp:imas_simu',
                     r'$log(\nu^{*})$' +'\ncp:imas_simu + imas_core',
                     'x=r/a\nequi',
                     'r/R' + '\nequi',
                     r'$\frac{-R*\nabla n_{e}}{n_e}$' +'Fevre' + '\nPlateaus: ' 
                     + str(self.nbr_plateau_fevre) + 'x' + str(self.nbr_rayon_utile) +
                     ' = '+ str(self.nbr_plateau_fevre*self.nbr_rayon_utile),
                     r'$\frac{-R*\nabla n_{e}}{n_e}$' +'Fevre corrected' + '\nPlateaus: ' 
                     + str(self.nbr_plateau_fevre) + 'x' + str(self.nbr_rayon_utile) +
                     ' = '+ str(self.nbr_plateau_fevre*self.nbr_rayon_utile),
                     r'beta tor ' +'\nFevre',
                      r'$log(\nu^{*})$' +'\nFevre']
        

        # self.print_hist(mean_rlte_Aaron, std_rlte_Aaron, self.name_hist[0]) 
        # self.print_hist(mean_rlne, std_rlne, self.name_hist[1])  
        # self.print_hist(mean_rlti, std_rlti, self.name_hist[2])  
        # self.print_hist(mean_ti_te, std_ti_te, self.name_hist[3])  
        # self.print_hist(mean_q, std_q, self.name_hist[4])
        # self.print_hist(mean_magn_shear, std_magn_shear, self.name_hist[5])
        # self.print_hist(mean_elong, std_elong, self.name_hist[6])
        # self.print_hist(mean_triang_up, std_triang_up, self.name_hist[7])
        # self.print_hist(mean_triang_low, std_triang_low, self.name_hist[8])  
        # self.print_hist(mean_beta_tor, std_beta_tor, self.name_hist[9])  
        # self.print_hist(mean_z_eff_res, std_z_eff_res, self.name_hist[10])  
        # self.print_hist(mean_collisionality, std_collisionality, self.name_hist[11])  
        # self.print_hist(mean_x, std_x, self.name_hist[12])  
        #self.print_hist(mean_rR, std_rR, self.name_hist[13])

        #self.print_hist(mean_rlne_fevre, std_rlne_fevre, self.name_hist[14])
        

        self.print_hist_color(dico_mean_std, 0, self.name_hist[0]) 
        self.print_hist_color(dico_mean_std, 1, self.name_hist[1])  
        self.print_hist_color(dico_mean_std, 2, self.name_hist[2])  
        self.print_hist_color(dico_mean_std, 3, self.name_hist[3])  
        self.print_hist_color(dico_mean_std, 4, self.name_hist[4])
        self.print_hist_color(dico_mean_std, 5, self.name_hist[5])
        self.print_hist_color(dico_mean_std, 6, self.name_hist[6])
        self.print_hist_color(dico_mean_std, 7, self.name_hist[7])
        self.print_hist_color(dico_mean_std, 8, self.name_hist[8])  
        self.print_hist_color(dico_mean_std, 9, self.name_hist[9])  
        self.print_hist_color(dico_mean_std, 10, self.name_hist[10])  
        self.print_hist_color(dico_mean_std, 11, self.name_hist[11])  
        self.print_hist_color(dico_mean_std, 12, self.name_hist[12]) 
        self.print_hist_color(dico_mean_std, 13, self.name_hist[13])

        self.print_hist_color(dico_mean_std, 14, self.name_hist[14])
        self.print_hist_color(dico_mean_std, 15, self.name_hist[15])

        #self.print_hist_color(dico_mean_std, 16, self.name_hist[16])

        #self.print_hist_color(dico_mean_std, 16, self.name_hist[16])

        self.axs[0,0].hist([[] for i in range(self.nbr_rayon_utile)], 
                      label= ['$x_{' + str(i) + '}$' for i in range(self.nbr_rayon_utile)],
                      color=[self.color_list[i] for i in range(self.nbr_rayon_utile)])
        self.axs[0,0].hist([], color = 'r', linestyle = '--', label='QLKNN core')
        self.axs[0,0].hist([], color = 'b', linestyle = '--', label='QLKNN edge')
        
        self.fig.legend(loc = "upper right")
        
        titre = "Plateaus number: " + str(self.nombre_plateaus_total) +  " x " +  str(self.nbr_rayon_utile) + " radial points = "+ str(self.nbr_rayon_utile*self.nombre_plateaus_total) + "\nNumber of shot computed: " + str(self.nbr_shot)    
        # titre = "Nombre de plateaus: " + str(self.nombre_plateaus_total
        # "erreur_fevre" : self.erreur_fevre,) +  " x " +  str(self.nbr_rayon_utile) + " points radiaux = "+ str(self.nbr_rayon_utile*self.nombre_plateaus_total) + "\nNombre de shot calculés: " + str(self.nbr_shot) + " nb of shot with issues: " + str(len(self.shot_problematic)) 
        print(titre)
        self.fig.suptitle(titre)
        plt.subplots_adjust(hspace= 0.5)

        

        plt.show() 

    def print_hist(self, mean, std, nom_hist):
        #On prépare l'affichage
        #plt.subplot(nbr_lines,nbr_columns,num_hist)
        if self.line_hist == 1:
            ax = self.axs[self.ind_col_hist]
        else:
            ax = self.axs[self.ind_line_hist , self.ind_col_hist]
        ax.hist(mean, bins= self.bins_hist, alpha= 1., label= '$mean$', color='c', edgecolor='k')
        
        #plt.hist(mean + np.abs(std), bins= 50, alpha= 1., label= '$mean+|std|$', color='pink')
        ax.set_title(nom_hist)
        ax.legend(loc='upper right')
        if self.ind_col_hist != self.col_hist-1:
            self.ind_col_hist += 1
        else:
            self.ind_line_hist += 1
            self.ind_col_hist = 0
            
    

    def print_hist_color(self, dico_mean_std, int_adimensional_parameter, nom_hist):
        #On prépare l'affichage
        #plt.subplot(nbr_lines,nbr_columns,num_hist)
        mean_std = self.total_plateaus_par_rayon_dico(dico_mean_std)
        try:
            tot_mean = []
            tot_std = []
            for rayon in range(self.nbr_rayon_utile):
                tot_mean += mean_std[int_adimensional_parameter][rayon][0]
                tot_std += mean_std[int_adimensional_parameter][rayon][1]
            
            max_mean = np.max(tot_mean)
            min_mean = np.min(tot_mean)
        


            if self.line_hist == 1:
                ax = self.axs[self.ind_col_hist]
            else:
                ax = self.axs[self.ind_line_hist , self.ind_col_hist]
            
            ax.hist([mean_std[int_adimensional_parameter][i][0]  for i in range(self.nbr_rayon_utile)], stacked = True, 
                    histtype = 'barstacked', range = (min_mean, max_mean), bins= self.bins_hist, alpha= 1., 
                    #label= ['$x_{' + str(i) + '}$' for i in range(self.nbr_rayon_utile)], 
                    color=[self.color_list[i] for i in range(self.nbr_rayon_utile)])

            #bound printing:
            self.print_bound_qlknn(ax, int_adimensional_parameter)

            #plt.hist(mean + np.abs(std), bins= 50, alpha= 1., label= '$mean+|std|$', color='pink')
            ax.set_title(nom_hist)
            #ax.legend(loc='upper right')
        except:
            print("pas de valeur pour le param adim: ", int_adimensional_parameter)
        if self.ind_col_hist != self.col_hist-1:
            self.ind_col_hist += 1
        else:
            self.ind_line_hist += 1
            self.ind_col_hist = 0

    def print_bound_qlknn(self, ax, int_adimensional_parameter):
        if str(int_adimensional_parameter) in self.core_bound_dico:
            core_bound = self.core_bound_dico[str(int_adimensional_parameter)]
            if np.size(core_bound) == 2:
                min_core_bound = core_bound[0]
                max_core_bound = core_bound[1]

                ax.axvline(min_core_bound, color = 'r', linestyle = '--')
                ax.axvline(max_core_bound, color = 'r', linestyle = '--')
            else: #shape == 1
                ax.axvline(core_bound[0], color = 'r', linestyle = '--')

            edge_bound = self.edge_bound_dico[str(int_adimensional_parameter)]
            if np.size(edge_bound) == 2:
                min_edge_bound = edge_bound[0]
                max_edge_bound = edge_bound[1]
                
                ax.axvline(min_edge_bound, color = 'b', linestyle = '--')
                ax.axvline(max_edge_bound, color = 'b', linestyle = '--')
            else: #shape == 1
                ax.axvline(edge_bound[0], color = 'b', linestyle = '--')
        



    def cumulation_shot_mean_std(self, mean_std, mean_std_to_add):
        nbr_input_param = len(mean_std)
        for i in range(nbr_input_param):
            mean_std[i][0] += mean_std_to_add[i][0] #ajout des means
            mean_std[i][1] += mean_std_to_add[i][1] #ajout des std
        return(mean_std)
    
    def value_filter(self, int_adimensional_parameter, value):
        if int_adimensional_parameter == 3: #cas ti/te
            for val in value:
                if val<0: #ce filtre marche moyen quand je mets deux conditions
                    return False
                elif val>2:
                    return False
        if int_adimensional_parameter == 1:
            for val in value:
                if val>150:
                    return False
        if int_adimensional_parameter == 2:
            for val in value:
                if val>150:
                    return False
        return True
            

    #création des deux fonctions qui permettent l'affichage des histo sur l'entièreté du dico et vire les nan résiduels
    def total_plateaus_par_rayon_dico(self, dico_mean_std):
        tot_mean_std_rayons = [[[[], []] for j in range (self.nbr_rayon_utile)] for i in range(self.nbr_input_param)]
        for shot, mean_std in dico_mean_std.items():
            for j in range(self.nbr_input_param):
                for i in range(self.nbr_rayon_utile):
                    if not np.isnan(mean_std[j][i][0]).any():
                        if self.value_filter(j,mean_std[j][i][0]):
                            tot_mean_std_rayons[j][i][0] += mean_std[j][i][0] #mean 
                            tot_mean_std_rayons[j][i][1] += mean_std[j][i][1] #std

        return tot_mean_std_rayons

    def total_plateaus_dico(self, dico_mean_std):
        tot_mean_std_rayons = [[[],[]] for i in range(self.nbr_input_param)]
        for shot, mean_std in dico_mean_std.items():
            for j in range(self.nbr_input_param):
                for i in range(self.nbr_rayon_utile):
                    #print("i, j:", i, j)
                    if not np.isnan(mean_std[j][i][0]).any():
                        tot_mean_std_rayons[j][0] += mean_std[j][i][0] #mean
                        tot_mean_std_rayons[j][1] += mean_std[j][i][1] #std

        return tot_mean_std_rayons

    #####

    def all_campaign_hist(self, display = True, save = False, name_json_file = "campaign_data"):
        self.initialisation_stat()
        return self.shot_list_hist(self.shot_list, display = display, save = save, name_json_file = name_json_file)

    def random_shot_list_hist(self, number_of_shot, display = True, save = False, name_json_file = "list_shot_data"):
        self.initialisation_stat()
        shot_list = []
        for i in range(number_of_shot):
            shot_list += [self.shot_list[np.random.randint(0, np.size(self.shot_list))]]
        return self.shot_list_hist(shot_list, display = display, save = save, name_json_file = name_json_file)

    def shot_list_hist(self, shot_list, display = False, save = False, name_json_file = "list_shot_data"):
        # dico_mean_std = {}
        self.initialisation_stat()
        shot_problematic = []
        self.nbr_shot = len(shot_list)
        shot_treatment = 1
        for shot in shot_list:
            print("shot:" + str(shot_treatment))
            shot_treatment += 1
            try:
                self.receive_shot_imas_data(shot)
                mean_std = self.data_computation(shot)
                self.dico_mean_std.update({str(shot) : mean_std})
                self.dico_all_data.update({str(shot) : self.dico_data})
                
            except:
                shot_problematic += [shot]
        if display:
            self.print_all_hist(self.dico_mean_std)
        print("Shot problematiques: ", shot_problematic)
        print("Shot problematiques de Fèvre: ", self.tableau_erreur_fevre)
        print("nombre de shot pb:", len(shot_problematic))
        print("shot Te pb: ", self.shot_Te_pb)

        if save:
            with open(name_json_file, 'w') as f:
                js.dump(self.dico_all_data, f)

        return self.dico_mean_std

    def specific_shot_hist(self, shot_number, display = False, save_data = False, name_json_file = "shot_data"):
        self.initialisation_stat() #inutile si on a déjà save la data dans un fichier json
        self.receive_shot_imas_data(shot_number)
        mean_std = self.data_computation(shot_number)
        self.dico_mean_std.update({str(shot_number) : mean_std})
        print(self.plateau_problematic)
        self.nombre_plateaus_total = self.plateau_number
        self.dico_all_data = {str(shot_number) : self.dico_data}

        if display:
            self.print_all_hist(self.dico_mean_std)

        if save_data:
            with open(name_json_file, 'w') as f:
                js.dump(self.dico_all_data, f)

        return self.dico_mean_std


    def saved_hist(self, display = False, name_saved_file = None):
        self.initialisation_stat() #utile que pour les analyses post-traitement comme mon analyse des courbes ne
        with open(name_saved_file) as f:
            self.dico_all_data = js.load(f)
        nb_tot_shot = len(self.dico_all_data)
        for shot, dico_data in self.dico_all_data.items():
            print("Shot traité:", self.nbr_shot, "/", nb_tot_shot)
            self.nbr_shot +=1
            self.receive_shot_json_data(shot, dico_data)
            
            #self.plateau_number = np.size(self.plateau[shot_number])
            mean_std = self.data_computation(shot)
            self.dico_mean_std.update({str(shot) : mean_std})

        if display:
            self.print_all_hist(self.dico_mean_std)

        return self.dico_mean_std

    def hist_from_saved_file(self, name_json_file, nbr_input_param):
        self.nbr_input_param = nbr_input_param
        with open(name_json_file) as f:
            self.dico_mean_std = js.load(f)
        self.print_all_hist(self.dico_mean_std)
            


    def on_click(self, event):
        x, y = event.x, event.y
        if event.key == 'control':
            if self.line_hist == 1:
                for j in range(self.col_hist):
                        if self.axs[j].contains(event)[0]:
                            #print('on est bien sur hist ', 1, j)
                            try:
                                self.different_rayons_hist(self.dico_mean_std, j)
                                self.different_rayons_en_un_hist(self.dico_mean_std, j)
                            except:
                                print("pas de data à analyser ici")
            else:
                for i in range(self.line_hist):
                    for j in range(self.col_hist):
                        if self.axs[i, j].contains(event)[0]:
                            # print('on est bien sur hist ', i, j)
                            
                            self.different_rayons_hist(self.dico_mean_std, i*self.col_hist+j)
                            self.different_rayons_en_un_hist(self.dico_mean_std, i*self.col_hist+j)
                            
                            print("pas de data à analyser ici")
        if event.key == 'shift':
            print("yes")
            I, J = 0, 0
            for i in range(self.line_hist):
                    for j in range(self.col_hist):
                        if self.axs[i, j].contains(event)[0]:
                            I = i
                            J = j
            print(I, J)
            fig_dialog, ax_dialog = plt.subplots()
            ax_dialog.axis('off')
            text_min = TextBox(ax_dialog, 'Minimum:')
            text_max = TextBox(ax_dialog, 'Maximum:')
            button_apply = Button(ax_dialog, 'Appliquer')
            
            def apply_limits(event):
                new_x_min = float(text_min.text)
                new_x_max = float(text_max.text)
                self.axs[I, J].set_xlim(new_x_min, new_x_max)
                
                plt.draw()
                
                #close the window dialog:
                #fig_dialog.canvas.mpl_disconnect(cid)
                fig_dialog.canvas.mpl_disconnect(cid)
                plt.close(fig_dialog)
            
            cid_apply = button_apply.on_clicked(apply_limits)
                
            
            
            plt.show()
                



#MAIN
campaign_name = 'C4'

shot_number = 54396 #shot that have no problem CAN COMPUTE
shot_number = 54459 #shot with few data for test
#shot_number = 55799 #shot that can be open but after problem during the reception of plateaus CANNOT COMPUTE (Pb during the StatsPlateau function)
#shot_number = 55588 #shot that cannot be opened :  IMAS file does not exist. CANNOT COMPUTE
#shot_number = 55046 #shot with Nan in the center (no in the edge) CAN COMPUTE
shot_number_fevre_problematic = 55787
shot_number_fevre = 54558
shot_list_test = [54459, 54459]
shot_list_test2 = [54396, 54459, 55799]

shot_number = 55192
shot_pb_Ti = 54395
shot_list = [55513, 54629, 54734]

rayons_norm = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8]
shot_problematique_Te = 54952
shot_fevre_problem = 55516

shot_pb = 54914
shot_list = [54524, 54369]

shot_list = [55192, 55979, 54369, 55223]
name_json_file_saved = "shots_listFevre" # 12 shots avec Fevre
name_json_file_saved = "50shots"
name_json_file_saved = "C4_campaign"

shot_grad_neg =  54369#shot avec grad négatif 

Hist = Histograms_(campaign_name, rayons_norm)
# Hist.shot_list_hist(shot_list_test2, True)
#dico = Hist.specific_shot_hist(55223, display = True, save_data = True, name_json_file= "shot_data")
#dico = Hist.saved_hist(display = True, name_saved_file="shot_data")
#Hist.specific_shot_saved_hist(shot_problematique_Te, True, "json_data")
#dico = Hist.shot_list_hist(shot_list, display = True, save = True, name_json_file="Test_shot_list")
#Hist.saved_hist(display = True, name_saved_file="Test_shot_list")

#dico = Hist.random_shot_list_hist(50, display = True, save = True, name_json_file="50shots")
dico = Hist.all_campaign_hist(display = True, save = True, name_json_file="C4_campaign_3")
#dico = Hist.saved_hist(display = True, name_saved_file="50shots")
#Hist.quel_shot(100, 3, dico)

