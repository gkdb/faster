%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Interferometry and Reflectometry to density profiles for WEST    %
%
% [out,maps]=INTREF2ne(nshot,options);
%               
% how it works: 
%   - extrapolate (DREFRAP) or combine (DREFLUC with DREFRAP) the reflectometry profiles to extend the profile over the whole low field side midplane
%    By default, only treats DREFLUC, but DREFRAP can be treated too (see flags)
%   - Compute the LID of the interferometers with the profiles given by  
%   - uses the prevous parameters to fit the INTERF profiles
%   - allows for a magnetic map compression from optimisation
%   - treatments are applied, fcorrb default, on the time basis of the DREFRAP reflectometry, even in burst mode. 
%   - To relax this, see option 'dt'. 
%   - fits the DREFRAP profiles with an mtanh fonction, two fits available
%   	  Fedorczak fit :
%    	  ne(PsiN) = n0(1+alpha(PsiP-PsiN))*exp((PsiP-PsiN)/Delta)/( exp((PsiP-PsiN)/Delta)+exp(-(PsiP-PsiN)/Delta)
% 	  (0.75<PsiP<1.25 and n0>0)
%	  Groebner fit : A*tanh((PSIP-PSIN)/(delta/2) + B + C*(PsiNref<psiknee).*(psiknee-PsiNref)
%	  with psiknee = PSIP-delta/2
%       
%
% INPUTS: 
%    nshot : shot number 
%    options: 
%        'tlim', [start,end]: [s] specify a time interval. Default is the union of EQUINOX, DREFRAP & INTERF time interval.
%                             reference is t=0.0s at ignitron if ignitron subtracted [default]
%        'dt', float        : [s] Defines a new time basis, DREFRAP profiles are then averaged in time on time interals of width 'dt'. 
%        'noignitron'       : [default none] time referenced from the absolute timing, no ignitron subtraction.  
%        'int2R', [int]     : [Default int2R=[1]] Removes specified interferometry channels for density inversion. 
% deactivated       'PSIN',[PSIN]      : [Default [0:0.01:1.3]]: specify a given coordinate vector along which density profile is returned as output.
%                             'PSIN','EQUINOX'   will use the EQUINOX grid for PSIN definition (from 0 to 1). 
% deactivated        'Zcompress',       : [default none] magnetic flux map is corrected fcorrb a vertical compression factor. 
%                             optimised during interferometry inversion
% deactivated        'VACTH'            : [default none] uses VACTH against EQUINOX to caclulate the compression factor. 
%                             takes over Zcompress option  
%        'plot',            : adds an interactive graph at the end to observe if the calculation has been successful.
% deactivated       'map'              : [default none] returns the time dynamics of 2D (R,Z) maps of magnetic flux and electron density from inversion
% deactivated       'RCP'              : [default none] returns the time dynamics of density profile at the reciprocating probe location   
%
%	'frap' 		    : [default off] not only treats DREFLUC, but also DREFRAP
%	'nocut'		    : [default off] doesn't do the truncation of the jump in the core to gain time
% 	'cutcompression     : [default off] does the compression of the map for the truncated profiles
% 	'groebner'          : [default off] turns on the groebner fit
% 	'fedoreczak'        : [default on]  turns off the fedorczak fit
%       'polarimetry map'   : [default off] get the magnetic equilibrium from IMAS with the polarimetry constraints; occ = 1
%       'channel'           : [default 3:10] given a list between 1 and 10, allows to choose which interferometry channels are considered by the compression
%	'fullrefl' 	    : by default, only timesteps with both DREFRAP and DREFLUC are kept, activating this keeps all the timesteps 
%  
% OUTPUTS:    
%  out
%     .flags               : =1 if treatment done, 0 otherwise
%     .t                  : [NT] array of time values in [s] 
%     .INTERF
%            .lid         : [NT,10] line integrated density [m-2] (single path) from imas for all 10 lines
%            .channel     : channel indices
%            .validity    : [NT,10] validity of lid data:
%                              = 1 if data are valid from imas (validity flags >-2) 
%                                  AND if the channel is required for inversion
%                              = 0 if the channel is not considered for inversion 
%                                  OR if an effective fringe jump is found on the time trace
%                                  (only time points after the jump are set to validity=0)
%            .lid_fit     : [NT,10] time traces of line integrated density [m-2] (single path) 
%                                   reconstructed from the density profile inversion 
%     .DREFRAP
%            .ne          : [NR,NT] local density [m-3] estimated by DREFRAP, from IMAS
%            .Z           : vartical position along the profile [m]
%            .R_old       : [NR,NT] local major radius [m] estimated by DREFRAP, from IMAS
%            .R           : [NR,NT] local major radius [m] corrected by the inversion.
%                           consists in a rigid shift of the original position R_old, at each time step
%            .PSIN_old    : [NR,NT] local normalized flux coordinate given by EQUINOX, from IMAS
%            .PSIN        : [NR,NT] local normalized flux coordinate corrected by the inversion
%            .fit         : parameters from the mtanh fit applied to DREFRAP profiles
%                .ne      : [NR,NT] local electron density [m-3] profile from the mtanh fit
%                .n0      : [NT] electron density [m-3] at the pedestal top
%                .alpha   : [NT] peaking factor for the core part
%                .PsiP    : [NT] pedestal center position in PSIN unit
%                .Delta   : [NT] pedestal width in PSIN unit
%                .Delta_R : [NT] pedestal width in major radius unit [m]
%                .validity: [NT] 3 = all values above are valid
%                                2 = alpha is not valid 
%                                1 = PsiP and Delta are valid
%                                0 = none of the values are valid
%                .R2      : [NT] R2 residual of the fit
%     .profile            
%            .PSIN        : [NP] rigid grid of normalized flux coordinate
%            .rho_tor_norm: [NP,t] normaized toroidal rho 
%            .ne          : [NP,NT] local density [m-3] from inversion
%            .ne_sep      : [NT] separatrix density [m-3] from inversion
%            .fit   
%                .n0      : [NT] electron density [m-3] at the pedestal top
%                .alpha   : [NT] peaking factor for the core part
%                .PsiP    : [NT] pedestal center position in PSIN unit
%                .Delta   : [NT] pedestal width in PSIN unit
%                .validity: [NT] 1 = inversion valid
%                                0 =  inversion not valid
%                .R2      : [NT] R2 residual of the inversion
%                .SQ      : [NT] vertical compression factor from the inversion or from VACTH
%     .param              : list of parameters   
%     .dZ0                : [NT] vertical shift of the plasma center [m] obtained from inversion or from VACTH
%     .RCP
%            .Z           : [NZ] vertical mesh in [m] at the top of the vessel
%            .R           : [1] major radius position of the RCP [m]
%            .PSIN        : [NZ,NT] normalized flux coordinate along the RCP profile, from corrected PSIN map
%            .ne          : [NZ,NT] local electron density [m-3] from inversion
%            .dZsep       : [NT] vertical shift [m] of the separatrix at the probe radius, from the inversion
%
%
%
% function used
% imas_west_get.m
%
% nicolas.fedorczak@cea.fr & pierre-louis.ardizzone@ens-lyon.fr
% last modification: 22/09/21


function [OUT,maps]=INTREF2ne(nshot,varargin)


param	                              = initialise_parameters(nshot,varargin);  %

[REFL_frap, REFL_fluc,param]  	      = read_reflectometry(nshot,param); %
if param.flags.refl==0; disp('no reflectometry data'); return;  end

[EQUI,param]                	      = read_magnetics(nshot,param); %
if EQUI.flags==0; disp('no magnetic data'); return; end

[INT,param]               	      = read_interferometry(nshot,param); %
if INT.flags==0; disp('no interferometry data'); return; end

[REFL_frap, REFL_fluc,EQUI,INT,param] = resample_data(REFL_frap,REFL_fluc, EQUI,INT,param); %
if param.flags.resample ==0; disp('no overlapping diagnostics'); return;end





% computation of PSIN from REFL.R
[REFL_frap, param]                   	  = reflectometry_PSIN(REFL_frap,EQUI, param); %
[REFL_fluc, param]                   	  = reflectometry_PSIN(REFL_fluc,EQUI, param); %

%extrapolation of the profile, eventual cutoff of the bump in the core
[REFL_tot_fluc, REFL_cut_fluc,seconde, param] = reflectometry_extrapolation(REFL_frap, REFL_fluc, param); %
if param.flags.frap
	[REFL_tot_frap, REFL_cut_frap]                =  reflectometry_extrapolation(REFL_frap, REFL_frap, param); %
end
%Computation of the LID from reflectometry and comparison with the one of interferometry
[REFL_tot_fluc, REFL_cut_fluc]                = interferometry_from_reflectometry(REFL_tot_fluc, REFL_cut_fluc, INT,EQUI, param); %

if param.flags.frap
	[REFL_tot_frap, REFL_cut_frap]                = interferometry_from_reflectometry(REFL_tot_frap, REFL_cut_frap, INT,EQUI, param); %
end

%optimisation of the LID with compression of the map (and optionally cutoff)

[REFL_tot_fluc]        	  = lid_PSIN_corr(REFL_tot_fluc, INT,EQUI, param);   %
if param.flags.REFL_cut & param.flags.REFL_cut_compression
	[REFL_cut_fluc] 		  = lid_PSIN_corr(REFL_cut_fluc, INT,EQUI, param);
end
if param.flags.frap
	[REFL_tot_frap]        	  = lid_PSIN_corr(REFL_tot_frap, INT,EQUI, param);   %
	if param.flags.REFL_cut & param.flags.REFL_cut_compression
		[REFL_cut_frap] 		  = lid_PSIN_corr(REFL_cut_frap, INT,EQUI, param);
	end
end

%fit in mtanh with fedorczak fit
if param.flags.fitfedorczak
	REFL_tot_fluc 		   	  = mtanh_fit(REFL_tot_fluc, param);%
	if param.flags.REFL_cut
		REFL_cut_fluc 		   	  = mtanh_fit(REFL_cut_fluc, param);%
	end
end
%fit in mtanh with groebner fit
if param.flags.fitgroebner
	REFL_tot_fluc                    	  = mtanh_fit_groebner(REFL_tot_fluc, param); %
	if param.flags.REFL_cut
		REFL_cut_fluc 		   	  = mtanh_fit_groebner(REFL_cut_fluc, param);%
	end
end


if param.flags.frap
	%fit in mtanh with fedorczak fit
	if param.flags.fitfedorczak
		REFL_tot_frap 		   	  = mtanh_fit(REFL_tot_frap, param);%
		if param.flags.REFL_cut
			REFL_cut_frap 		   	  = mtanh_fit(REFL_cut_frap, param);%
		end
	end
	%fit in mtanh with groebner fit
	if param.flags.fitgroebner
		REFL_tot_frap                  	  = mtanh_fit_groebner(REFL_tot_frap, param); %
		if param.flags.REFL_cut
			REFL_cut_frap 		   	  = mtanh_fit_groebner(REFL_cut_frap, param);%
		end
	end
end

%keyboard
if param.flags.plot
	
	
	scr=get(0,'screensize');
	scrx=scr(3);
	scry=scr(4);
	
	h1=figure('color',[1,1,1],'position',[scrx.*0.2 scry.*0.25 scrx*0.7 scry*0.5]);

	h2=subplot('position',[0.05,0.09,0.45,0.85],'fontsize',16);
	hold on
	box on
	grid on

	try delete(handle_pl) 
	end

	try clear('handle_pl')	
	end
	handle_pl(1) = plot(0, 0);

	xlim([-0.1,max(REFL_frap.PSIN(:))+0.1]);
	ylim([-0.5,10]);
	xlabel('PSIN')
	ylabel('ne [E19m-3]')
	h3=subplot('position',[0.55,0.09,0.43,0.85],'fontsize',16);
	hold on
	box on
	grid on
	xlim([1,INT.N]);
	ylim([0,max(INT.lid(:))*1.1]);
	paramfrap = 1;
	paramfluc = 1;
	paramfrapcorr = 1;
	paramfluccorr = 1;

	legend('DREFRAP', 'DREFLUC');
	NTsm=20;
	NT=length(param.t);

	key=[];
	set(h1,'KeyPressFcn',@(H,E) assignin('caller','key',E.Key));
	disp( '------------------------------------')
	disp( '| figure has to active windows     |')
	disp( '| leftarrow to go back     -1      |')
	disp( '| rightarrow to go further +1      |')
	disp(['| downarrow to go back   ' int2str(NTsm) ' steps |'])
	disp(['| uparrow to go further  ' int2str(NTsm) ' steps |'])
	disp( '| return to quit                   |')

	disp( '------------------------------------')

	it=1;
	encore=1;

	while encore
		ihandle = 1;
		delete(handle_pl);

		subplot(h2);
		%handle_pl(ihandle)(1)=plot(REFL_frap.PSIN(:,it),REFL_frap.ne(:,it)*1.e-19, 'b-');
		%handle_pl(ihandle)(2)=plot(REFL_fluc.PSIN(:,it),REFL_fluc.ne(:,it)*1.e-19, 'r-');
		if paramfrap
			handle_pl(ihandle) = plot(REFL_frap.PSIN(:, it) ,REFL_frap.ne(:,it)*1.e-19, '*g-');	
			%handle_pl(ihandle)(2) = plot(REFL_frap.PSIN, REFL_frap.neclean(:,it)*1.e-19, 'c');
			handle_pl(ihandle) = plot(0, 0);
		else
			handle_pl(ihandle) = plot(0,0, '*g-');	
			handle_pl(ihandle) = plot(0,0, 'c');
		end
		handle_pl(ihandle) = plot(REFL_fluc.PSIN(:, it), REFL_fluc.ne(:,it)*1.e-19, 'r');
		handle_pl(ihandle) = plot(REFL_fluc.PSIN(:, it), REFL_fluc.ne(:,it)*1.e-19, 'k');
		%handle_pl(ihandle)(4) = plot(0, 0);
		handle_pl(ihandle) = plot(0, 0); %plot(REFL_frap.PSIN(:, it), REFL_frap.ne(:,it)*1.e-19);
		%handle_pl(ihandle)(6) = plot(REFL_fluc.PSIN, REFL_fluc.fitnegroebner.ne(:,it)*1.e-19, 'c');
		handle_pl(ihandle) = plot(0, 0);		
		%handle_pl(ihandle)(8) = plot([0, 2], [(REFL_fluc.fitnegroebner.A(it)+REFL_fluc.fitnegroebner.B(it))*1e-19, (REFL_fluc.fitnegroebner.A(it)+REFL_fluc.fitnegroebner.B(it))*1e-19], 'm');	
		handle_pl(ihandle) = plot(0, 0);
		if 1% REFL_tot.PSIfrapclean(it)
			 %plot(seconde{it}.PSI(:, 2), seconde{it}.neseconde*100);
			handle_pl(ihandle) = plot(0, 0); %plot([REFL_tot.PSIfrapclean(it), REFL_tot.PSIfrapclean(it)], [0, 10]);
		else

			handle_pl(ihandle) = plot(0, 0);
		end
		legend('frap', 'frapclean', 'fluc', 'flucclean'); 
		%for k = 1:NR1
		%	try handle_pl(ihandle)(k+4) = plot(0, 0); %plot([REFL_frap.Rlbound(k, it),REFL_frap.Rubound(k, it)], [REFL_frap.ne(k,it)*1.e-19, REFL_frap.ne(k,it)*1.e-19], 'g');
		%	catch me
		%	handle_pl(ihandle)(k+4) = plot(0, 0);
		%	end					
		%end
		subplot(h3);
		if paramfrap
			handle_pl(ihandle) = plot(REFL_frap.lid(it,:),'g*-');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(REFL_frap.lid(it,:),'c*-');ihandle = ihandle+1;

		else
			handle_pl(ihandle) = plot(0, 0, 'g');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0, 'c');ihandle = ihandle+1;
		end
		if paramfluc
			handle_pl(ihandle) = plot(REFL_fluc.lid(it,:),'r*-');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(REFL_fluc.lid(it,:),'k*-');ihandle = ihandle+1;
		else
			handle_pl(ihandle) = plot(0, 0, 'r*-');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0, 'k*-');ihandle = ihandle+1;
		end
		handle_pl(ihandle) = plot(INT.lid(it,:),'bo-', 'linewidth', 3);ihandle = ihandle+1;
		
		if paramfrapcorr

			try handle_pl(ihandle) = plot(REFL_frap.lidcorr(it,:),'go-');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(REFL_frap.lidcleancorr(it,:),'co-');ihandle = ihandle+1;
			catch me
			handle_pl(ihandle) = plot(0, 0, 'r.');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0);ihandle = ihandle+1;
			end
		else
			handle_pl(ihandle) = plot(0, 0, 'r.');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0);ihandle = ihandle+1;
		end
		if paramfluccorr
			try handle_pl(ihandle) = plot(REFL_fluc.lidcorr(it,:),'ro-', 'linewidth', 3);ihandle = ihandle+1;
			handle_pl(ihandle) = plot(REFL_fluc.lidcleancorr(it,:),'ko-');ihandle = ihandle+1;
			catch me
			handle_pl(ihandle) = plot(0, 0, 'r.');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0);ihandle = ihandle+1;
			end
		else
			handle_pl(ihandle) = plot(0, 0, 'r.');ihandle = ihandle+1;
			handle_pl(ihandle) = plot(0, 0);ihandle = ihandle+1;
		end
		legend('FRAP', 'FRAP clean', 'FLUC', 'FLUC clean', 'INT', 'FRAPcorr', 'FRAPcorrclean', 'FLUCcorr', 'FLUCcorrclean');
			
		title([int2str(nshot) '  t = ' num2str(param.t(it)) ' s, validity = ' num2str(REFL_fluc.fitnegroebner.validity(it)) ]);

	      
		pause(0.001)	
		key=['NaN'];
		%wait for a pressed key
		% What to do with the pressed key
		loopmore=1;
		while loopmore
		    try dum=get(h1,'Type');
		    catch me
		        disp('figure closed')
		        return
		    end

		    switch key;
			case{'leftarrow'}
				loopmore=0;
				it=max([1,it-1]);
			case{'rightarrow'}
				loopmore=0;
				it=min([NT,it+1]);
			case{'downarrow'}
				loopmore=0;
				it=max([1,it-NTsm]);
			case{'uparrow'}
				loopmore=0;
				it=min([NT,it+NTsm]);
			case{'return'}
				loopmore=0;
				encore=0;
			case{'f'}
				loopmore=0;
				paramfrap = -paramfrap+1;
			case{'g'}
				loopmore=0;
				paramfluc = -paramfluc+1;
			case{'h'}
				loopmore=0;
				paramfrapcorr = -paramfrapcorr+1;
			case{'j'}
				loopmore=0;
				paramfluccorr = -paramfluccorr+1;
			case{'i'}
				loopmore = 0;
				keyboard


				
			otherwise
				pause(0.01)
		     end
		end
	end

param.tprof = it;

else 
		
	OUT.REFL_frap = REFL_frap;
	OUT.REFL_fluc = REFL_fluc;
	OUT.REFL_tot_fluc = REFL_tot_fluc;
	OUT.REFL_cut_fluc = REFL_cut_fluc;
	OUT.INT = INT;
	OUT.EQUI = EQUI;
	OUT.param = param;
	if param.flags.frap
		OUT.REFL_tot_frap = REFL_tot_frap;
		OUT.REFL_cut_frap = REFL_cut_frap;
	end
	return

end


OUT.REFL = REFL;

% On part d'ici 

keyboard


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% SUBFUNCTIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% INITALIZE PARAMETERS  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function param = initialise_parameters(nshot,parameters);




%% Define inputs
param.nshot=nshot;
param.tlim=[];
param.flags.dt=0;
param.flags.Zcompress=1;
param.flags.VACTH=0;
param.flags.ignitron=1;
%param.flags.map=0;
param.flags.map_occ=0;
%param.flags.RCP=0;
param.flags.plot=0;
param.INT.ind2R=[1];      % I have no idea what this does
param.INT.dN_jump=8.e18;  % check if there is a jump in density in the interferometry (which makes the data unusable) 
param.flags.quickrefl = 1;
param.flags.REFL_cut = 0;
param.flags.REFL_cut_compression = 1;
param.flags.frap = 0;
param.ind_lid = 3:10;
param.flags.fitfedorczak = 0;
param.flags.fitgroebner = 0;

%param.RCP.R=2.553;
%param.RCP.Z=[0.5:1.e-3:0.8]';

param.SYNDIpath='/Home/NF216031/MATLAB_NF/Syndi_2.0/Machines/WEST/basis/EQUINOX/INTERF.mat';
%param.interp=0;
%param.premium_shift=0;
param.gamma=0;
%param.PSINmax_fit=0.6;
%param.recalc=0;


if length(parameters)>=1
    it=1;
    while it<=length(parameters)
        switch parameters{it}
		case{'tlim'}
			param.tlim=parameters{it+1};
			it=it+2;
		case{'dt'}
	    		param.dt=parameters{it+1};
	    		param.flags.dt=1;
	    		it=it+2;
		case{'noignitron'}
          		param.flags.ignitron=0;
           		it=it+1;
	        case{'ind2R'}
	    		param.INT.ind2R = parameters{it+1};
	    		it=it+2;
      %  case{'map'}
    %        param.flags.map=1;
    %        it=it+1;
        %case{'RCP'}
            %param.flags.RCP=1;
            %it=it+1;
	case{'polarimetry map'}
		param.flags.map_occ =1;
		it = it+1;
	case{'plot'}
		param.flags.plot=1;
		it=it+1;
%	case{'Zcompress'}
%		param.flags.Zcompress = 1;
%		it=it+1;
	%case{'VACTH'}
	 %   param.flags.VACTH = 1;
       %     param.flags.Zcompress = 0;   
	%    it=it+1;
	%case{'interp'}
%	    param.interp=1;
%	    it=it+1;
	%case{'shift_corr'}
	 %   param.premium_shift=1;
	 %   it=it+1;
	case{'gamma'}
	    param.gamma=parameters{it+1};
	    it=it+2;
	%case{'recalc'}
	%    param.recalc=1;
	%    it=it+1;
	case{'fullREFL'}
		param.flags.quickrefl = 0;
		it = it+1;
	case{'nocut'}
		param.flags.REFL_cut = 0;
		it = it+1;
	case{'cutcompression'}
		param.flags.REFL_cut_compression = 1;
		it = it+1;
	case{'nofedorczak'}
		param.flags.fitfedorczak= 0;
		it = it+1;
	case{'groebner'}
		param.flags.fitgroebner = 1;
		it = it+1;
	case{'frap'}
		param.flags.frap = 1;
		it = it+1;
	case{'channel'}
		param.ind_lid = parameters{it+1};
		it = it+2;
        otherwise
            it=it+1;
        end
    end
end


out.flags=0;

%% Recap the inputs %%%%%%%%%%

switch length(param.tlim)
    case{0}
        tlim_disp='EQUINOX time boundaries';
    otherwise
        tlim_disp=['[ ' num2str(param.tlim(1),2) ' , ' num2str(param.tlim(2),2) ' ] s'];
end
if param.flags.map_occ==1
    PSIN_disp='polarimetry constraints added';
elseif param.flags.map_occ==0
    PSIN_disp='default NICE';
end

if param.flags.Zcompress
    mode_disp='optimise PSI map compression from X point';
elseif param.flags.VACTH
    mode_disp='optimisation using VACTH';
else
    mode_disp='No optimisation of asymmetries';
end

disp('-------------------------------------------------------'        )
disp('--  SELECTED PARAMETERS    ----------------------------'        )
disp(['- WEST shot number         : ' int2str(nshot)                 ])
disp(['- time interval            : ' tlim_disp                      ])
disp(['- correct time by ignitron : ' int2str(param.flags.ignitron)  ])
disp(['- magnetic coordinate PSIN : ' PSIN_disp                      ])
disp(['- INTERF indices to remove : ' int2str(param.INT.ind2R(:)')   ])
disp(['- vertical correction      : ' mode_disp                      ])
disp('-------------------------------------------------------'        ) 

if param.flags.ignitron
    igni_name='IGNITRON|1';
    T=evalc('param.tign=tsmat(nshot,igni_name);');
    if isempty(param.tign)
        disp('     no ignitron read! force to 32')
        param.tign=32;
    end
else
    disp('    no ignitron correction asked')
    param.tign=0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  READ REFLECTOMETRY   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [REFL_frap, REFL_fluc,param]  = read_reflectometry(nshot,param);

%initialisation
REFL_frap.flags = 0;
REFL_frap.Z = 0;


REFL_fluc.flags = 0;
REFL_fluc.Z = 0;

imas_name='reflectometer_profile';

loc.dt=[NaN];
loc.tlim=[NaN;NaN];
loc.ind2k=[];

try 
	T=evalc('REF0=imas_west_get(nshot,imas_name,0,0);');

	REFL_frap.t=REF0.time-param.tign;
	REFL_frap.R=REF0.channel{1}.position.r.data;
	REFL_frap.Z=REF0.channel{1}.position.z.data;
	REFL_frap.ne=REF0.channel{1}.n_e.data;
	REFL_frap.tlim=REFL_frap.t([1,end]);
	REFL_frap.dt=median(diff(REFL_frap.t));
	loc.tlim(1)=max([loc.tlim(1),REFL_frap.tlim(1)]);
	loc.tlim(2)=min([loc.tlim(2),REFL_frap.tlim(2)]);
	loc.dt=max([loc.dt;REFL_frap.dt]);
	REFL_frap.flags = 1;
	catch me
	disp(['     Failed to read imas DREF occ 0 data'])
	REFL_frap.flags=0;	
	param.flags.refl = 0;
	return
end

clear REF0

try 
	T=evalc('REF0=imas_west_get(nshot,imas_name,0,1);');
	REFL_fluc.t=REF0.time-param.tign;
	REFL_fluc.R=REF0.channel{1}.position.r.data;
	REFL_fluc.Z=REF0.channel{1}.position.z.data;
	REFL_fluc.ne=REF0.channel{1}.n_e.data;
	REFL_fluc.tlim=REFL_fluc.t([1,end]);
	REFL_fluc.dt=median(diff(REFL_fluc.t));
	loc.tlim(1)=max([loc.tlim(1),REFL_fluc.tlim(1)]);
	loc.tlim(2)=min([loc.tlim(2),REFL_fluc.tlim(2)]);
	REFL_fluc.flags = 1;
	loc.dt=max([loc.dt;REFL_fluc.dt]);
	catch me
	disp(['     Failed to read imas DREF occ 1 data'])
	REFL_fluc.flags=0;
	param.flags.refl = 0;
	return	
end

clear REF0

% Check time average request
if ~param.flags.dt
    param.dt=loc.dt;
    param.flags.dt=1;
else
    param.dt=max([param.dt;loc.dt]);
end
if isempty(param.tlim)
    param.tlim=loc.tlim;
else    param.tlim=[max([param.tlim(1);loc.tlim(1)]);...
                    min([param.tlim(2);loc.tlim(2)])];
end

param.t=[param.tlim(1):param.dt:param.tlim(2)]';
param.occ=zeros(length(param.t),1);

%extracting DREFRAP
	loc2  = lar_lite(REFL_frap.t,REFL_frap.R',param.t);
	REFL_frap.R=loc2.mean';
	REFL_frap.RRMS =loc2.rms';
	REFL_frap.N=loc2.N;
	loc2  = lar_lite(REFL_frap.t,REFL_frap.Z',param.t);
	REFL_frap.Z=loc2.mean';
	loc2  = lar_lite(REFL_frap.t,REFL_frap.ne',param.t);
	REFL_frap.ne=loc2.mean';
	REFL_frap.neRMS = loc2.rms';
	% Select times when data exist
	REFL_frap.N=sum(REFL_frap.N,2);
	ind=find(REFL_frap.N>0);
	param.occ(ind) = param.occ(ind)+1;


%extracting DREFLUC
	loc2  = lar_lite(REFL_fluc.t,REFL_fluc.R',param.t);
	REFL_fluc.R=loc2.mean';
	REFL_fluc.RRMS =loc2.rms';
	REFL_fluc.N=loc2.N;
	loc2  = lar_lite(REFL_fluc.t,REFL_fluc.Z',param.t);
	REFL_fluc.Z=loc2.mean';
	loc2  = lar_lite(REFL_fluc.t,REFL_fluc.ne',param.t);
	REFL_fluc.ne=loc2.mean';
	REFL_fluc.neRMS = loc2.rms';
	% Select times when data exist
	REFL_fluc.N=sum(REFL_fluc.N,2);
	ind=find(REFL_fluc.N>0);
	param.occ(ind) = param.occ(ind)+2;



if param.flags.quickrefl
	ind = find(param.occ ==3); %keep indices where DREFRAP and DREFLUC exist simultaneously (occ ==3)
else 
	param.occ(param.occ==2) = 0; %timestep with only DREFLUC are removed for further treatment. It is still possible to access its values, but we won't extrapolate with only DREFLUC 
				     % see reflectometry_extrapolation 
	ind = 1:length(param.occ); %takes all timestep
end

if isempty(ind)
	param.flags.refl = 0;
	disp('no reflectometry data');
	return
else 
	param.t = param.t(ind);

	REFL_frap.R     =REFL_frap.R(:,ind);
	REFL_frap.RRMS  =REFL_frap.RRMS(:,ind);
	REFL_frap.Z     =REFL_frap.Z(:,ind);
	REFL_frap.ne    =REFL_frap.ne(:,ind);
	REFL_frap.neRMS = REFL_frap.neRMS(:, ind);

	REFL_fluc.R     =REFL_fluc.R(:,ind);
	REFL_fluc.RRMS  =REFL_fluc.RRMS(:,ind);
	REFL_fluc.Z     =REFL_fluc.Z(:,ind);
	REFL_fluc.ne    =REFL_fluc.ne(:,ind);
	REFL_fluc.neRMS = REFL_fluc.neRMS(:, ind);

	param.flags.refl=1;
	param.occ = param.occ(ind);
	param.tlim=param.t([1,end]);
	param.dt=median(diff(param.t));
end



% writing DREFRAP in the correct order (growing R), DREFLUC is already in the correct order
REFL_frap.R =flipud(REFL_frap.R); 
REFL_frap.RRMS =flipud(REFL_frap.RRMS); 
REFL_frap.Z =flipud(REFL_frap.Z); 
REFL_frap.ne =flipud(REFL_frap.ne);
REFL_frap.neRMS =flipud(REFL_frap.neRMS); 

REFL_frap.occ = 1;
REFL_fluc.occ = 2; 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  READ MAGNETICS        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [EQUI,param]   = read_magnetics(nshot,param);

EQUI.flags=0;
imas_name='equilibrium';

try
    T=evalc('MF0=imas_west_get(nshot,imas_name, 0, param.flags.map_occ);');
catch me
    disp('     failed to read imas equilibrium data, byebye')
    return
end

ind=find(MF0.code.output_flag==0);

EQUI.t=MF0.timeEquiIDS(ind)'-param.tign;
EQUI.R=MF0.RInterp(:,1);
EQUI.Z=MF0.ZInterp(1,:)';
EQUI.PSI=permute(MF0.Psi_interp(ind,:,:),[1,3,2]);
EQUI.xPoint=MF0.xPoint(ind,:);
EQUI.limPoint=MF0.limPoint(ind,:);
EQUI.R0=MF0.mag_ax_R(ind)';
EQUI.Z0=MF0.mag_ax_Z(ind)';
EQUI.rho_tor=MF0.rho_tor(ind,:);
EQUI.psi_prof=MF0.psi_prof(ind,:);

clear MF0

%%% loop on xpoints & limiter points
EQUI.XDIV=1+zeros(size(EQUI.t));
for it=1:length(EQUI.t)
    if isnan(EQUI.xPoint(it,1))
        EQUI.xPoint(it,:)=EQUI.limPoint(it,:);
        EQUI.XDIV(it)=0;
    end
end
EQUI.RX=EQUI.xPoint(:,1);
EQUI.ZX=EQUI.xPoint(:,2);
EQUI=rmfield(EQUI,{'xPoint','limPoint'});

[RM,ZM]=meshgrid(EQUI.R,EQUI.Z);

% central & Xpoint PSI, PSIN
EQUI.PSI0=zeros(size(EQUI.t));
EQUI.PSIX=zeros(size(EQUI.t));
EQUI.PSIN=zeros(size(EQUI.PSI));
for it=1:length(EQUI.t)
    EQUI.PSI0(it)=interp2(EQUI.R,EQUI.Z,squeeze(EQUI.PSI(it,:,:)),EQUI.R0(it),EQUI.Z0(it));
    EQUI.PSIX(it)=interp2(EQUI.R,EQUI.Z,squeeze(EQUI.PSI(it,:,:)),EQUI.RX(it),EQUI.ZX(it));
    EQUI.PSIN(it,:,:)=abs(EQUI.PSI(it,:,:)-EQUI.PSI0(it))./(EQUI.PSIX(it)-EQUI.PSI0(it));

    % modify the PSIN map in the private flux region
    if EQUI.XDIV(it)
        PSIN=squeeze(EQUI.PSIN(it,:,:)); 
        if EQUI.ZX(it)<0    
            ind=find(ZM<EQUI.ZX(it) & PSIN<1);
        else
            ind=find(ZM>EQUI.ZX(it) & PSIN<1);
        end
        PSIN(ind)=(2-PSIN(ind)).^4;
        EQUI.PSIN(it,:,:)=PSIN;
    end 
end
EQUI.Vol=RM*2*pi.*mean(diff(EQUI.R)).*mean(diff(EQUI.Z));

EQUI.flags=1;

%computation of the flux expansion between the x point and the midplane. 
EQUI.E = NaN(length(EQUI.t), 1);
for it = 1: length(EQUI.t)
	EQUI.E(it) = flux_expansion(EQUI.RX(it), EQUI.ZX(it), 1, 0.8, squeeze(EQUI.PSIN(it, :, :)), ...
	EQUI.R, EQUI.Z, EQUI.R0(it), EQUI.Z0(it));
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read interferometry  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [INT,param]=read_interferometry(nshot,param);

INT.flags=0;

imas_name='interferometer';
try
    T=evalc('LID=imas_west_get(nshot,imas_name);');
catch me
    disp('    Failed to read imas interferometer data, byebye')
    return
end

if length(LID.time)==1
    disp('     empty interferometer data, byebye')
    return
end 

%% reformulate validity flags
if ~isfield(LID.channel{1}.n_e_line,'validity') & isfield(LID.channel{1},'n_e_line_validity')
    for it=1:length(LID.channel)
        LID.channel{it}.n_e_line.validity=LID.channel{it}.n_e_line_validity
    end
elseif ~isfield(LID.channel{1}.n_e_line,'validity') & ~isfield(LID.channel{1},'n_e_line_validity')
    disp('issue with validity flags, sorry bybye')
    return
end

%% Find valid channel
INT.t=LID.time-param.tign;
INT.N=length(LID.channel);
INT.validity=zeros(length(LID.time),INT.N);
INT.lid=zeros(length(LID.time),INT.N);

for it=1:INT.N
    if isempty(LID.channel{it}.n_e_line.data)
        INT.lid(:,it)=NaN;
    else
        INT.lid(:,it)=LID.channel{it}.n_e_line.data./2;
    end

    if LID.channel{it}.n_e_line.validity>-2 & ...
       isempty(find(param.INT.ind2R==it)) 
        INT.validity(:,it)=1;
    end

    ndex = find(abs(diff(INT.lid(:,it)))>param.INT.dN_jump);
    if length(ndex)>0
		ndex = ndex(1);
		INT.validity(ndex:end,it) = 0;
	end
    INT.lid(find(INT.lid<0))= NaN;
end
INT.flags=1;

INT = interferometry_basis(INT, param);    %      



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  RESAMPLE DATA         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [REFL_frap, REFL_fluc,EQUI,INT,param]  = resample_data(REFL_frap, REFL_fluc,EQUI,INT,param);
param.flags.resample = 1;
%Define the effective time limit %%%%%%%%%%%%%%%%%%
% Note that time limit was already defined while reading reflectometry
t1=[param.t(1);EQUI.t(1);INT.t(1)];
t2=[param.t(end);EQUI.t(end);INT.t(end)];
param.tlim=[max(t1),min(t2)];

% Reflectrometry time basis already set to tlim and dt
ind=find(param.t>=param.tlim(1) & param.t<=param.tlim(2));
param.t=param.t(ind);
param.occ=param.occ(ind);

REFL_frap.R     = REFL_frap.R(:,ind);
REFL_frap.RRMS  = REFL_frap.RRMS(:,ind);
REFL_frap.Z     = REFL_frap.Z(:,ind);
REFL_frap.ne    = REFL_frap.ne(:,ind);
REFL_frap.neRMS = REFL_frap.neRMS(:, ind);

REFL_fluc.R     = REFL_fluc.R(:,ind);
REFL_fluc.RRMS  = REFL_fluc.RRMS(:,ind);
REFL_fluc.Z     = REFL_fluc.Z(:,ind);
REFL_fluc.ne    = REFL_fluc.ne(:,ind);
REFL_fluc.neRMS = REFL_fluc.neRMS(:, ind);




if isempty(ind)
    param.flags.resample=0;
end


% EQUILIBRIUM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% interpolation on the time base of reflectometry
names={'PSI','R0','Z0','rho_tor','psi_prof','RX','ZX','PSI0','PSIX','PSIN', 'E'};

for k=1:length(names)
    eval(['EQUI.' names{k} '=interp1(EQUI.t,EQUI.' names{k} ',param.t);']);
end



EQUI.XDIV=interp1(EQUI.t,EQUI.XDIV,param.t,'nearest');
EQUI.t=param.t;
if isempty(find(~isnan(EQUI.R0)))
    EQUI.flags=0;
end

% interferometry %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%average interferometry values on time scale of reflectometry
loc=lar_lite(INT.t,INT.lid,param.t);

%verify that more than half of the values from the channel are valid.
loc2=lar_lite(INT.t,INT.validity,param.t);
loc2.mean=floor(loc2.mean);

ind=find(loc.N==0); %if one of the bin is empty, fill it with an interpolation
loc.mean(ind)=interp1(INT.t,INT.lid,param.t(ind));
loc2.mean(ind)=interp1(INT.t,INT.validity,param.t(ind),'nearest');

INT.t=param.t;
INT.lid=loc.mean;
INT.validity=loc2.mean;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% load interferometry diagnostic basis from SYNDI %%%%%%%%%%%%%%%%%%%%%%%%
function BASIS =  interferometry_basis(BASIS, param);

loc=load(param.SYNDIpath);
BASIS.R=loc.out.R;
BASIS.Z=loc.out.Z;
BASIS.basis=loc.out.mask;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reflectometry PSI interpolation  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [REFL, param]=reflectometry_PSIN(REFL,EQUI, param);

REFL.PSIN=zeros(size(REFL.R))+NaN;
PSIN=permute(EQUI.PSIN,[2,3,1]);
PSIN=reshape(PSIN,length(EQUI.Z)*length(EQUI.R),length(EQUI.t));

Rmin=min(EQUI.R);
Rmax=max(EQUI.R);
Zmin=min(EQUI.Z);
Zmax=max(EQUI.Z);
dR=mean(diff(EQUI.R));
dZ=mean(diff(EQUI.Z));
NR=length(EQUI.R);
NZ=length(EQUI.Z);

indR=(REFL.R-Rmin)./dR;
diR=indR;
indR=floor(indR);
diR=diR-indR;
indR=indR+1;
indR=(indR>0).*indR + (indR<=0).*(indR*0+1);
indR=(indR<NR).*indR+(indR>=NR).*(indR*0+NR-1);
    
% prepare vertical indices %%%%%%%%%
indZ=(REFL.Z-Zmin)./dZ;
diZ=indZ;
indZ=floor(indZ);
diZ=diZ-indZ;
indZ=indZ+1;
indZ=(indZ>0).*indZ + (indZ<=0).*(indZ*0+1);
indZ=(indZ<NZ).*indZ+(indZ>=NZ).*(indZ*0+NZ-1);
	
% Index for unfolded matrices %%%%%%%
indRZ1=(indR-1)*NZ+indZ;
indRZ2=(indR)*NZ+indZ; 

for u=1:length(param.t)
    ind=find(~isnan(indRZ1(:,u).*indRZ2(:,u)));
    a1=PSIN(indRZ1(ind,u),u).*(1-diZ(ind,u)) + PSIN(indRZ1(ind,u)+1,u).*diZ(ind,u);
    a2=PSIN(indRZ2(ind,u),u).*(1-diZ(ind,u)) + PSIN(indRZ2(ind,u)+1,u).*diZ(ind,u);
    REFL.PSIN(ind,u)=a1.*(1-diR(ind,u))+a2.*diR(ind,u);
end

%compute dpsi/dr at the separatrix (constant near the separatrix)
param.dpsidr = NaN(length(param.t), 1);
for it = 1:length(param.t)	
	if ~isempty(find(REFL.PSIN(:, it)>0.9 & REFL.PSIN(:, it)<1.1));
		idpsidr = find(REFL.PSIN(:, it)>0.9 & REFL.PSIN(:, it)<1.1);
		idpsidr1 = idpsidr(1);
		idpsidr2 = idpsidr(end);
		if REFL.R(idpsidr1, it)~=REFL.R(idpsidr2, it)
			param.dpsidr(it) = (REFL.PSIN(idpsidr1, it)-REFL.PSIN(idpsidr2, it))./(REFL.R(idpsidr1, it)-REFL.R(idpsidr2, it));
		end
	end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Reflectometry extrapolation  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [REFL_tot, REFL_cut,seconde, param] = reflectometry_extrapolation(REFL_second, REFL_main, param);
%{
Extrapolate REFL_main over the whole range of PSIN, with the help of REFL_second if needed, and stock it in REFL_tot
By default, cut the bump in the core and stock the result in REFL_cut.

INPUTS
REFL_main   : reflectometry profile that will be extrapolated. 
REFL_second : reflectometry profile that will be eventually be used to complete REFL_main. 
		If no profile is used to complete, input the same profile twice. 
		Note : It is easier to complete DREFLUC with DREFRAP than completing DREFRAP with DREFLUC (less discontinuity)
param       : parameters of the shot

OUTPUTS 
REFL_tot    : extrapolated profile. At the core, the max value is extended to the center (constant extrapolation). In the unknown part of the SOL, the profile is completed with 0.  
REFL_cut    : extrapolated truncated profile. After cutting off the eventual bump in density at the core, it extrapolates with a linear extrapolation. 


Idea of improvement : the code could be improved by adding a condition on the validation of the extrapolation
	For REFL_tot : impose condition on smallest value of psi so that the extrapolation is small enough 
	for REFL_cut : impose condition on linear extrapolation to have a slope smaller than a certain thresold. 

%}
psimax = 0.6;  %boundary for the cutoff of the bump at the core (to avoid cutting the bump in the separatrix)
Dpsi = 0.05;   % size of the "comb" of the cutoff part
dpsi = Dpsi/5; % size of the steps of the "comb" 
REFL_main.err = 0.010; %thresold of the bump
psiend = max(max([REFL_second.PSIN(end, :);REFL_main.PSIN(end, :)]))+0.001; 
psilen = int64(psiend/dpsi);
REFL_tot.PSIN = [0:0.01:psiend];
REFL_tot.ne = NaN([length(REFL_tot.PSIN), length(param.t)]);

if param.flags.REFL_cut
	REFL_cut.ne = NaN([length(REFL_tot.PSIN), length(param.t)]);
	REFL_cut.PSIN = REFL_tot.PSIN;
	REFL_cut.necut = NaN(length(param.t),1);  % array to stock the density of the cutoff
	REFL_cut.psincut = NaN(length(param.t),1);% array to stock the PSI of the cutoff
end
%initialisation of the cut 
seconde.PSI = NaN(psilen,length(param.t));
seconde.ne  = NaN(psilen,length(param.t));


if REFL_second.ne(1, 1) == REFL_main.ne(1, 1) %check if the inputs are the same. 
	occ = REFL_main.occ; %get the time step where REFL_main is alone 
else
	occ =  3;
end






for it = find(param.occ==occ|param.occ == 3)'
	if occ~= 3
		psicombined  = REFL_main.PSIN(:, it);
		necombined   = REFL_main.ne(:, it);
	else %see how to complete the profile with REFL_second
		if mean(REFL_second.ne(end, :)) < mean(REFL_main.ne(end, :))  
			nemin = REFL_main.ne(end, it);
			psimin = REFL_main.PSIN(end, it);
	 
			i3 = find(REFL_second.PSIN(:, it)>psimin& REFL_second.ne(:, it)<nemin);

			psicomp = REFL_second.PSIN(i3, it);
			necomp = REFL_second.ne(i3, it);
			psicombined = [REFL_main.PSIN(:, it); psicomp];
			necombined = [REFL_main.ne(:, it); necomp]; 
		else
			nemax = REFL_main.ne(1, it);
			psimax = REFL_main.PSIN(1, it);
			i3 = find(REFL_second.PSIN(:, it)<psimax& REFL_second.ne(:, it)>nemax);

			psicomp = REFL_second.PSIN(i3, it);
			necomp = REFL_second.ne(i3, it);
			psicombined  = [psicomp; REFL_main.PSIN(:, it)];
			necombined   = [necomp, REFL_main.ne(:, it)];
		end
	end
	 
	if param.flags.REFL_cut
		
		if psicombined(1)<psimax
			% creation of the comb
			Psi1 = [psicombined(1):dpsi:psicombined(end)-2*Dpsi]';
			Psi2 = Psi1 + Dpsi;
			Psi3 = Psi2+Dpsi;
			PSI = [Psi1, Psi2, Psi3];
			neinterp0 = interp1(psicombined(:), necombined(:), PSI);
			neseconde = (neinterp0(:, 1)-2*neinterp0 (:, 2) +neinterp0(:, 3) )./(Dpsi^2);
			nesecondenorm = neseconde/2*(Dpsi^2)./neinterp0(:, 2);
			
			seconde.PSI(1:length(Psi2), it) = Psi2;
			seconde.ne(1:length(Psi2), it) = nesecondenorm;
			
			Npsi = min(int8((-psicombined(1)-Dpsi+psimax)/dpsi), length(nesecondenorm));
			PSINclean = 0;	
			flags.err = 0;	
			ipsi = 1;
			%scanning of the inner part of the profile (psi<psimax)
			while ~flags.err & ipsi<Npsi
				if nesecondenorm(Npsi-ipsi)>REFL_main.err
					flags.err = 1;
					PSINclean= PSI(Npsi-ipsi, 2);
				else 
					ipsi = ipsi+1;
				end		
			end		
			if PSINclean
				ipsinclean = find(psicombined(:)>PSINclean);
				REFL_cut.psincut(it) = psicombined(ipsinclean(1));
				REFL_cut.necut(it)   = necombined(ipsinclean(1));
				psiinterp = [psicombined(ipsinclean); psiend];
				neinterp = [necombined(ipsinclean); 0];

				psiprime = linspace(psicombined(ipsinclean(1)), psicombined(ipsinclean(1))+0.1, 5);
				neprime = interp1(psiinterp, neinterp, psiprime);
				neprime = mean(diff(neprime)/(psiprime(2)-psiprime(1)));

				psiinterp = [0;psiinterp];	
				neinterp = [-neprime*psicombined(ipsinclean(1))+necombined(ipsinclean(1)); neinterp];
				REFL_cut.ne(:, it) = interp1(psiinterp, neinterp, REFL_tot.PSIN );	
			else
				psiinterp = [psicombined; psiend];	
				neinterp = [necombined; 0];

				psiprime = linspace(psicombined(1), psicombined(1)+0.1, 5);
				neprime = interp1(psiinterp, neinterp, psiprime);
				neprime = mean(diff(neprime)/(psiprime(2)-psiprime(1)));

				psiinterp = [0; psiinterp];	
				neinterp = [-neprime*psicombined(1)+necombined(1);  neinterp];
				REFL_cut.ne(:, it) = interp1(psiinterp, neinterp, REFL_tot.PSIN);
		
			end
		else
				psiinterp = [psicombined; psiend];	
				neinterp = [necombined; 0];

				psiprime = linspace(psicombined(1), psicombined(1)+0.1, 5);
				neprime = interp1(psiinterp, neinterp, psiprime);
				neprime = mean(diff(neprime)/(psiprime(2)-psiprime(1)));

				psiinterp = [0; psiinterp];	
				neinterp = [-neprime*psicombined(1)+necombined(1);  neinterp];
				REFL_cut.ne(:, it) = interp1(psiinterp, neinterp, REFL_tot.PSIN);
		
			
		end
	else
		REFL_cut = NaN;	
	end


	%complete the profile at the borders for the uncleaned measure
	psiinterp = [0; psicombined; psiend];	
	neinterp = [max(REFL_main.ne(:, it)) ; necombined; 0];
	REFL_tot.ne(:, it) = interp1(psiinterp, neinterp, REFL_tot.PSIN);

end %for it





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Interferometry construction from reflectometry    %%%%%%%%%%%%%%%%%%%%%

function [REFL_tot, REFL_cut] = interferometry_from_reflectometry(REFL_tot, REFL_cut ,INT,EQUI, param);
%{
computes the LID along the channels of interferometry from the reflectometry profiles, extrapolated to the whole tokamak. 

INPUTS
REFL_tot, REFL_cut : reflectometry profiles extrapolated along the whole midplane
INT                : interferometry
EQUI               ; magnetics
param		   : parameters of the shot
indtot 


OUTPUTS 
REFL_tot, REFL_cut :reflectometry profiles, with
	.lid : LID computed from REFL
	.R2  : regression coefficient

%}
indtot = param.ind_lid;


% Initialisation of the output
REFL_tot.lid = NaN(size(INT.lid));
REFL_tot.R2  = NaN(length(param.t), 1);

if param.flags.REFL_cut
	REFL_cut.lid = NaN(size(INT.lid));
	REFL_cut.R2  = NaN(length(param.t), 1);
end
for it=find(param.occ==3)'
	ind = indtot(find(INT.validity(it, indtot) ==1));
	% construct the 2D map of density
	neloc=interp1(REFL_tot.PSIN',REFL_tot.ne(:,it),squeeze(EQUI.PSIN(it,:,:)));
	if length(neloc(isnan(neloc)))~= length(neloc(:))
		neloc(isnan(neloc))=0;
	end
	neloc=neloc(:);
	
	for los=ind
		REFL_tot.lid(it,los)=sum(INT.basis(los, :)'.*neloc);
	end
	[xi2, REFL_tot.R2(it)] = kicarre_REFL_INT(REFL_tot.lid(it, :), INT.lid(it, :), ind);
	
	if param.flags.REFL_cut

		neloc=interp1(REFL_cut.PSIN',REFL_cut.ne(:,it),squeeze(EQUI.PSIN(it,:,:)));
		if length(neloc(isnan(neloc)))~= length(neloc(:))
			neloc(isnan(neloc))=0;
		end
		neloc=neloc(:);
		
		for los=ind
			REFL_cut.lid(it,los)=sum(INT.basis(los, :)'.*neloc);
		end
		[xi2, REFL_cut.R2(it)] = kicarre_REFL_INT(REFL_cut.lid(it, :), INT.lid(it, :), ind);
	end
end


function [s]=arctan(x,y)

s=atan(y./x);

s= s + pi*( 1*(x<0 & y>0) + 1*(x<0 & y<=0) + 2*(x>=0 & y<0) );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% [out]=lar_lite(t,s,dt,f_overlap)
%
% Perform a binning average of a signal with rectangular windows
% Accept multidimensional signal
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% INPUT   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% t   : abscisse coordinate of the points
% s   : value taken by the points, to average
% dt  : depends on the size
% - if length(dt)=1: then build a grid, starting at min(t), ending at max(t) 
%                    the local average is made over dt around the grid points
%                    the grid points are spaced by dt*(1-f_overlap)
%       f_overlap defines the degree of overlap between binning windows  
%       if not speficied, f_overlap=0.5;
% - if length(dt)>1 then use dt as time grid and f_overlap as dt
%        if f_overlap not specified, then use diff(dt)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% OUTPUT  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% t    : binning grid
% mean    : mean signal amplitude at each grid point
% N : number of data points in each binning window
% rms   : root mean sqaure of the data within a binning window
% med   : median value
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% local functions used: None
% 
% last update : 24/11/2014
% nicolas.fedorczak@cea.fr            


function [out]=lar_lite(t,s,tm,dt);

% Define new time 
if nargin<=4
    if length(tm)>1
        dt=mean(diff(tm));
    else
        dt=mean(diff(t));
    end
end

ss=size(s);

if isempty(s)
    out.t=tm;
    out.mean=zeros(size(tm))+NaN;
    out.rms=zeros(size(tm))+NaN;
    out.N = zeros(size(out.t));;
    return
end
out.t = tm;
out.mean=zeros([size(tm,1),ss(2:end)]);
out.rms=zeros([size(tm,1),ss(2:end)]);
out.dt=dt;
out.N = zeros(size(out.t));

if ss(2)==1

    for it=1:length(tm)
        ind=find(t>=(tm(it)-dt./2) & t<=(tm(it)+dt./2) & ~isnan(s.*t));  
        out.mean(it,:)=mean(s(ind,:),1); 
        out.rms(it,:)=std(s(ind,:),1,1);
	out.N(it) = length(ind);
    end

else

    for it=1:length(tm)
        ind=find(t>=(tm(it)-dt./2) & t<=(tm(it)+dt./2));
        count(it)=length(ind);
        out.mean(it,:)=mean(s(ind,:),1);
        out.rms(it,:)=std(s(ind,:),1,1);
	out.N(it) = length(ind);
    end
end

out.t=tm;



function [ne]=ne_mtanh(PsiN,n0,alpha,PsiP,delta);
ind=isnan(PsiN);

ne=n0.*(1+alpha.*(PsiP-PsiN)).*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
ne(ind)=0;

function [PsiN_SQ] = PSIN_squeeze(Zfix,Z,PsiN,f_comp)
PsiN(isnan(PsiN))=2.5;
% a function to compress the PsiN values
Z_new = f_comp*(Z-Zfix)+Zfix;
PsiN_SQ = interp1(Z_new,PsiN,Z);
PsiN_SQ(isnan(PsiN_SQ))=2.5;

function [SQ] = VACTH_SQ(nshot,t_ech,ZX,Z0)
[s,t] = tsbase(nshot,'GMAG_BARY');
Z0_new = interp1(t(:,2),s(:,2),t_ech);
SQ = (Z0_new-ZX)./(Z0-ZX);
SQ(SQ<0.85|SQ>1.15) = 1;

function [pos] = max_research(ne,n_max)
pos = [];
ne_max = [];
for k = 2:length(ne)-1
	if ne(k-1)<ne(k)&ne(k)>ne(k+1)
		pos(end+1) = k;
		ne_max(end+1) = ne(k);
	end
end
if length(pos)>n_max
	[new_ne,interv] = sort(ne_max);
	pos = pos(interv);
	pos = pos(1:n_max);
end



% [parameters]=fit_mtanh_REF_parabola_02(PsiN,ne,options)
%
% Perform a fit of mtanh by minimizing ki2 using the 2-dimensionnal parabola method 
%
% ne = n0 * (1-alpha*(PsiN-PsiP)) *  exp(-(PsiN-PsiP)/Delta) / ( exp(-(PsiN-PsiP)/Delta) + exp((PsiN-PsiP)/Delta) );
%
%%%%%%%%%%
% OUTPUTS: 
%
% parameters.n0 = the density at PsiP is n0/2
% parameters.alpha = the gradient of the edge part
% parameters.PsiP = the center of the pedestal
% parameters.Delta = the width of the pedestal
% parameters.validity = 3 if all parameters are valid, 2 if n0, PsiP and Delta are valid, 
% 1 if PsiP and Delta are valid and 0 if no parameters are valid
%
% parameters.reg.ki2 = non-normalized value of ki2 at the convergence point
% parameters.reg.R2 = value of R2 calculated from ki2
% parameters.reg.dPsiP = precision on PsiP
% parameters.reg.dDelta = precision on Delta
% parameters.reg.use = number of uses of the ki2 function
% parameters.reg.t_tot = total time used for the fit
% parameters.reg.not_conv = 1 if the fit didn't converge
%
%%%%%%%%%%
% INPUTS :
%
% PsiN : abscisse vector of positions
% ne  : value vector of electronic density
% options: separate them with coma
%    'plot', to get a plot of the result
%    'sq_ratio', the minimal ratio of the square (by default, 0.1)
%    'N_max', the number of uses of the ki2 fonction the program is allowed to make (by default 150)
%    'dPsiP', the result will have at least that precision on PsiP (default 1e-3)
%    'dDelta', the result will have at least that precision on Delta (default 3e-3)
%    'PsiP_edges', a list of 2 numbers [min,max] values for PsiP (default [0.6,1.4])
%    'Delta_edges', a list of 2 numbers [min,max] values for Delta (default [0.05,0.015])
%    'debug_mode', a keyboard is placed in the loop and there is a display of the variables
%
% Last modification 27/05/2021
% pierre-louis.ardizzone@ens-lyon.fr

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% minimization by parabola %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [parameters] = fit_mtanh_REF_parabola_02(PsiN,ne,varargin)
tic;
neref=ne;
neNorm=3.e19;
ne=ne./neNorm;
PsiNref=PsiN;
PSINmin=0;
PsiN = PsiN(PsiN>PSINmin);
ne = ne(PsiN>PSINmin);

% We are going to use here the parabola method

% we would like to know the usage of the ki2_mtanh
N=0;
% We define the borders of the parabola
PsiP_edges_start=[0.7,1.4];
delta_edges_start=[0.05,0.15];

% The beginning of the program
PsiP_edges = PsiP_edges_start;
delta_edges = delta_edges_start;

% the stop condition
dPsiP=1e-3;
ddelta=3e-3;
C=0.1; % the minimal ratio of the square
ploti = 0; % no plot by default
N_max=150; % break condition
flags = 0;
debug_mode = 0;
gamma = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Testing if there are additionnal inputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin>=3
    it=1;  
    while it<=length(varargin)

        switch varargin{it}

            case{'plot'}
                ploti=1;
                it=it+1;
            case{'N_max'}
	        N_max=varargin{it+1};
	        it=it+2;
            case{'sq_ratio'}
                C=varargin{it+1};
                it=it+2;
            case{'dPsiP'}
                dPsiP=varargin{it+1};
                it=it+2;
            case{'dDelta'}
                Ntot_lim=varargin{it+1};
                it=it+2;
            case{'PsiP_edges'}
                PsiP_edges=varargin{it+1};
                it=it+2;
            case{'Delta_edges'}
                delta_edges=varargin{it+1};
                it=it+2;
	    case{'debug_mode'}
	        debug_mode = 1
	        it=it+1;
	    case{'gamma'}
        	gamma = varargin{it+1};
        	it = it+2;
            case{'PSINmin'}
                PSINmin = varargin{it+1};
                it = it+2;
            otherwise
                it=it+1;
        end

    end

end


while((abs(PsiP_edges(2)-PsiP_edges(1))>dPsiP)|(abs(delta_edges(2)-delta_edges(1))>ddelta))&(N<N_max)

%no need to replicate the parabola method if the parameter already converged
if(abs(PsiP_edges(2)-PsiP_edges(1))>dPsiP)
%loop initialization
%We begin by a parabola method on PsiP because it depends less on delta than the inverse
a=PsiP_edges(1);
c=PsiP_edges(2);
b=(a+c)/2;
%We need a value for delta so we are using the middle of the square
delta=(delta_edges(2)+delta_edges(1))/2;

%values of ki2 usefull for the parabola calculation
fa=ki2_mtanh(a,delta,PsiN,ne,gamma);N=N+1;
fc=ki2_mtanh(c,delta,PsiN,ne,gamma);N=N+1;
fb=ki2_mtanh(b,delta,PsiN,ne,gamma);N=N+1;

%testing an alternative path
while(abs(c-a)>C*abs(delta_edges(2)-delta_edges(1)))&(abs(c-a)>dPsiP)&(N<N_max)

	if (fb-fc)/(b-c)>(fa-fb)/(a-b)

		%we do the calculation in several steps
		num=(b-a)^2*(fb-fc)-(b-c)^2*(fb-fa);
		den=(b-a)*(fb-fc)-(b-c)*(fb-fa);
		%x is the minimum of the parabola
		x=b-0.5*num/den;
		fx=ki2_mtanh(x,delta,PsiN,ne,gamma);N=N+1;

		if debug_mode
			abxc = [a,b,x,c]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end

		if(x<a)
			c=b;
			b=a;
			a=x;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(x>c)
			a=b;
			b=c;
			c=x;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(x>b)
			c=(x+c)/2;
			a=b;
			b=x;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh(c,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x>b)
			a=(a+b)/2;
			c=x;
			fc=fx;
			fa=ki2_mtanh(a,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx<fb)&(x<b)
			a=(a+x)/2;
			c=b;
			b=x;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh(a,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x<b)
			c=(b+c)/2;
			a=x;
			fa=fx;
			fc=ki2_mtanh(c,delta,PsiN,ne,gamma);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			c=b;
			b=a;
			a = b - (c-b);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh(a,delta,PsiN,ne,gamma);N=N+1;
		else
			a=b;
			b=c;
			c = b + (b-a);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh(c,delta,PsiN,ne,gamma);N=N+1;
		end
	end %end of the positive parabola if
	if c>2
		c=2;
	end
	if b>1.9
		b=1.9;
	end
	if a>1.8
		a=1.8;
	end
	if c<0.2
		c=0.2;
	end
	if b<0.1
		b=0.1;
	end
	if a<0
		a=0;
	end


end %end of the for loop
%the new borders are defined by the borders of the parabola
PsiP_edges=[a,c];

end %end of the outer if

%no need to replicate the parabola method if the parameter already converged
if(abs(delta_edges(2)-delta_edges(1))>ddelta)
%loop initialization
%We can now go on with the delta loop
a=delta_edges(1);
c=delta_edges(2);
b=(a+c)/2;
%We need a value for PsiP
PsiP=(PsiP_edges(2)+PsiP_edges(1))/2;

%values of ki2 usefull for the parabola
fa=ki2_mtanh(PsiP,a,PsiN,ne,gamma);N=N+1;
fc=ki2_mtanh(PsiP,c,PsiN,ne,gamma);N=N+1;
fb=ki2_mtanh(PsiP,b,PsiN,ne,gamma);N=N+1;

%The loop is the exact same as above
%while(abs(c-a)>0.1*abs(PsiP_edges(2)-PsiP_edges(1)))
while(abs(c-a)>C*abs(PsiP_edges(2)-PsiP_edges(1)))&(abs(c-a)>ddelta)&(N<N_max)
	if (fb-fc)/(b-c)>(fa-fb)/(a-b)

		%we do the calculation in several steps
		num=(b-a)^2*(fb-fc)-(b-c)^2*(fb-fa);
		den=(b-a)*(fb-fc)-(b-c)*(fb-fa);
		%x is the minimum of the parabola
		x=b-0.5*num/den;
		fx=ki2_mtanh(PsiP,x,PsiN,ne,gamma);N=N+1;
		if debug_mode
			abxc = [a,b,x,c]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end
		if(x<a)
			c=b;
			b=a;
			a=x;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(x>c)
			a=b;
			b=c;
			c=x;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(x>b)
			c=(x+c)/2;
			a=b;
			b=x;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh(PsiP,c,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x>b)
			a=(a+b)/2;
			c=x;
			fc=fx;
			fa=ki2_mtanh(PsiP,a,PsiN,ne,gamma);N=N+1;
		elseif(fx<fb)&(x<b)
			a=(a+x)/2;
			c=b;
			b=x;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh(PsiP,a,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x<b)
			c=(b+c)/2;
			a=x;
			fa=fx;
			fc=ki2_mtanh(PsiP,c,PsiN,ne,gamma);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			c=b;
			b=a;
			a = b - (c-b);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh(PsiP,a,PsiN,ne,gamma);N=N+1;
		else
			a=b;
			b=c;
			c = b + (b-a);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh(PsiP,c,PsiN,ne,gamma);N=N+1;
		end
	end %end of the positive parabola if




end %end of the for loop

%the new borders defined by the parabola borders
delta_edges=[a,c];
end %end of the outer if
end%end of the while loop


PsiP=mean(PsiP_edges);
delta=mean(delta_edges);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% We need to retrive the parameter n0 and alpha%%%%%%%%%%%%%%%%%%%%%%%%

%we calculate the value of those vectors
f1k=exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
f2k=(PsiP-PsiN).*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);

fk=n0.*f1k+n1.*f2k;
ki2_value=sum((fk-ne).^2);
N=N+1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% we prepare the result %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R2_value = 1-ki2_value/(length(PsiN)*var(ne));
temps_tot = toc;

parameters.n0 = n0*neNorm;
parameters.alpha = n1/n0;
parameters.PsiP = PsiP;
parameters.Delta = delta;
parameters.validity = (min(PsiN)<(parameters.PsiP-3*parameters.Delta))*(parameters.alpha>0&parameters.alpha<2)+(min(PsiN)<(parameters.PsiP-parameters.Delta))+(min(PsiN)<(parameters.PsiP));
parameters.validity= parameters.validity.*(parameters.n0>mean(ne*neNorm)/10 & parameters.PsiP<1.3 & parameters.PsiP>0.7);

parameters.reg.ki2 = ki2_value;
parameters.reg.R2 = R2_value;
parameters.reg.dPsiP = abs(diff(PsiP_edges));
parameters.reg.dDelta = abs(diff(delta_edges));
parameters.reg.use = N;
parameters.reg.t_tot = temps_tot;
parameters.reg.not_conv = (N>=N_max);

%%% Recalculate the entire density profile from fit, not limited to PSIN>PSINlim
f1k=exp((PsiP-PsiNref)./delta)./(exp((PsiP-PsiNref)./delta)+exp(-(PsiP-PsiNref)./delta));
f2k=(PsiP-PsiNref).*exp((PsiP-PsiNref)./delta)./(exp((PsiP-PsiNref)./delta)+exp(-(PsiP-PsiNref)./delta));
fk=n0.*f1k+n1.*f2k;
parameters.fk = fk*neNorm;

if ploti
	figure;
	plot(PsiNref,parameters.fk,PsiNref,neref,'+')
	hold on
	plot(PSINmin+[0,0],[0,parameters.n0],'r--');
	plot(parameters.PsiP+[0,0],[0,parameters.n0./2],'k--')
	plot(parameters.PsiP-parameters.Delta+[0,0],[0,parameters.n0],'k--')
	xlim([min(PsiNref)-0.1 max(PsiNref)+0.05])
	ylim([0 max(neref)*1.1])
	hold off
end

%%%%%%%%%%%%%%%%%%%% recalculation without some parts of the profile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind = PsiN<PsiP-0.5*delta;
PsiN = PsiN(ind);
ne = ne(ind);
%we calculate the value of those vectors
f1k=exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
f2k=(PsiP-PsiN).*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);

parameters.bis.n0 = n0*neNorm;
parameters.bis.alpha = n1/n0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ki2 calculation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%we calculate separately the value of ki2 for mtanh with specified parameters
function [ki2_value] = ki2_mtanh(PsiP,delta,PsiN,ne,gamma)
%we calculate the value of those vectors
ne_ = mean(ne);
wk = (ne/ne_).^gamma;

f1k=wk.*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
f2k=wk.*(PsiP-PsiN).*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
ne=wk.*ne;

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);



fk=n0.*f1k+n1.*f2k;
ki2_value=sum((fk-ne).^2);


% [parameters]=fit_mtanh_INT_parabola_01(Lne,basis,PsiN,Delta)
%
% Perform a fit of mtanh by minimizing ki2 using the 1-dimensionnal parabola method on PsiP
%
% ne = n0 * (1-alpha*(PsiN-PsiP)) *  exp(-(PsiN-PsiP)/Delta) / ( exp(-(PsiN-PsiP)/Delta) + exp((PsiN-PsiP)/Delta) );
%
%%%%%%%%%%
% OUTPUTS: 
%
% parameters.n0 = the density at PsiP is n0/2
% parameters.alpha = the gradient of the edge part
% parameters.PsiP = center of the pedestal
% parameters.Delta_used = the width of the pedestal as given by the reflectometry
% parameters.validity = 1 if the parameters are valid and 0 if they don't make sense
%
% parameters.reg.ki2 = the non-normalized value of ki2
% parameters.reg.R2 = value of R2 calculated from ki2
% parameters.reg.dPsiP = the precision we need on PsiP (by default 2e-3)
% parameters.reg.use = the number of calls to the ki2 fonction
% parameters.reg.t_tot = total time for the fit
% parameters.reg.not_conv = 1 if the number of uses reached N_max = 100, 0 otherwise

%
%%%%%%%%%%
% INPUTS :
%
% Lne : the line integrated density along the lasers at a given time
% basis : the map of the lasers paths in R,Z space
% PsiN : the 2-d map of PsiN at a given time
% Delta : the value of Delta determined by the reflectometry
% options: separate them with coma
%    'plot', to get a plot of the result
%    'n0', forces a value of n0 as given by the reflectometry (PsiP can be better determined if given)
%    'alpha', forces a value of alpha as given by the reflectometry /!\ n0 must also be given
%    'N_max', the number of uses of the ki2 fonction the program is allowed to make (by default 100)
%    'dPsiP', the precision needed on PsiP (default 2e-3)
%    'PsiP_edges', a list of 2 numbers [min,max] values for PsiP (default [0.9,1.2])
%    'debug_mode', a keyboard is placed in the loop and there is a display of the variables
%
% Last modification 02/06/2021
% pierre-louis.ardizzone@ens-lyon.fr


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% minimization by parabole method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [parameters] = fit_mtanh_INT_parabola_01(Lne,basis,PsiN,Delta,varargin)
tic;
Lne=Lne*1e-19;
N=0; % number of calls to the ki2 function
PsiP_edges_start = [0.9,1.2];
PsiP_edges = PsiP_edges_start; % initialization of the parabola

dPsiP = 2e-3; % Precision we need on PsiP
N_max = 100;
ploti=0;
given_param = 0;
n0 = 0;
alpha = 0;
debug_mode = 0;

if nargin>=5
    it=1;
    while it<=length(varargin)
        switch varargin{it}
            case{'plot'}
                ploti=1;
                it=it+1;
            case{'n0'}
                n0=varargin{it+1}; % if we want to give n0
                it=it+2;
		given_param = given_param+1;
	    case{'N_max'}
		N_max=varargin{it+1};
		it=it+2;
            case{'alpha'}
                alpha=varargin{it+1}; % if we want to give alpha
                it=it+2;
		given_param = given_param+1;
            case{'dPsiP'}
                dPsiP=varargin{it+1};
                it=it+2;
            case{'PsiP_edges'}
                PsiP_edges=varargin{it+1};
                it=it+2;
	    case{'debug_mode'}
		debug_mode = 1
		it=it+1;
            otherwise
                it=it+1;
        end
    end
end

% we can now begin the while loop
a=PsiP_edges(1);
c=PsiP_edges(2);
b=(a+c)/2;

fa = ki2_mtanh_INT(Lne,basis,PsiN,a,Delta,n0,alpha,given_param);N=N+1;
fb = ki2_mtanh_INT(Lne,basis,PsiN,b,Delta,n0,alpha,given_param);N=N+1;
fc = ki2_mtanh_INT(Lne,basis,PsiN,c,Delta,n0,alpha,given_param);N=N+1;

while(abs(a-c)>dPsiP)&(N<N_max)
	if (fb-fc)/(b-c)>(fa-fb)/(a-b)
		%we do the calculation in several steps
		num=(b-a)^2*(fb-fc)-(b-c)^2*(fb-fa);
		den=(b-a)*(fb-fc)-(b-c)*(fb-fa);
		%x is the minimum of the parabola
		x=b-0.5*num/den;
		fx = ki2_mtanh_INT(Lne,basis,PsiN,x,Delta,n0,alpha,given_param);N=N+1;
		if debug_mode
			abxc = [a,b,x,c]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end
		if(x<a)
			c=b;
			b=a;
			a=x;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(x>c)
			a=b;
			b=c;
			c=x;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(x>b)
			c=(x+c)/2;
			a=b;
			b=x;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiN,c,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(x>b)
			a=(a+b)/2;
			c=x;
			fc=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiN,a,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx<fb)&(x<b)
			a=(a+x)/2;
			c=b;
			b=x;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiN,a,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(x<b)
			c=(b+c)/2;
			a=x;
			fa=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiN,c,Delta,n0,alpha,given_param);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			c=b;
			b=a;
			a = b - (c-b);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh_INT(Lne,basis,PsiN,a,Delta,n0,alpha,given_param);N=N+1;
		else
			a=b;
			b=c;
			c = b + (b-a);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh_INT(Lne,basis,PsiN,c,Delta,n0,alpha,given_param);N=N+1;
		end
	end % end of the positive parabola if
end %end of the for loop

PsiP = x;
dPsiP = abs(a-c);

%%%%%%%%%%%%%%%%%%%%%%%% We still need to get the parameters n0 and alpha %%%%%%%%%%%%%%%%%%%%%%%%%

%the supposed value of f1k and f2k where ne = n0*f1k + n1*f2k
f1k = exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f2k = (PsiP-PsiN).*exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f1k(isnan(f1k))=0;
f2k(isnan(f2k))=0;

F1k = zeros(size(Lne));
F2k = zeros(size(Lne));
%the integrated values of F1k and F2k
for it = 1:length(Lne)
	F1k(it) = sum(basis(it,:).*f1k(:)');
	F2k(it) = sum(basis(it,:).*f2k(:)');
end

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(F1k.*F1k);
sumf2f2=sum(F2k.*F2k);
sumf1f2=sum(F1k.*F2k);
sumf1y=sum(F1k.*Lne);
sumf2y=sum(F2k.*Lne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);

Fk=n0.*F1k+n1.*F2k;
ki2_value=sum((Fk-Lne).^2);
N=N+1;

%we can now calculate all the output parameters
R2_value = 1-(ki2_value/(length(Lne)*var(Lne)));
temps_tot = toc;

parameters.n0 = n0*1e19;
parameters.alpha = n1/n0;
parameters.PsiP = PsiP;
parameters.Delta_used = Delta;
parameters.validity = (parameters.n0>0 & parameters.PsiP<1.25 & parameters.PsiP>0.75);

parameters.fk = Fk*1e19;

parameters.reg.ki2 = ki2_value;
parameters.reg.R2 = R2_value;
parameters.reg.dPsiP = dPsiP;
parameters.reg.use = N;
parameters.reg.t_tot = temps_tot;
parameters.reg.not_conv = (N>=N_max);

% we plot like asked
if ploti
	hold on
	plot(Fk,'ro-')
	plot(Lne,'o-')
	hold off
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%% ki2 calculation for the case of the lasers %%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ki2_value] = ki2_mtanh_INT(Lne,basis,PsiN,PsiP,Delta,varargin)
% we can give the parameters n0 and alpha in that order and after the number of parameters given
given_param = 0;
if nargin>=6
	n0_giv = varargin{1}*1e-19;
	alpha_giv = varargin{2};
	given_param = varargin{3};
end

%the supposed value of f1k and f2k where ne = n0*f1k + n1*f2k
f1k = exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f2k = (PsiP-PsiN).*exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f1k(isnan(f1k))=0;
f2k(isnan(f2k))=0;

F1k = zeros(size(Lne));
F2k = zeros(size(Lne));
% the integrated values of F1k and F2k
for it = 1:length(Lne)
	F1k(it) = sum(basis(it,:).*f1k(:)');
	F2k(it) = sum(basis(it,:).*f2k(:)');
end

% we need to separate the different cases for the given parameters
switch given_param
	case{0}
	% sums used to calculate n0 and n1 the linear parameters
	sumf1f1=sum(F1k.*F1k);
	sumf2f2=sum(F2k.*F2k);
	sumf1f2=sum(F1k.*F2k);
	sumf1y=sum(F1k.*Lne);
	sumf2y=sum(F2k.*Lne);

	%calculation on n0 and n1 which is a matrix inversion
	n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
	n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
	case{1}
	% sums used to calculate n1 the linear parameter
	sumf2f2=sum(F2k.*F2k);
	sumf1f2=sum(F1k.*F2k);
	sumf2y=sum(F2k.*Lne);

	%calculation for n1
	n0=n0_giv;
	n1=(sumf2y-n0*sumf1f2)/sumf2f2;
	case{2}
	% no need for a calculation, the result is given
	n0=n0_giv;
	n1=n0_giv*alpha_giv;

end

Fk=n0.*F1k+n1.*F2k;
ki2_value=sum((Fk-Lne).^2);



% [parameters]=fit_mtanh_INT_s_parabola_01(Lne,basis,PsiN,Zfix,Z,Delta)
%
% Perform a fit of mtanh by minimizing ki2 using the 1-dimensionnal parabola method on PsiP
%
% ne = n0 * (1-alpha*(PsiN-PsiP)) *  exp(-(PsiN-PsiP)/Delta) / ( exp(-(PsiN-PsiP)/Delta) + exp((PsiN-PsiP)/Delta) );
%
%%%%%%%%%%
% OUTPUTS: 
%
% parameters.n0 = the density at PsiP is n0/2
% parameters.alpha = the gradient of the edge part
% parameters.PsiP = center of the pedestal
% parameters.Delta_used = the width of the pedestal as given by the reflectometry
% parameters.validity = 1 if the parameters are valid and 0 if they don't make sense
%
% parameters.reg.ki2 = the non-normalized value of ki2
% parameters.reg.R2 = value of R2 calculated from ki2
% parameters.reg.dPsiP = the precision we need on PsiP (by default 2e-3)
% parameters.reg.use = the number of calls to the ki2 fonction
% parameters.reg.t_tot = total time for the fit
% parameters.reg.not_conv = 1 if the number of uses reached N_max = 100, 0 otherwise

%
%%%%%%%%%%
% INPUTS :
%
% Lne : the line integrated density along the lasers at a given time
% basis : the map of the lasers paths in R,Z space
% PsiN : the 2-d map of PsiN at a given time
% Delta : the value of Delta determined by the reflectometry
% Zfix : the coordinate Z at which is situated the X-point or if there is none, -0.8 the bottom of the plasma
% Z : EQUI.Z, list of Z positions
% options: separate them with coma
%    'plot', to get a plot of the result
%    'n0', forces a value of n0 as given by the reflectometry (PsiP can be better determined if given)
%    'alpha', forces a value of alpha as given by the reflectometry /!\ n0 must also be given
%    'N_max', the number of uses of the ki2 fonction the program is allowed to make (by default 150)
%    'dPsiP', the precision needed on PsiP (default 2e-3)
%    'dSQ', precision on the squeeze parameter (default 1e-3)
%    'SQ_edges', a list of 2 numbers [min,max] values for SQ (default [0.9,1.1])
%    'PsiP_edges', a list of 2 numbers [min,max] values for PsiP (default [0.9,1.2])
%    'debug_mode', a keyboard is placed in the loop and there is a display of the variables
%
% Last modification 10/06/2021
% pierre-louis.ardizzone@ens-lyon.fr


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% minimization by parabole method %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [parameters] = fit_mtanh_INT_s_parabola_01(Lne,basis,PsiN,Delta,Zfix,Z,varargin)
tic;

Lne=Lne*1e-19;
PsiN = squeeze(PsiN);
N=0; % number of calls to the ki2 function
PsiP_edges_start = [0.9,1.2];
PsiP_edges = PsiP_edges_start; % initialization of the parabola

SQ_edges_start = [0.9,1.1];
SQ_edges = SQ_edges_start;

dPsiP = 2e-3; % Precision we need on PsiP
dSQ = 1e-2;
N_max = 150;
N_loop = 2;
ploti=0;
given_param = 0;
n0 = 0;
alpha = 0;
debug_mode = 0;

if nargin>=7
    it=1;
    while it<=length(varargin)
        switch varargin{it}
            case{'plot'}
                ploti=1;
                it=it+1;
            case{'n0'}
                n0=varargin{it+1}; % if we want to give n0
                it=it+2;
		given_param = given_param+1;
	    case{'N_max'}
		N_max=varargin{it+1};
		it=it+2;
            case{'alpha'}
                alpha=varargin{it+1}; % if we want to give alpha
                it=it+2;
		given_param = given_param+1;
            case{'dPsiP'}
                dPsiP=varargin{it+1};
                it=it+2;
	    case{'dSQ'}
                dSQ=varargin{it+1};
                it=it+2;
            case{'PsiP_edges'}
                PsiP_edges=varargin{it+1};
                it=it+2;
	    case{'SQ_edges'}
                SQ_edges=varargin{it+1};
                it=it+2;
	    case{'N_loop'}
		N_loop=varargin{it+1};
		it=it+2;
	    case{'debug_mode'}
		debug_mode = 1
		it=it+1;
            otherwise
                it=it+1;
        end
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% we can now begin the while loop %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ax=PsiP_edges(1);
cx=PsiP_edges(2);
bx=(ax+cx)/2;

ay=SQ_edges(1);
cy=SQ_edges(2);
by=(ay+cy)/2;

PsiNa = PSIN_squeeze(Zfix,Z,PsiN,ay);
PsiNb = PSIN_squeeze(Zfix,Z,PsiN,by);
PsiNc = PSIN_squeeze(Zfix,Z,PsiN,cy);

while(abs(ax-cx)>dPsiP|abs(ay-cy)>dSQ)&(N<N_max)
% we can now begin the while loop
y = by;
PsiNy = PsiNb;

fa = ki2_mtanh_INT(Lne,basis,PsiNy,ax,Delta,n0,alpha,given_param);N=N+1;
fb = ki2_mtanh_INT(Lne,basis,PsiNy,bx,Delta,n0,alpha,given_param);N=N+1;
fc = ki2_mtanh_INT(Lne,basis,PsiNy,cx,Delta,n0,alpha,given_param);N=N+1;

for i = 1:N_loop

	if (fb-fc)/(bx-cx)>(fa-fb)/(ax-bx)
		%we do the calculation in several steps
		num=(bx-ax)^2*(fb-fc)-(bx-cx)^2*(fb-fa);
		den=(bx-ax)*(fb-fc)-(bx-cx)*(fb-fa);s
		%x is the minimum of the parabola
		xx=bx-0.5*num/den;
		fx = ki2_mtanh_INT(Lne,basis,PsiN,xx,Delta,n0,alpha,given_param);N=N+1;
		if debug_mode
			abxc = [ax,bx,xx,cx]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end
		if(xx<ax)
			cx=bx;
			bx=ax;
			ax=xx;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(xx>cx)
			ax=bx;
			bx=cx;
			cx=xx;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(xx>bx)
			cx=(xx+cx)/2;
			ax=bx;
			bx=xx;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiNy,cx,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(xx>bx)
			ax=(ax+bx)/2;
			cx=xx;
			fc=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiNy,ax,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx<fb)&(xx<bx)
			ax=(ax+xx)/2;
			cx=bx;
			bx=xx;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiNy,ax,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(xx<bx)
			cx=(bx+cx)/2;
			ax=xx;
			fa=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiNy,cx,Delta,n0,alpha,given_param);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			cx=bx;
			bx=ax;
			ax = bx - (cx-bx);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh_INT(Lne,basis,PsiNy,ax,Delta,n0,alpha,given_param);N=N+1;
		else
			ax=bx;
			bx=cx;
			cx = bx + (bx-ax);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh_INT(Lne,basis,PsiNy,cx,Delta,n0,alpha,given_param);N=N+1;
		end
	end %end of the positive parabola if

end %end of the for loop

x = (ax+cx)/2;

fa = ki2_mtanh_INT(Lne,basis,PsiNa,x,Delta,n0,alpha,given_param);N=N+1;
fb = ki2_mtanh_INT(Lne,basis,PsiNb,x,Delta,n0,alpha,given_param);N=N+1;
fc = ki2_mtanh_INT(Lne,basis,PsiNc,x,Delta,n0,alpha,given_param);N=N+1;

if abs(ay-cy)>dSQ
for i = 1:N_loop

	if (fb-fc)/(by-cy)>(fa-fb)/(ay-by)
		%we do the calculation in several steps
		num=(by-ay)^2*(fb-fc)-(by-cy)^2*(fb-fa);
		den=(by-ay)*(fb-fc)-(by-cy)*(fb-fa);
		%x is the minimum of the parabola
		xy=by-0.5*num/den;
		PsiNx = PSIN_squeeze(Zfix,Z,PsiN,xy);
		fx = ki2_mtanh_INT(Lne,basis,PsiNx,x,Delta,n0,alpha,given_param);N=N+1;
		if debug_mode
			abxc = [ay,by,xy,cy]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end
		if(xy<ay)
			cy=by;
			by=ay;
			ay=xy;
			PsiNc=PsiNb;
			PsiNb=PsiNa;
			PsiNa=PsiNx;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(xy>cy)
			ay=by;
			by=cy;
			cy=xy;
			PsiNa=PsiNb;
			PsiNb=PsiNc;
			PsiNc=PsiNx;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(xy>by)
			cy=(xy+cy)/2;
			ay=by;
			by=xy;
			PsiNc = PSIN_squeeze(Zfix,Z,PsiN,cy);
			PsiNa=PsiNb;
			PsiNb=PsiNx;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiNc,x,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(xy>by)
			ay=(ay+by)/2;
			cy=xy;
			PsiNa = PSIN_squeeze(Zfix,Z,PsiN,ay);
			PsiNc = PsiNx;
			fc=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiNa,x,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx<fb)&(xy<by)
			ay=(ay+xy)/2;
			cy=by;
			by=xy;
			PsiNa = PSIN_squeeze(Zfix,Z,PsiN,ay);
			PsiNc=PsiNb;
			PsiNb=PsiNx;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh_INT(Lne,basis,PsiNa,x,Delta,n0,alpha,given_param);N=N+1;
		elseif(fx>fb)&(xy<by)
			cy=(by+cy)/2;
			ay=xy;
			PsiNc = PSIN_squeeze(Zfix,Z,PsiN,cy);
			PsiNa=PsiNx;
			fa=fx;
			fc=ki2_mtanh_INT(Lne,basis,PsiNc,x,Delta,n0,alpha,given_param);N=N+1;
		end %end of the inner if
	else 
		if fa<fc
			cy=by;
			by=ay;
			ay = by - (cy-by);
			PsiNc=PsiNb;
			PsiNb=PsiNa;
			PsiNa = PSIN_squeeze(Zfix,Z,PsiN,ay);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh_INT(Lne,basis,PsiNa,x,Delta,n0,alpha,given_param);N=N+1;
		else
			ay=by;
			by=cy;
			cy = by + (by-ay);
			PsiNa=PsiNb;
			PsiNb=PsiNc;
			PsiNc = PSIN_squeeze(Zfix,Z,PsiN,cy);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh_INT(Lne,basis,PsiNc,x,Delta,n0,alpha,given_param);N=N+1;
		end
	end %end of the positive parabola if


end %end of the for loop
end

end %end of the while

PsiP = xx;
SQ = xy;
PsiN = PsiNx;

dPsiP = abs(ax-cx);
dSQ = abs(ay-cy);

%%%%%%%%%%%%%%%%%%%%%%%% We still need to get the parameters n0 and alpha %%%%%%%%%%%%%%%%%%%%%%%%%

%the supposed value of f1k and f2k where ne = n0*f1k + n1*f2k
f1k = exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f2k = (PsiP-PsiN).*exp((PsiP-PsiN)./Delta)./(exp((PsiP-PsiN)./Delta)+exp(-(PsiP-PsiN)./Delta));
f1k(isnan(f1k))=0;
f2k(isnan(f2k))=0;

F1k = zeros(size(Lne));
F2k = zeros(size(Lne));
%the integrated values of F1k and F2k
for it = 1:length(Lne)
	F1k(it) = sum(basis(it,:).*f1k(:)');
	F2k(it) = sum(basis(it,:).*f2k(:)');
end

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(F1k.*F1k);
sumf2f2=sum(F2k.*F2k);
sumf1f2=sum(F1k.*F2k);
sumf1y=sum(F1k.*Lne);
sumf2y=sum(F2k.*Lne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);

Fk=n0.*F1k+n1.*F2k;
ki2_value=sum((Fk-Lne).^2);
N=N+1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Output preparation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%we can now calculate all the output parameters
R2_value = 1-(ki2_value/(length(Lne)*var(Lne)));
temps_tot = toc;

parameters.n0 = n0*1e19;
parameters.alpha = n1/n0;
parameters.PsiP = PsiP;
parameters.Delta_used = Delta;
parameters.SQ = SQ;
parameters.validity = (parameters.n0>0 & parameters.PsiP<1.4 & parameters.PsiP>0.5);

parameters.fk = Fk*1e19;

parameters.reg.ki2 = ki2_value;
parameters.reg.R2 = R2_value;
parameters.reg.dPsiP = dPsiP;
parameters.reg.dSQ = dSQ;
parameters.reg.use = N;
parameters.reg.t_tot = temps_tot;
parameters.reg.not_conv = (N>=N_max);

% we plot like asked
if ploti
	hold on
	plot(Fk,'ro-')
	plot(Lne,'o-')
	hold off
end







function [REFL_tot, EQUI]        	  = lid_PSIN_corr(REFL_tot, INT,EQUI, param);
indtot = param.ind_lid;
EQUI.PSINcorr = NaN(size(EQUI.PSIN));
REFL_tot.lidcorr = NaN(length(param.t), size(INT.lid,2));;
REFL_tot.fcompress = NaN(length(EQUI.t), 1);
REFL_tot.R2corr = NaN(length(param.t), 1);

lid = INT.lid;
for it = find(param.occ == 3)'

	ind = indtot(find(INT.validity(it, indtot)==1));
	
	N_max = 20;
	N = 0;
	fcorra = 0.8;
	fcorrb = 1;
	fcorrc = 1.2;
	dfcorr = 0.01;
	debug_mode = 0;
	PSINcorra = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorra);N=N+1;
	PSINcorrb = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorrb);N=N+1;
	PSINcorrc = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorrc);N=N+1;

	lidr_corra = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorra, ind);
	lidr_corrb = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrb, ind);
	lidr_corrc = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrc, ind);
	
	[ya, R2_value] = kicarre_REFL_INT(lidr_corra, lid(it, :),ind);
	[yb, R2_value] = kicarre_REFL_INT(lidr_corrb, lid(it, :),ind);
	[yc, R2_value] = kicarre_REFL_INT(lidr_corrc, lid(it, :),ind);
	while abs(fcorrc-fcorra)>dfcorr&(N<N_max)
		
		if (yb-yc)/(fcorrb-fcorrc)>(ya-yb)/(fcorra-fcorrb)
			%we do the calculation in several steps
			num=(fcorrb-fcorra)^2*(yb-yc)-(fcorrb-fcorrc)^2*(yb-ya);
			den=(fcorrb-fcorra)*(yb-yc)-(fcorrb-fcorrc)*(yb-ya);
			%x is the minimum of the parabola
			xy=fcorrb-0.5*num/den;
			PSINcorrx = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), xy);
			lidr_corrx =  ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrx, ind);
			[yx, R2_value] = kicarre_REFL_INT(lidr_corrx, lid(it, :),ind);N=N+1;
			if debug_mode
				abxc = [fcorra,fcorrb,xy,fcorrc]
				fafbfxfc = [ya,yb,yx,yc]
				keyboard
			end
			if(xy<fcorra)
				fcorrc=fcorrb;
				fcorrb=fcorra;
				fcorra=xy;
				PSINcorrc=PSINcorrb;
				PSINcorrb=PSINcorra;
				PSINcorra=PSINcorrx;

				lidr_corrc=lidr_corrb;
				lidr_corrb=lidr_corra;
				lidr_corra=lidr_corrx;

				yc=yb;
				yb=ya;
				ya=yx;
			elseif(xy>fcorrc)
				fcorra=fcorrb;
				fcorrb=fcorrc;
				fcorrc=xy;
				PSINcorra=PSINcorrb;
				PSINcorrb=PSINcorrc;
				PSINcorrc=PSINcorrx;

				lidr_corra=lidr_corrb;
				lidr_corrb=lidr_corrc;
				lidr_corrc=lidr_corrx;
				ya=yb;
				yb=yc;
				yc=yx;
			elseif(yx<yb)&(xy>fcorrb)
				fcorrc=(xy+fcorrc)/2;
				fcorra=fcorrb;
				fcorrb=xy;
				PSINcorrc = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorrc);
				PSINcorra=PSINcorrb;
				PSINcorrb=PSINcorrx;
				lidr_corrc = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrc, ind);
				lidr_corra=lidr_corrb;
				lidr_corrb=lidr_corrx;
				ya=yb;
				yb=yx;
				[yc, R2_value]= kicarre_REFL_INT(lidr_corrc, lid(it, :),ind);N=N+1;

			elseif(yx>yb)&(xy>fcorrb)
				fcorra=(fcorra+fcorrb)/2;
				fcorrc=xy;
				PSINcorra = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorra);
				PSINcorrc = PSINcorrx;
			
				lidr_corra = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorra, ind);
				lidr_corrc = lidr_corrx;
				yc=yx;
				[ya, R2_value]=kicarre_REFL_INT(lidr_corra, lid(it, :),ind);N=N+1;
			elseif(yx<yb)&(xy<fcorrb)
				fcorra=(fcorra+xy)/2;
				fcorrc=fcorrb;
				fcorrb=xy;
				PSINcorra = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorra);
				PSINcorrc=PSINcorrb;
				PSINcorrb=PSINcorrx;
				
				lidr_corra = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorra, ind);
				lidr_corrc=lidr_corrb;
				lidr_corrb=lidr_corrx;
				yc=yb;
				yb=yx;
				[ya, R2_value]=kicarre_REFL_INT(lidr_corra, lid(it, :),ind);N=N+1;
			elseif(yx>yb)&(xy<fcorrb)
				fcorrc=(fcorrb+fcorrc)/2;
				fcorra=xy;
				PSINcorrc = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorrc);
				PSINcorra=PSINcorrx;
				
				lidr_corrc = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrc, ind);
				lidr_corra=lidr_corrx;
				ya=yx;
				[yc, R2_value]=kicarre_REFL_INT(lidr_corrc, lid(it, :),ind);N=N+1;
			end %end of the inner if
		else 
			if ya<yc
				fcorrc=fcorrb;
				fcorrb=fcorra;
				fcorra = fcorrb - (fcorrc-fcorrb);
				PSINcorrc=PSINcorrb;
				PSINcorrb=PSINcorra;
				PSINcorra = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorra);

				lidr_corrc=lidr_corrb;
				lidr_corrb=lidr_corra;
				lidr_corra = ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorra, ind);

				yc=yb;
				yb=ya;
				[ya, R2_value] = kicarre_REFL_INT(lidr_corra, lid(it, :),ind);N=N+1;
			else
				fcorra=fcorrb;
				fcorrb=fcorrc;
				fcorrc = fcorrb + (fcorrb-fcorra);
				PSINcorra=PSINcorrb;
				PSINcorrb=PSINcorrc;
				PSINcorrc = PSIN_squeeze(EQUI.ZX(it), EQUI.Z, squeeze(EQUI.PSIN(it,:,:)), fcorra);

				lidr_corra=lidr_corrb;
				lidr_corrb=lidr_corrc;
				lidr_corrc =ne_to_lid(REFL_tot.PSIN, REFL_tot.ne(:, it), INT.basis, PSINcorrc, ind);
				ya=yb;
				yb=yc;
				[yc, R2_value] = kicarre_REFL_INT(lidr_corrc, lid(it, :),ind);N=N+1;
			end
		end %end of the positive parabola if

		
	end %end of the while
	
	EQUI.psicorr(it,:,:) = PSINcorrb(:, :);
	
	REFL_tot.lidcorr(it,ind) = lidr_corrb(ind);
	REFL_tot.fcompress(it) = fcorrb;
	REFL_tot.R2corr(it) = R2_value;
end % end of the for



function lid_corr = ne_to_lid(PSI_ref, ne_ref, basis, PSINcorr, ind) 
% construct the 2D map of density
neloc=interp1(PSI_ref',ne_ref,PSINcorr(:,:));
neloc(isnan(neloc))=0;
neloc=neloc(:);
for los=ind
	lid_corr(los)=sum(basis(los,:)'.*neloc);
end


function [kicarre_value, R2_value] = kicarre_REFL_INT(lidr, lidint, varargin)
if nargin <=2
	ecart = lidr-lidint;
	stdxi2 = std(ecart.^2);

	xi2 = ecart.^2;

	kicarre_value = sum(xi2);
	if length(lidint)>1
	R2_value = 1-(kicarre_value/(length(lidint)*var(lidint)));
	else
	R2_value = 1-(kicarre_value/length(lidint));
	end	

	
else
	ind = varargin{1};
	xi2 = (lidr(ind)-lidint(ind)).^2;
	xi2(isnan(xi2)) = 0;
	%if std(lidr(ind)-lidint(ind))~=0
	%	xi2 = xi2./(std(lidr(ind)-lidint(ind))).^2;
	%end
	kicarre_value = sum(xi2);
	if length(lidint(ind))>1
	R2_value = 1-(kicarre_value/(length(lidint(ind))*var(lidint(ind))));
	else
	R2_value = 1-(kicarre_value/(length(lidint(ind))*var(lidint(ind))));
	end	
end 

% [parameters]=fit_mtanh_REF_parabola_Groebner(PsiN,ne,options)
%
% Perform a fit of mtanh by minimizing ki2 using the 2-dimensionnal parabola method 
%
%  ne = A*tanh((PSIP-PSIN)/(delta/2) + B + C*(PsiNref<psiknee).*(psiknee-PsiNref)
%	with psiknee = PSIP-delta/2
%%%%%%%%%%
% OUTPUTS: 
%
% parameters.A = height of the pedestal
% parameters.B = offset of the pedestal
% parameters.PsiP = the center of the pedestal
% parameters.Delta = the width of the pedestal
% parameters.validity = 3 if all parameters are valid, 2 if A, PsiP and Delta are valid, 
% 1 if PsiP and Delta are valid and 0 if no parameters are valid
%
% parameters.reg.ki2 = non-normalized value of ki2 at the convergence point
% parameters.reg.R2 = value of R2 calculated from ki2
% parameters.reg.dPsiP = precision on PsiP
% parameters.reg.dDelta = precision on Delta
% parameters.reg.use = number of uses of the ki2 function
% parameters.reg.t_tot = total time used for the fit
% parameters.reg.not_conv = 1 if the fit didn't converge
%
%%%%%%%%%%
% INPUTS :
%
% PsiN : abscisse vector of positions
% ne  : value vector of electronic density
% options: separate them with coma
%    'plot', to get a plot of the result
%    'sq_ratio', the minimal ratio of the square (by default, 0.1)
%    'N_max', the number of uses of the ki2 fonction the program is allowed to make (by default 150)
%    'dPsiP', the result will have at least that precision on PsiP (default 1e-3)
%    'dDelta', the result will have at least that precision on Delta (default 3e-3)
%    'PsiP_edges', a list of 2 numbers [min,max] values for PsiP (default [0.6,1.4])
%    'Delta_edges', a list of 2 numbers [min,max] values for Delta (default [0.05,0.015])
%    'debug_mode', a keyboard is placed in the loop and there is a display of the variables
%
% Last modification 01/06/2022
% loufev@hotmail.fr

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% minimization by parabola %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [parameters] = fit_mtanh_REF_parabola_Groebner(PsiN,ne,varargin)

tic;
neref=ne;
neNorm=3.e19;
ne=ne./neNorm;
PsiNref=PsiN;
PSINmin=0;
PsiN = PsiN(PsiN>PSINmin);
ne = ne(PsiN>PSINmin);

% We are going to use here the parabola method

% we would like to know the usage of the ki2_mtanh_Groebner
N=0;
% We define the borders of the parabola
PsiP_edges_start=[0.7,1.4];
delta_edges_start=[0.01,0.4];

% The beginning of the program
PsiP_edges = PsiP_edges_start;
delta_edges = delta_edges_start;

% the stop condition
dPsiP=1e-3;
ddelta=3e-3;
C=0.1; % the minimal ratio of the square
ploti = 0; % no plot by default
N_max=150; % break condition
flags = 0;
debug_mode = 0;
gamma = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Testing if there are additionnal inputs %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if nargin>=3
    it=1;  
    while it<=length(varargin)

        switch varargin{it}

            case{'plot'}
                ploti=1;
                it=it+1;
            case{'N_max'}
	        N_max=varargin{it+1};
	        it=it+2;
            case{'sq_ratio'}
                C=varargin{it+1};
                it=it+2;
            case{'dPsiP'}
                dPsiP=varargin{it+1};
                it=it+2;
            case{'dDelta'}
                Ntot_lim=varargin{it+1};
                it=it+2;
            case{'PsiP_edges'}
                PsiP_edges=varargin{it+1};
                it=it+2;
            case{'Delta_edges'}
                delta_edges=varargin{it+1};
                it=it+2;
	    case{'debug_mode'}
	        debug_mode = 1
	        it=it+1;
	    case{'gamma'}
        	gamma = varargin{it+1};
        	it = it+2;
            case{'PSINmin'}
                PSINmin = varargin{it+1};
                it = it+2;
            otherwise
                it=it+1;
        end

    end

end


while((abs(PsiP_edges(2)-PsiP_edges(1))>dPsiP)|(abs(delta_edges(2)-delta_edges(1))>ddelta))&(N<N_max)

%no need to replicate the parabola method if the parameter already converged
if(abs(PsiP_edges(2)-PsiP_edges(1))>dPsiP)
%loop initialization
%We begin by a parabola method on PsiP because it depends less on delta than the inverse
a=PsiP_edges(1);
c=PsiP_edges(2);
b=(a+c)/2;
%We need a value for delta so we are using the middle of the square
delta=(delta_edges(2)+delta_edges(1))/2;

%values of ki2 usefull for the parabola calculation
fa=ki2_mtanh_Groebner(a,delta,PsiN,ne,gamma);N=N+1;
fc=ki2_mtanh_Groebner(c,delta,PsiN,ne,gamma);N=N+1;
fb=ki2_mtanh_Groebner(b,delta,PsiN,ne,gamma);N=N+1;

%testing an alternative path
while(abs(c-a)>C*abs(delta_edges(2)-delta_edges(1)))&(abs(c-a)>dPsiP)&(N<N_max)

	if (fb-fc)/(b-c)>(fa-fb)/(a-b)

		%we do the calculation in several steps
		num=(b-a)^2*(fb-fc)-(b-c)^2*(fb-fa);
		den=(b-a)*(fb-fc)-(b-c)*(fb-fa);
		%x is the minimum of the parabola
		x=b-0.5*num/den;
		fx=ki2_mtanh_Groebner(x,delta,PsiN,ne,gamma);N=N+1;

		if debug_mode
			abxc = [a,b,x,c]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end

		if(x<a)
			c=b;
			b=a;
			a=x;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(x>c)
			a=b;
			b=c;
			c=x;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(x>b)
			c=(x+c)/2;
			a=b;
			b=x;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh_Groebner(c,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x>b)
			a=(a+b)/2;
			c=x;
			fc=fx;
			fa=ki2_mtanh_Groebner(a,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx<fb)&(x<b)
			a=(a+x)/2;
			c=b;
			b=x;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh_Groebner(a,delta,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x<b)
			c=(b+c)/2;
			a=x;
			fa=fx;
			fc=ki2_mtanh_Groebner(c,delta,PsiN,ne,gamma);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			c=b;
			b=a;
			a = b - (c-b);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh_Groebner(a,delta,PsiN,ne,gamma);N=N+1;
		else
			a=b;
			b=c;
			c = b + (b-a);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh_Groebner(c,delta,PsiN,ne,gamma);N=N+1;
		end
	end %end of the positive parabola if
	if c>2
		c=2;
	end
	if b>1.9
		b=1.9;
	end
	if a>1.8
		a=1.8;
	end
	if c<0.2
		c=0.2;
	end
	if b<0.1
		b=0.1;
	end
	if a<0
		a=0;
	end


end %end of the for loop
%the new borders are defined by the borders of the parabola
PsiP_edges=[a,c];

end %end of the outer if

%no need to replicate the parabola method if the parameter already converged
if(abs(delta_edges(2)-delta_edges(1))>ddelta)
%loop initialization
%We can now go on with the delta loop
a=delta_edges(1);
c=delta_edges(2);
b=(a+c)/2;
%We need a value for PsiP
PsiP=(PsiP_edges(2)+PsiP_edges(1))/2;

%values of ki2 usefull for the parabola
fa=ki2_mtanh_Groebner(PsiP,a,PsiN,ne,gamma);N=N+1;
fc=ki2_mtanh_Groebner(PsiP,c,PsiN,ne,gamma);N=N+1;
fb=ki2_mtanh_Groebner(PsiP,b,PsiN,ne,gamma);N=N+1;

%The loop is the exact same as above
%while(abs(c-a)>0.1*abs(PsiP_edges(2)-PsiP_edges(1)))
while(abs(c-a)>C*abs(PsiP_edges(2)-PsiP_edges(1)))&(abs(c-a)>ddelta)&(N<N_max)
	if (fb-fc)/(b-c)>(fa-fb)/(a-b)

		%we do the calculation in several steps
		num=(b-a)^2*(fb-fc)-(b-c)^2*(fb-fa);
		den=(b-a)*(fb-fc)-(b-c)*(fb-fa);
		%x is the minimum of the parabola
		x=b-0.5*num/den;
		fx=ki2_mtanh_Groebner(PsiP,x,PsiN,ne,gamma);N=N+1;
		if debug_mode
			abxc = [a,b,x,c]
			fafbfxfc = [fa,fb,fx,fc]
			keyboard
		end
		if(x<a)
			c=b;
			b=a;
			a=x;
			fc=fb;
			fb=fa;
			fa=fx;
		elseif(x>c)
			a=b;
			b=c;
			c=x;
			fa=fb;
			fb=fc;
			fc=fx;
		elseif(fx<fb)&(x>b)
			c=(x+c)/2;
			a=b;
			b=x;
			fa=fb;
			fb=fx;
			fc=ki2_mtanh_Groebner(PsiP,c,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x>b)
			a=(a+b)/2;
			c=x;
			fc=fx;
			fa=ki2_mtanh_Groebner(PsiP,a,PsiN,ne,gamma);N=N+1;
		elseif(fx<fb)&(x<b)
			a=(a+x)/2;
			c=b;
			b=x;
			fc=fb;
			fb=fx;
			fa=ki2_mtanh_Groebner(PsiP,a,PsiN,ne,gamma);N=N+1;
		elseif(fx>fb)&(x<b)
			c=(b+c)/2;
			a=x;
			fa=fx;
			fc=ki2_mtanh_Groebner(PsiP,c,PsiN,ne,gamma);N=N+1;
		end %end of the inner if
	else
		if fa<fc
			c=b;
			b=a;
			a = b - (c-b);
			fc=fb;
			fb=fa;
			fa = ki2_mtanh_Groebner(PsiP,a,PsiN,ne,gamma);N=N+1;
		else
			a=b;
			b=c;
			c = b + (b-a);
			fa=fb;
			fb=fc;
			fc = ki2_mtanh_Groebner(PsiP,c,PsiN,ne,gamma);N=N+1;
		end
	end %end of the positive parabola if




end %end of the for loop

%the new borders defined by the parabola borders
delta_edges=[a,c];
end %end of the outer if
end%end of the while loop


PsiP=mean(PsiP_edges);
delta=mean(delta_edges);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%% We need to retrive the parameter n0 and alpha%%%%%%%%%%%%%%%%%%%%%%%%

%we calculate the value of those vectors
f0k = tanh(2*(PsiP-PsiN)./delta);
f1k = ones(length(PsiN), 1);
psiknee = PsiP - delta/2;
f2k = (PsiN<psiknee).*(psiknee-PsiN);
%sums used to calculate n0 and n1 the linear parameters
sumf0f0=sum(f0k.*f0k);
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf0f1=sum(f0k.*f1k);
sumf0f2=sum(f0k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf0y=sum(f0k.*ne);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on n0 and n1 which is a matrix inversion
M0 = [sumf0f0, sumf0f1, sumf0f2];
M1 = [sumf0f1,sumf1f1, sumf1f2];
M2 = [sumf0f2, sumf1f2, sumf2f2];

M = [M0; M1; M2];
normM = mean(abs(M(:)));
M = M/normM;
V = [sumf0y; sumf1y;, sumf2y];
N = inv(M)*V./normM; A = N(1); B= N(2); C = N(3);
fk=A.*f0k+B.*f1k+C.*f2k;
ki2_value=sum((fk-ne).^2);
N=N+1;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% we prepare the result %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
R2_value = 1-ki2_value/(length(PsiN)*var(ne));
temps_tot = toc;

parameters.A = A;
parameters.B = B;
parameters.C = C;
parameters.PsiP= PsiP;
parameters.Delta = delta;
parameters.validity = (min(PsiN)<(parameters.PsiP-3*parameters.Delta))*(parameters.B>-2&parameters.B<2)+(min(PsiN)<(parameters.PsiP-parameters.Delta))+(min(PsiN)<(parameters.PsiP));
parameters.validity= parameters.validity.*(parameters.A>mean(ne)/10 & parameters.PsiP<1.3 & parameters.PsiP>0.7);
parameters.A = A*neNorm;
parameters.B = B*neNorm;
parameters.C = C*neNorm;
parameters.reg.ki2 = ki2_value;
parameters.reg.R2 = R2_value;
parameters.reg.dPsiP = abs(diff(PsiP_edges));
parameters.reg.dDelta = abs(diff(delta_edges));
parameters.reg.use = N;
parameters.reg.t_tot = temps_tot;
parameters.reg.not_conv = (N>=N_max);

%%% Recalculate the entire density profile from fit, not limited to PSIN>PSINlim
f0k = tanh(2*(PsiP-PsiNref)./delta);
f1k = ones(length(PsiNref), 1);
psiknee = PsiP - delta/2;
f2k = (PsiNref<psiknee).*(psiknee-PsiNref);
fk=A.*f0k+B.*f1k+C.*f2k;
parameters.fk = fk*neNorm;

%%%% tout ce qu'il y a après est pas important
if ploti
	figure;
	plot(PsiNref,parameters.fk,PsiNref,neref,'+')
	hold on
	plot(PSINmin+[0,0],[0,parameters.n0],'r--');
	plot(parameters.PsiP+[0,0],[0,parameters.n0./2],'k--')
	plot(parameters.PsiP-parameters.Delta+[0,0],[0,parameters.n0],'k--')
	xlim([min(PsiNref)-0.1 max(PsiNref)+0.05])
	ylim([0 max(neref)*1.1])
	hold off
end

%%%%%%%%%%%%%%%%%%%% recalculation without some parts of the profile %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ind = PsiN<PsiP-0.5*delta;
PsiN = PsiN(ind);
ne = ne(ind);
%we calculate the value of those vectors
f1k=exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));
f2k=(PsiP-PsiN).*exp((PsiP-PsiN)./delta)./(exp((PsiP-PsiN)./delta)+exp(-(PsiP-PsiN)./delta));

%sums used to calculate n0 and n1 the linear parameters
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on n0 and n1 which is a matrix inversion
n0=(sumf2f2*sumf1y-sumf2y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);
n1=(sumf1f1*sumf2y-sumf1y*sumf1f2)/(sumf1f1*sumf2f2-sumf1f2*sumf1f2);

parameters.bis.n0 = n0*neNorm;
parameters.bis.alpha = n1/n0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% ki2 calculation %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%we calculate separately the value of ki2 for mtanh with specified parameters
function [ki2_value] = ki2_mtanh_Groebner(PsiP,delta,PsiN,ne,gamma)
%we calculate the value of those vectors
ne_ = mean(ne);
wk = (ne/ne_).^gamma;

f0k=wk.*tanh(2*(PsiP-PsiN)./delta);
f1k=wk.*ones(length(PsiN), 1);
psiknee = PsiP - delta/2;
f2k=wk.*(PsiN<=psiknee).*(psiknee-PsiN);
ne=wk.*ne;

%sums used to calculate A, B and C the linear parameters
sumf0f0=sum(f0k.*f0k);
sumf1f1=sum(f1k.*f1k);
sumf2f2=sum(f2k.*f2k);
sumf0f1=sum(f0k.*f1k);
sumf0f2=sum(f0k.*f2k);
sumf1f2=sum(f1k.*f2k);
sumf0y=sum(f0k.*ne);
sumf1y=sum(f1k.*ne);
sumf2y=sum(f2k.*ne);

%calculation on A, B and C which is a matrix inversion
M0 = [sumf0f0, sumf0f1, sumf0f2];
M1 = [sumf0f1,sumf1f1, sumf1f2];
M2 = [sumf0f2, sumf1f2, sumf2f2];

M = [M0; M1; M2];
V = [sumf0y; sumf1y;, sumf2y];
N = inv(M)*V; A = N(1); B= N(2); C = N(3);

%inv(M)
fk=A.*f0k+B.*f1k+C.*f2k;
ki2_value=sum((fk-ne).^2);


%%{
function E = flux_expansion(R, Z, psi1, psi2, map, Rmap, Zmap, R0, Z0)

%%% check if psi1 and R, Z makes sense%%%
if R>max(Rmap) |R<min(Rmap) |Z>max(Zmap) |Z<min(Zmap)
	disp('R, Z out of bounds')
	E = NaN;
	return
end
%keyboard


%%%%%computing dxm/dpsi%%%%%
Rinterp = linspace(R0, max(Rmap), 50);
Zinterp = 0;
psiinterp = interp2(Rmap, Zmap, map, Rinterp, 0);

try indpsi = find_closest(psi1, psiinterp);
	catch me
	E = NaN;
	return
end
dpsi = (psiinterp(indpsi(1))-psi1)/(psiinterp(indpsi(1))-psiinterp(indpsi(2)));
Rin = dpsi*Rinterp(indpsi(1))+(1-dpsi)*Rinterp(indpsi(2));


try indpsi = find_closest(psi2, psiinterp);
	catch me
	E = NaN;
	return
end
dpsi = (psiinterp(indpsi(1))-psi2)/(psiinterp(indpsi(1))-psiinterp(indpsi(2)));
Rout = dpsi*Rinterp(indpsi(1))+(1-dpsi)*Rinterp(indpsi(2));
Emid = sqrt((Rout-Rin)^2);




%%%%% initialisation%%%%%%
Rinterp = linspace(R0, R, 50);
Zinterp = linspace(Z0, Z, length(Rinterp));
psiinterp = interp2(Rmap, Zmap, map, Rinterp, Zinterp);
try indpsi = find_closest(psi2, psiinterp);
	catch me
	E = NaN;
	return
end
dpsi = (psiinterp(indpsi(1))-psi2)/(psiinterp(indpsi(1))-psiinterp(indpsi(2)));
Rout = dpsi*Rinterp(indpsi(1))+(1-dpsi)*Rinterp(indpsi(2));
Zout = dpsi*Zinterp(indpsi(1))+(1-dpsi)*Zinterp(indpsi(2));
EX = sqrt((Rout-R)^2+(Zout-Z)^2);
E = EX/Emid;

%%}



function [ind] = find_closest(num, list)

if ~issorted(list)
	list;
	disp('not sorted list')
	
end

if num>list(end)|num<list(1)
	disp('out of bounds')
	return 
end
ind = find(list<num);
ind1 = ind(end);
ind2 = ind1+1;
ind = [ind1,ind2];

function REFL_tot = mtanh_fit(REFL_tot, param)

REFL_tot.fitne.ne = NaN(length(REFL_tot.PSIN), length(param.t));
REFL_tot.fitne.delta = NaN(length(param.t),1);
REFL_tot.fitne.alpha = NaN(length(param.t),1);
REFL_tot.fitne.n0    = NaN(length(param.t),1);
REFL_tot.fitne.psip  = NaN(length(param.t),1);
REFL_tot.fitne.validity  = NaN(length(param.t),1);
REFL_tot.fitne.err = NaN(length(param.t),1);
	for it = find(param.occ==3)'
	
		parameters = fit_mtanh_REF_parabola_02(REFL_tot.PSIN',REFL_tot.ne(:,it), 'gamma',param.gamma);
		REFL_tot.fitne.delta(it) = parameters.Delta;
		REFL_tot.fitne.alpha(it) = parameters.alpha;
		REFL_tot.fitne.n0(it)    = parameters.n0;
		REFL_tot.fitne.psip(it)  = parameters.PsiP;
		REFL_tot.fitne.validity(it)  = parameters.validity;
		REFL_tot.fitne.ne(:, it)= ne_mtanh(REFL_tot.PSIN,REFL_tot.fitne.n0(it),REFL_tot.fitne.alpha(it),REFL_tot.fitne.psip(it),REFL_tot.fitne.delta(it));
		REFL_tot.fitne.err(it) = parameters.reg.R2;

	end
function REFL_tot = mtanh_fit_groebner(REFL_tot, param)

REFL_tot.fitnegroebner.ne = NaN(length(REFL_tot.PSIN), length(param.t));
REFL_tot.fitnegroebner.A = NaN(length(param.t),1);
REFL_tot.fitnegroebner.B = NaN(length(param.t),1);
REFL_tot.fitnegroebner.delta    = NaN(length(param.t),1);
REFL_tot.fitnegroebner.err    = NaN(length(param.t),1);
REFL_tot.fitnegroebner.psip    = NaN(length(param.t),1);
REFL_tot.fitnegroebner.validity    = NaN(length(param.t),1);
	for it = find(param.occ==3)'
		
		parameters = fit_mtanh_REF_parabola_Groebner(REFL_tot.PSIN',REFL_tot.ne(:,it), 'gamma',param.gamma);
		REFL_tot.fitnegroebner.ne(:, it) = parameters.fk;
		REFL_tot.fitnegroebner.delta(it) = parameters.Delta;
		REFL_tot.fitnegroebner.A(it) = parameters.A;
		REFL_tot.fitnegroebner.B(it)    = parameters.B;
		REFL_tot.fitnegroebner.err(it)    = parameters.reg.R2;
		REFL_tot.fitnegroebner.validity(it) = parameters.validity;
		REFL_tot.fitnegroebner.psip(it)   = parameters.PsiP;
	end
	












