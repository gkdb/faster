################################################################
################################################################
''' Purpose of this code:
- retrieve data (Te, ne, q, ŝ, ...) from IDS format
- retrieve plateaus time window (defined as the one of WEST for the moment)
- compute GKDB parameters (cf GKDB documentation by Y. Camenen and the GKDB team) 
- gather means and std of plateaus for shot chosen by the user as the reducted base of WEST 

User guide:

this code uses object called DatabaseConstruction()
INPUT: 
 - campaign_name : Campaign analysed for WEST (by default: campaign_name = 'C4')
 - list_rayons_norm : radial points (x=r/a) chosen by the user to compute the code (by default: list_rayons_norm = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])
 - n_e_fevre : boolean that uses the matlab code INTREF2ne_perfect______ok_not_perfect_but_pretty_good from Fèvre internship to compute n_e (by default n_e_fevre = False)

OUTPUT:
 dictionnaire (panda format) that is constructed as:
 {shot_number : INT_NUMBER ; GKDB_param_1  = {dico_1} ; GKDB_param_2  = {dico_2}; GKDB_param_3  = {dico_3}; ... ; GKDB_param_n  = {dico_n}}
 with dico_x = {rayon_x :[plateau_1, plateau_2,..., plateau_j]} with plateau_j = [mean, std]

 How to use the code?
 Methods of the DatabaseConstruction class to construct automatically the database are:



'''
################################################################
'''Conventions adopted for this code:
 - class object : DatabaseConstruction
 - methods: Method_Of_The_Class()
 - attributs: type_attribute + attribute_has_to_be_explicitly_defined

'''
################################################################
################################################################

########################
#Librairies importation#
########################
import pandas as pd
from pandas.io.json import json_normalize
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import TextBox, Button #for the automatic rescale of histograms
from matplotlib.animation import FuncAnimation #useful to see the evolution of a parameter
import scipy.interpolate as sp #useful for interpolation between different IDS
#useful for save data  
import json as js
#useful to run matlab code and retrieve data (Fevre)
import subprocess
import io
import os
#useful for open .mat file
from scipy.io import loadmat
#useful for imas
import imas_west as iw
import statistics_west as sw
########################


class GKDatabaseConstruction():
    def __init__(self, campaign_name = 'C4', list_rayons_norm = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], fevre_computation = False, metis_computation = False):
        self.campaign_name = campaign_name
        self.list_rayons_norm = list_rayons_norm
        self.int_radii_number = len(list_rayons_norm)
        self.bool_fevre_computation = fevre_computation
        self.bool_metis_computation = metis_computation

        #dico
        self.dico_mean_std = {} #contains for one shot GKDB_param_1  = {dico_1} ; GKDB_param_2  = {dico_2}; GKDB_param_3  = {dico_3}; ... ; GKDB_param_n  = {dico_n}} (defined in introduction commentary)
        self.dico_all_data = {} #contains {shot: dico_mean_std}
        self.dico_plateau_problematic = {}  #will contain the shot and their respective plateau that have issue
    
        #Arguments that will be updated through a run
        self.int_number_of_plateau = 0
        self.int_total_number_of_plateaus = 0
        self.int_number_param_computed = 0 
        self.int_total_number_of_shot = 0
        
        #for the display
        self.int_ind_line_hist = 0
        self.int_ind_col_hist = 0
        self.int_line_hist, self.int_col_hist = 0, 0 # number of columns and lines of the histograms display
        self.fig, self.axs = None, None #main windows that display global results with histograms
        self.list_colors = ['red', 'blue', 'green', 'yellow', 'orange', 'purple', 'pink', 'brown', 'gray','cyan', 'magenta', 'gold', 'silver', 'indigo', 'violet', 'teal', 'olive', 'maroon']
        self.int_bins_hist = 250 #accuracy of histograms

        #initialisation of physical values
        self.me = 9.109382e-31 # kg
        self.qe = -1.602176e-19 # C
        self.kb = 1.380649e-23 # m2 kg s-2 K-1
        self.mu_zero = 12.56637e-7 # kg m A-2 s-2

        #bound QLKNN core: [min, max]
        rlte_qlknn_core = [0, 14]
        rlti_qlknn_core = [0, 14]
        rlne_qlknn_core = [-5, 6]
        q_qlknn_core = [0.66, 15]
        magn_shear_qlknn_core = [-1, 5]
        r_on_R_qlknn_core =  [0.03, 0.33]
        Ti_on_Te_qlknn_core = [0.25, 2.25]
        collisionality_qlknn_core = [np.log(1e-5), np.log(1)]
        z_eff_qlknn_core = [1, 3]
        beta_norm_core = [0]

        self.dico_core_bound = {"rlte" : rlte_qlknn_core,"rlte_metis" : rlte_qlknn_core, "rlti" : rlti_qlknn_core, "rlne": rlne_qlknn_core, "tite":Ti_on_Te_qlknn_core,
                                "q": q_qlknn_core, "magn_shear": magn_shear_qlknn_core, "zeff": z_eff_qlknn_core, "log_nu": collisionality_qlknn_core,
                                 "beta_core": beta_norm_core, "rR" : r_on_R_qlknn_core }
        
        #name of the parameters computed:
        # self.dico_name_parameters = {
        #             "rlte" : r'$\frac{-R*\nabla T_{e}}{T_e}$:' + '\n imas_public grad with r', 
        #             "rlne" : r'$\frac{-R*\nabla n_{e}}{n_e}$' +'\nimas_public',
        #             "rlti" : r'$\frac{-R*\nabla T_{i}}{T_i}$' +'\nimas_simu',
        #             "tite" : r'$\frac{T_i}{T_e}$' +'\nimas public + imas_simu',
        #             "q" : r'$q$' +'\nequilibrium',
        #             "magn_shear" : r'magnectic shear' +'\nequilibrium',
        #             "elongation" : r'elongation' +'\nequilibrium',
        #             "triangularity_up" : r'triangularity up' +'\neqnbruilibrium',
        #             "triangularity_low" : r'triangularity low' +'\nequilibrium',
        #             "beta_tor" : r'beta tor' +'\nequilibrium',
        #             "zeff" : r'$Z_{eff}$ resistif' +'\ncp:imas_simu',
        #             "log_nu" : r'$log(\nu^{*})$' +'\ncp:imas_simu + imas_core',
        #             "x" : 'x=r/a\nequi',
        #             "rR" : 'r/R' + '\nequi',
        #             "rlne_INTREF2ne" : r'$\frac{-R*\nabla n_{e}}{n_e}$' +'Fevre' + '\nPlateaus: ' 
        #             + str(self.int_number_of_plateau_fevre) + 'x' + str(self.int_radii_number) +
        #             ' = '+ str(self.int_number_of_plateau_fevre*self.int_radii_number),
        #             "rlne_fevre" :  r'$\frac{-R*\nabla n_{e}}{n_e}$' +'Fevre corrected' + '\nPlateaus: ' 
        #             + str(self.int_number_of_plateau_fevre) + 'x' + str(self.int_radii_number) +
        #             ' = '+ str(self.int_number_of_plateau_fevre*self.int_radii_number),
        #             "beta_tor_fevre" : r'beta tor ' +'\nFevre',
        #             "log_nu_fevre" : r'$log(\nu^{*})$' +'\nFevre'}

        self.dico_GKDB_parameters_title = {
                    "rlte" : r'$\frac{-R*\nabla T_{e}}{T_e}$:' + '\n imas_public grad with r', 
                    "q" : r'$q$' +'\nequilibrium'
                    }
        
            
        self.dico_GKDB_function_name = {
            "rlte" : "R_L_Te_Plateaus",
            "q" : "q_Plateaus"
        }

        if self.bool_fevre_computation:
            self.list_shot_fevre_failure = []
            self.bool_fevre_failure = False # = True if the shot cannot be open by the shot from Fevre matlab code
            self.list_shot_fevre_failed = []
            self.int_number_of_fevre_plateau = 0
            self.int_total_number_of_fevre_plateaus = 0
            self.dico_plateau_problematic_fevre = {}  #will contain the shot and their respective plateau that have issue with fevre matlab code
            self.dico_GKDB_parameters_title['rlne_fevre'] = r'$\frac{-R*\nabla n_{e}}{n_e}$' +'Fevre' 
            self.dico_GKDB_function_name['rlne_fevre'] = "R_L_ne_Plateaus_FEVRE"
            
        
        if self.bool_metis_computation:
            self.int_total_number_of_metis_plateaus = 0
            self.dico_GKDB_parameters_title['rlte_metis'] = r'$\frac{-R*\nabla T_{e}}{T_e}$:' + '\n imas_public grad with r'
            self.dico_GKDB_function_name['rlte_metis'] = "R_L_Te_Plateaus_METIS"


        self.Initialisation_Campaign_Stat()
    

    def on_click(self, event):
        x, y = event.x, event.y
        if event.key == 'control':
            if self.int_line_hist == 1:
                print(x,y)
                for j in range(self.int_col_hist):
                        if self.axs[j].contains(event)[0]:
                            title = self.axs[j].get_title()
                            # try:
                            self.Print_One_Hist_Color(self.dico_as_one_shot_mean_std, title)
                            # self.different_rayons_hist(self.dico_mean_std, j)
                            # self.different_rayons_en_un_hist(self.dico_mean_std, j)
                            # except:
                            #     print("pas de data à analyser ici")
            else:

                print(x,y)
                for i in range(self.int_line_hist):
                    
                    for j in range(self.int_col_hist):
                        if self.axs[i, j].contains(event)[0]:
                            print('on est bien sur hist ', i, j)
                            title = self.axs[j].get_title()
                            try:
                                self.Print_One_Hist_Color(self.dico_as_one_shot_mean_std, title)
                                #self.different_rayons_en_un_hist(self.dico_mean_std, i*self.int_col_hist+j)
                            except:
                                print("pas de data à analyser ici")

    #give the shot plateau and begin/end plateau + t_igni
    def Initialisation_Campaign_Stat(self):  
        #Reduced database retrieving 
        stats = sw.read_stats(self.campaign_name)

        #Data filtering
        statsf = stats[(stats['eq_q_95_mean_plto'] < 20)
                    & (stats['ne3_mean_plto'] > 0)
                    & (stats['P_COND_mean_plto'] > 0)
                    & (stats['eq_w_mhd_mean_plto'] > 0)
                    & ((stats['contr_nelMax_mean_plto'] / stats['neVol_mean_plto']) < 0.2) # Remove large errors in W_mhd computation
                    & ((stats['shot'] < 55625) | (stats['shot'] > 55643)) # Remove ECE wrong pulses because thunderstorm
                    & ((stats['shot'] < 56876) | (stats['shot'] > 56891))] # Remove shots without DCN laser available (interferometry)
                    # & (stats['H_98y2_mean_plto'] < 10) \
                    # & (stats['contr_bpolMax_mean_plto'] / (1E-6*stats['Ip_mean_plto']) < 0.03) # Remove large Bpol probe errors
                    # & (stats['contr_faMax_mean_plto'] != 0)] # Remove shots without polarimetry

        # Helium shots:
        helium_shots = [(55230, 55498), (55827, 55987)]

        # Remove helium pulses:
        for iishot in helium_shots:
            statsf = statsf[(stats['shot'] < iishot[0]) | (stats['shot'] > iishot[1])]

        #Stock all the shot numbers filtered previously
        self.list_campaign_shot = np.array((statsf['shot']))
        self.list_campaign_shot = np.unique(self.list_campaign_shot)
        self.list_campaign_shot = self.list_campaign_shot.astype(int)

        #Stock the plateaus time for the shot filtered previously
        self.pd_t_ini_plateaus_every_shot =statsf['tIni_plto'] #pd for panda data type
        self.pd_t_end_plateaus_every_shot =statsf['tEnd_plto']
        self.pd_t_igni_every_shot = statsf['t_ignitron']
        self.pd_plateaus_every_shot = statsf['plateau']

    #####_______________________________________________#####
    #####______________Utilitary functions______________#####
    #####_______________________________________________#####

    # automatically compute the disposition of the histograms windows by returning the number of lines and columns (only graphical application)
    #Input: number of parameters computed (int)
    #Output: lines, columns (int)
    def Columns_Lines_Cutting(self, dimension):
        square_roots = int(dimension**0.5)
        lines = square_roots
        columns = int(dimension / square_roots)
        if dimension % lines != 0:
            columns += 1

        return lines, columns

    # give the index corresponding to the closest value of a given value
    def Get_Closest_Index(self, value, input_list):
        # "Récupère l'indice le plus proche d'une valeur dans une liste stictement croissante."
        lst = np.abs(np.array(input_list)-value)
        min = np.min(lst)
        
        return np.where(lst == min)[0][0]

    # for a shot and a time vector from an IDS, give the time index of the plateaus.
    # Example: if a shot has 2 plateaus you will return [ [ind_plat_1_1,...,ind_plat_1_n]; [ind_plat_2_1,...,ind_plat_2_n] ]
    # Be careful to be in a common IDS vector time for the several values you use to compute your GKDB parameters
    def Plateaus_Index(self, shot_number, IDS_time): #donne les indices correspondants aux plateaux d'un vecteur time pour un shot donné
        tigni =  self.pd_t_igni_every_shot[shot_number][0]
        t0 = IDS_time[0]
        real_time = []
        ecart = abs(tigni-t0)
        for el in IDS_time:
            #real_time += [round(el + ecart, 2)]
            real_time += [round(el, 2)] #pas sûr de l'écart
        
        list_ind_ini_plt = []
        list_ind_fin_plt = []
        
        #self.numeros_plateaus_utiles_fevre = []
        for plat in range(self.int_number_of_plateau):
            #try:
            ind_ini = self.Get_Closest_Index(self.pd_t_ini_plateaus_every_shot[shot_number][plat],real_time)
            ind_fin = self.Get_Closest_Index(self.pd_t_end_plateaus_every_shot[shot_number][plat],real_time)
            if ind_ini == ind_fin: #Case where a plateau is outside the IDS time vector
                if self.dico_plateau_problematic.get(str(shot_number)) is not None:
                    self.dico_plateau_problematic[str(shot_number)] += [str(plat)]
                else:
                    self.dico_plateau_problematic.update({str(shot_number) : [str(plat)]})
                self.int_number_of_plateau -= 1 #On supprime un plateau
            else:
                list_ind_ini_plt += [ind_ini]
                list_ind_fin_plt += [ind_fin]
                #self.numeros_plateaus_utiles_fevre += [plat]
            # except: #if there is a problem with self.time_ini or self.time_fin : CHECK IF THIS EXCEPTION CASE OCCURS IN REALITY
            #     if self.dico_plateau_problematic.get(str(shot_number)) is not None:
            #         self.dico_plateau_problematic[str(shot_number)] += [str(plat)]
            #         self.int_number_of_plateau -= 1 #On supprime un plateau
            #     else:
            #         self.dico_plateau_problematic.update({str(shot_number) : [str(plat)]})
            #         self.int_number_of_plateau -= 1 #On supprime un plateau
        #Indices correspondant au début et fin des plateaus
        list_plateau_index = []
        
        for plat in range(self.int_number_of_plateau):
            list_plateau_index += [[ind for ind in range(list_ind_ini_plt[plat] , list_ind_fin_plt[plat])]]
        
        return list_plateau_index
    
    #give the index of radii coresponding at what the user wants
    def Radii_Normalized_Index(self, r, normalized_param):
        list_index = []
        for i in range(self.int_radii_number):
            list_index += [self.Get_Closest_Index(self.list_rayons_norm[i], r/normalized_param)] #on prend les rayons 0, 0.1, 0.2 ... 1.0

        return list_index

    # Check if it is realy useful
    def Total_Mean(self, mean_std):
        mean_tot = []
        for rayon in mean_std:
            mean_tot += rayon[0]
        return mean_tot 
    
    #Reduce the main dico containing for every shot the data as a one shot to unify the results
    def Dico_Reduction_As_One_Shot(self, dico_mean_std):
        dico_mean_std_by_radius = {}
        for GKDB_name_param, GKDB_title_hist in self.dico_GKDB_parameters_title.items():
            dico_mean_std_by_radius[GKDB_name_param] = {}
            for i in range (self.int_radii_number):
                radius_name = 'x_' + str(self.list_rayons_norm[i])
                dico_mean_std_by_radius[GKDB_name_param][radius_name] = {'mean' : [], 'std' : []}
        #tot_mean_std_rayons = [[[[], []] for j in range (self.int_radii_number)] for i in range(self.int_number_param_computed)]
        for shot, mean_std in dico_mean_std.items():
            for GKDB_name_param, by_radius_mean_std in mean_std.items():
                for i in range(self.int_radii_number):
                    radius_name = 'x_' + str(self.list_rayons_norm[i])
                    if not np.isnan(by_radius_mean_std[radius_name]['mean']).any():
                        #if self.value_filter(by_radius_mean_std[i][0]):
                        dico_mean_std_by_radius[GKDB_name_param][radius_name]['mean'] += by_radius_mean_std[radius_name]['mean'] #mean 
                        dico_mean_std_by_radius[GKDB_name_param][radius_name]['std'] += by_radius_mean_std[radius_name]['std'] #std

        return dico_mean_std_by_radius
    
    #filtering function
    def Erase_Nan(self, rho_tor, ne, time):
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
    
        
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
        return rho_tor, ne, time

    # Give the mean and std of the plateaus for a given GKDB parameters computed
    def Mean_Std_Computation(self, GKDB_plateaus):
        GKDB_mean_std = {}

        for r in range(self.int_radii_number):
            name_radius = "x_" + str(self.list_rayons_norm[r])
            GKDB_mean_std_r = {"mean" : [], "std" : []}
            for plat in GKDB_plateaus[r]:
                GKDB_mean_std_r["mean"] += [np.mean(plat)]
                GKDB_mean_std_r["std"] += [np.std(plat)]
            GKDB_mean_std[name_radius] = GKDB_mean_std_r
        
        return GKDB_mean_std
    
    
    #####_______________________________________________#####
    #####_______USEFUL PARAMETERS RETRIEVEMENT__________#####
    #####_______________________________________________#####

    def Interpolation_1D(self, time_ids1, time_ids2, lst_key_ids1):#ids1: ids de départ, ids2: ids d'arrivée
        #ex: time_equi, time_core, beta_equi => donne beta_core avec une interpolation
        r_interp = sp.interp1d(time_ids1 , lst_key_ids1, fill_value="extrapolate") #the "extrapolate" parameter is if we have a time_ids2 with larger ranger than time_ids1

        return r_interp(time_ids2)

    def Interpolation_2D(self, time_ids1, time_ids2, lst_key_ids1, lst_exemple_ids2):#ids1: ids de départ, ids2: ids d'arrivée
        #ex: time_equi, time_core, r_equi, rho_tor_core => donne r_core avec une interpolation
        dim_ids1 = np.shape(lst_key_ids1)
        dim_ids2 = np.shape(lst_exemple_ids2) #cette liste ne sert qu'à avoir des dimensions cohérentes avec l'ids 2

        lst_y_ids1 = [i for i in range(dim_ids1[0])]
        lst_y_ids2 = [i for i in range(dim_ids2[0])]

        r_interp = sp.interp2d(lst_y_ids1,time_ids1 , np.transpose(lst_key_ids1))

        return np.transpose(r_interp(lst_y_ids2, time_ids2))
    
    def Write_Script_Matlab_Fevre(self, shot):
        content = """
        
        out = INTREF2ne_perfect______ok_not_perfect_but_pretty_good(""" + str(shot) +""", 'polarimetry map', 1, 'cutcompression', 'on', 'frap', 1)
        
        ne_fevre_txt = 'ne_fevre_from_matlab.txt'
        t_txt = 't_from_matlab.txt'
        psin_txt = 'psin_from_matlab.txt'
        
        dlmwrite(ne_fevre_txt, out.REFL_tot_fluc.ne, 'delimiter', '\t', 'precision', '%.4f')
        dlmwrite(t_txt, out.param.t, 'delimiter', '\t', 'precision', '%.4f')
        dlmwrite(psin_txt, out.REFL_tot_fluc.PSIN, 'delimiter', '\t', 'precision', '%.4f')
        
        """
    
        file_name = "script_test.m"
        with io.open(file_name, 'w', encoding = 'utf-8') as file:
            file.write(content)
            
    def Reception_Data_From_Txt_Fevre(self, shot_number):
        ne_fevre_txt = 'ne_fevre_from_matlab.txt'
        t_txt = 't_from_matlab.txt'
        psin_txt = 'psin_from_matlab.txt'
        
        try:
            self.ne_fevre = np.loadtxt(ne_fevre_txt, delimiter = '\t')
            self.t_fevre = np.add(np.loadtxt(t_txt, delimiter = '\t'), self.pd_t_igni_every_shot[shot_number][0]) #vérifier l'addition de ce code
            self.psin_fevre_1d = np.loadtxt(psin_txt, delimiter = '\t')
            
            int_ind_separatrix = self.Get_Closest_Index(1, self.psin_fevre_1d)
            self.psin_fevre_1d = self.psin_fevre_1d[:int_ind_separatrix]
            self.psin_fevre = []
            for i in range(len(self.t_fevre)):
                self.psin_fevre +=[self.psin_fevre_1d]
            self.psin_fevre = np.transpose(np.array(self.psin_fevre))
            self.ne_fevre = self.ne_fevre[:int_ind_separatrix,:]
            if os.path.exists(t_txt):
                os.remove(ne_fevre_txt)
                os.remove(t_txt)
                os.remove(psin_txt)
            self.bool_fevre_failure = False
                
        except: 
            self.bool_fevre_failure = True
            print("Echec du code de Fevre")

    #reception of data from different IDS
    #Data stucture: 1D : time or r
    #               2D : r x time
    def Receive_Shot_IDS_Data(self, shot_number):
        #DATA FROM SPECIFIC IDS

        ############################################
        # 1 : core_profile_imas_public: Useful for Te, ne, t, rho_tor_norm
        core_profile_imas_public = iw.get(shot_number, 'core_profiles', 0, 0, 'imas_public', 'west')

        self.t_core = core_profile_imas_public.time

        rho_tor_norm_core = []
        rho_tor_core = []
        for i in range(np.size(self.t_core)):
            rho_tor_norm_core+= [core_profile_imas_public.profiles_1d[i].grid.rho_tor_norm] #same as the one for imas_simu (JE SUPPOSE, A VERIFIER)
            rho_tor_core+= [core_profile_imas_public.profiles_1d[i].grid.rho_tor]
        self.rho_tor_norm_core = np.transpose(np.array(rho_tor_norm_core))
        self.rho_tor_core = np.transpose(np.array(rho_tor_core))

        Te_core = []
        ne_core = []
        for i in range(np.size(self.t_core)):
            Te_core+= [core_profile_imas_public.profiles_1d[i].electrons.temperature]
            ne_core+= [core_profile_imas_public.profiles_1d[i].electrons.density]
        self.Te_core = np.transpose(np.array(Te_core))
        self.ne_core = np.transpose(np.array(ne_core))

        ############################################
        # 2 : core_profile_imas_simu : Useful pour Ti
        core_profile_imas_simu = iw.get(shot_number, 'core_profiles', 0, 0, 'imas_simulation', 'west_simu_preparation')

        Ti_core = []
        for i in range(np.size(self.t_core)):
            Ti_core+= [core_profile_imas_simu.profiles_1d[i].ion[0].temperature] 
        self.Ti_core = np.transpose(np.array(Ti_core))
        
        self.z_eff_res_core = np.transpose(core_profile_imas_simu.global_quantities.z_eff_resistive)

        ############################################
        # 3 : equilibrium: Useful for ne, q, R, r, a, rho_tor_norm
        equilibrium = iw.get(shot_number, 'equilibrium', 0, 1)

        self.t_equi = equilibrium.time

        self.rho_tor_norm_equi = np.transpose(equilibrium.profiles_1d.rho_tor_norm)

        self.r_equi = np.transpose((equilibrium.profiles_1d.r_outboard - equilibrium.profiles_1d.r_inboard)/2)
        self.R_equi = np.transpose((equilibrium.profiles_1d.r_inboard + equilibrium.profiles_1d.r_outboard)/2)

        self.elong_equi = np.transpose(equilibrium.profiles_1d.elongation) #difference with equilibrium.profiles_1d.elongation ???
        self.triang_up_equi = np.transpose(equilibrium.profiles_1d.triangularity_upper)
        self.triang_low_equi = np.transpose(equilibrium.profiles_1d.triangularity_lower)
        self.beta_norm_equi = np.transpose(equilibrium.global_quantities.beta_normal)
        self.beta_tor_equi = np.transpose(equilibrium.global_quantities.beta_tor)
        psi_equi = equilibrium.profiles_1d.psi
        self.psin_equi = np.transpose(np.divide((psi_equi - psi_equi[:,[0]]), (psi_equi[:,[100]]-psi_equi[:,[0]]))) #psi normalized
        self.q_equi = np.transpose(equilibrium.profiles_1d.q)
        self.magn_shear_equi = np.transpose(equilibrium.profiles_1d.magnetic_shear)
        self.Rmag_equi = equilibrium.vacuum_toroidal_field.r0 #a vérifier: pas utile à priori, c'est une constante
        self.Bmag_equi = equilibrium.vacuum_toroidal_field.b0
        #B_tor_equi = equilibrium.b_tor_val
        self.a_equi = equilibrium.boundary.minor_radius

        ############################################
        # 4 : reflectometer : Useful for ne, t, rho_tor_norm
        reflectometer = iw.get(shot_number, "reflectometer_profile")

        self.t_refl = reflectometer.time

        self.r_refl = reflectometer.position.r #check if it is the same definition as the r_equi above
        
        self.ne_refl = reflectometer.n_e.data
        #psi_refl = reflectometer.position.phi
        
        ############################################
        # 5 : METIS : for the moment I use it with my personal 
        if self.bool_metis_computation:
            core_profile_metis = iw.get(shot_number, "core_profiles", 1000, 0, "EV273809", "west_sim")
            #equilibrium_metis = iw.get(shot_number, "equilibrium", 1000, 0, "EV273809", "west_sim") #doesn't work from python but works with Matlab

            self.t_cp_metis = core_profile_metis.time # Théo Fonghetti has chosen 5 times by plateaus for 21 radii
            self.int_number_of_metis_plateau =  int(len(self.t_cp_metis)/5)
            self.rho_tor_norm_core_metis = core_profile_metis.profiles_1d[0].grid.rho_tor_norm

            Te_core_metis = []
            ne_core_metis = []
            r_core_metis = []
            R_core_metis = []
            a_core_metis = []
            for i in range(self.int_number_of_metis_plateau):
                Te_core_metis_plateau = []
                ne_core_metis_plateau = []
                r_core_metis_plateau = []
                R_core_metis_plateau = []
                a_core_metis_plateau = []
                for j in range(5):
                    Te_core_metis_plateau+= [core_profile_metis.profiles_1d[i*5 + j].electrons.temperature]
                    ne_core_metis_plateau+= [core_profile_metis.profiles_1d[i*5 + j].electrons.density]
                    #ex: time_equi, time_core, beta_equi => donne beta_core avec une interpolation
                    ind_rho_tor_norm = self.Get_Closest_Index(self.t_cp_metis[i*5 + j], self.t_equi)
                    r_core_metis_plateau += [self.Interpolation_1D(self.rho_tor_norm_equi[:, ind_rho_tor_norm], core_profile_metis.profiles_1d[i*5+j].grid.rho_tor_norm, self.r_equi[:, ind_rho_tor_norm])]
                    R_core_metis_plateau += [self.Interpolation_1D(self.rho_tor_norm_equi[:, ind_rho_tor_norm], core_profile_metis.profiles_1d[i*5+j].grid.rho_tor_norm, self.R_equi[:, ind_rho_tor_norm])]
                    a_core_metis_plateau += [self.a_equi[ind_rho_tor_norm]]
                Te_core_metis += [Te_core_metis_plateau]
                ne_core_metis += [ne_core_metis_plateau]
                r_core_metis += [r_core_metis_plateau]
                R_core_metis += [R_core_metis_plateau]
                a_core_metis += [a_core_metis_plateau]
            self.Te_core_metis = np.transpose(np.array(Te_core_metis))
            self.ne_core_metis = np.transpose(np.array(ne_core_metis))
            self.r_core_metis = np.transpose(np.array(r_core_metis))
            self.R_core_metis = np.transpose(np.array(R_core_metis))
            self.a_core_metis = np.transpose(np.array(a_core_metis))
        

        ############################################
        # 6 : Fevre code : matlab code case
        if self.bool_fevre_computation:
            self.Write_Script_Matlab_Fevre(shot_number)
            cmd = 'module load tools_dc;matlab -nodisplay -nosplash -nodesktop -r "run(\'script_test.m\');";exit;'
            subprocess.run(cmd, shell = True, text = True)
            self.Reception_Data_From_Txt_Fevre(shot_number)
            print('ERREUR FEVRE: ', self.bool_fevre_failure)
            if self.bool_fevre_failure:
                self.list_shot_error_fevre += [shot_number]
                self.t_fevre = np.array([])
                self.ne_fevre = np.array([])
                self.psin_fevre = np.array([])
                self.a_fevre = []
                self.r_fevre = []
                self.R_fevre = []
                self.Bmag_fevre = []
                self.Te_fevre = []
            else:      
                self.a_fevre = self.Interpolation_1D(self.t_equi, self.t_fevre, self.a_equi)
                self.r_fevre = self.Interpolation_2D(self.t_equi, self.t_fevre, self.r_equi, self.psin_fevre)
                self.R_fevre = self.Interpolation_2D(self.t_equi, self.t_fevre, self.R_equi, self.psin_fevre)
                self.Bmag_fevre  = self.Interpolation_1D(self.t_equi, self.t_fevre, self.Bmag_equi)
                self.Te_fevre = self.Interpolation_2D(self.t_core, self.t_fevre, self.Te_core, self.psin_fevre)
        

        ############################################
        # 7 : Interpolation of values on different IDS (the choice for the interpolation is to use the IDS that have the time vector with the highest accuracy)
        self.r_core = self.Interpolation_2D(self.t_equi, self.t_core, self.r_equi, self.rho_tor_norm_core)
        self.R_core = self.Interpolation_2D(self.t_equi, self.t_core, self.R_equi, self.rho_tor_norm_core)
        self.q_core = self.Interpolation_2D(self.t_equi, self.t_core, self.q_equi, self.rho_tor_norm_core)
        self.a_core = self.Interpolation_1D(self.t_equi, self.t_core, self.a_equi)     
        self.Bmag_core = self.Interpolation_1D(self.t_equi, self.t_core, self.Bmag_equi)

        ############################################
        # 8 : stock the data in a dico
        self.dico_data =  {
        # "plateau_number" : self.plateau_number,
        # "t_igni" : self.t_igni,
        # "t_ini" : self.t_ini,
        # "t_fin" : self.t_fin,
        "t_core": self.t_core.tolist(), 
        "t_equi": self.t_equi.tolist(), 
        "t_refl": self.t_refl.tolist(), 
        "rho_tor_norm_core": self.rho_tor_norm_core.tolist(), 
        "rho_tor_norm_equi": self.rho_tor_norm_equi.tolist(), 
        "rho_tor_core": self.rho_tor_core.tolist(),
        "r_equi" : self.r_equi.tolist(),
        "R_equi" : self.R_equi.tolist(),
        "r_refl" : self.r_refl.tolist(),
        "Te_core" : self.Te_core.tolist(),
        "Ti_core" : self.Ti_core.tolist(),
        "ne_core" : self.ne_core.tolist(),
        "q_equi" : self.q_equi.tolist(),
        "Rmag_equi" : self.Rmag_equi,
        "Bmag_equi" : self.Bmag_equi.tolist(),
        #"B_tor_equi" : B_tor_equi.tolist(),
        "a_equi" : self.a_equi.tolist(),
        "magn_shear_equi" : self.magn_shear_equi.tolist(),
        "elong_equi" : self.elong_equi.tolist(),
        "triang_up_equi" : self.triang_up_equi.tolist(),
        "triang_low_equi" : self.triang_low_equi.tolist(),
        "beta_norm_equi" : self.beta_norm_equi.tolist(),
        "beta_tor_equi" : self.beta_tor_equi.tolist(),
        "z_eff_res_core" : self.z_eff_res_core.tolist(),
        "ne_refl" : self.ne_refl.tolist(),
        "psin_equi" : self.psin_equi.tolist()
    
        }

        if self.bool_fevre_computation:
            self.dico_data["t_fevre"] = self.t_fevre.tolist()
            self.dico_data["ne_fevre"] = self.ne_fevre.tolist()
            self.dico_data["bool_fevre_failure"] = self.bool_fevre_failure
            self.dico_data["psin_fevre"] = self.psin_fevre.tolist()

        if self.bool_metis_computation:
            self.dico_data["t_cp_metis"] = self.t_cp_metis.tolist()
            self.dico_data["rho_tor_norm_core_metis"] = self.rho_tor_norm_core_metis.tolist()
            self.dico_data["Te_core_metis"] =self.Te_core_metis.tolist() 
            self.dico_data["ne_core_metis"] =self.ne_core_metis.tolist() 
            self.dico_data["r_core_metis"] =self.r_core_metis.tolist()  
            self.dico_data["R_core_metis"] =self.R_core_metis.tolist() 
            self.dico_data["a_core_metis"] =self.a_core_metis.tolist() 
        
    #useful for recomputing data
    def Receive_Shot_JSON_Data(self, shot_number,  dico_data):
        self.t_core =  np.array(dico_data['t_core'])
        self.t_equi =  np.array(dico_data['t_equi'])
        self.t_refl =  np.array(dico_data['t_refl'])
        self.rho_tor_norm_core =  np.array(dico_data['rho_tor_norm_core'])
        self.rho_tor_norm_equi =  np.array(dico_data['rho_tor_norm_equi'])
        self.rho_tor_core =  np.array(dico_data['rho_tor_core'])
        self.r_equi = np.array(dico_data['r_equi'] )
        self.R_equi = np.array(dico_data['R_equi'] )
        self.r_refl =  np.array(dico_data['r_refl'])
        self.Te_core = np.array(dico_data['Te_core'] )
        self.Ti_core = np.array(dico_data['Ti_core'] )
        self.ne_core = np.array(dico_data['ne_core'] )
        self.q_equi = np.array(dico_data['q_equi']) 
        self.Rmag_equi = dico_data['Rmag_equi'] 
        self.Bmag_equi = np.array(dico_data['Bmag_equi'])
        #self.B_tor_equi = np.array(dico_data['B_tor_equi'])
        self.a_equi = np.array(dico_data['a_equi']) 
        self.magn_shear_equi = np.array(dico_data['magn_shear_equi'] )
        self.elong_equi = np.array(dico_data['elong_equi'] )
        self.triang_up_equi = np.array(dico_data['triang_up_equi'] )
        self.triang_low_equi = np.array(dico_data['triang_low_equi']) 
        self.ne_refl = np.array(dico_data['ne_refl'] )
        self.beta_norm_equi = np.array(dico_data['beta_norm_equi'])
        self.beta_tor_equi = np.array(dico_data['beta_tor_equi'])
        self.z_eff_res_core = np.array(dico_data['z_eff_res_core'])
         #self.ne_DREFRAP_fit_fevre = np.array(dico_data['ne_DREFRAP_fit_fevre'])
        #self.rho_tor_norm_fevre = np.array(dico_data['rho_tor_norm_fevre'])
        self.psin_equi = np.array(dico_data['psin_equi'])
        

        self.r_core = self.Interpolation_2D(self.t_equi, self.t_core, self.r_equi, self.rho_tor_norm_core)
        self.R_core = self.Interpolation_2D(self.t_equi, self.t_core, self.R_equi, self.rho_tor_norm_core)
        self.q_core = self.Interpolation_2D(self.t_equi, self.t_core, self.q_equi, self.rho_tor_norm_core)
        self.a_core = self.Interpolation_1D(self.t_equi, self.t_core, self.a_equi)
        self.Bmag_core = self.Interpolation_1D(self.t_equi, self.t_core, self.Bmag_equi)
        
        if self.bool_fevre_computation:
            self.bool_fevre_failure = dico_data['bool_fevre_failure']
            if self.bool_fevre_failure:
                self.list_shot_fevre_failure += [shot_number]
                self.t_fevre = np.array([])
                self.ne_fevre = np.array([])
                self.psin_fevre = np.array([])
                self.a_fevre = []
                self.r_fevre = []
                self.R_fevre = []
                self.Bmag_fevre = []
                self.Te_fevre = []
            else:      
                self.ne_fevre = np.array(dico_data['ne_fevre'])
                self.t_fevre = np.array(dico_data['t_fevre'])
                self.psin_fevre = np.array(dico_data['psin_fevre']) 
                self.a_fevre = self.Interpolation_1D(self.t_equi, self.t_fevre, self.a_equi)
                self.r_fevre = self.Interpolation_2D(self.t_equi, self.t_fevre, self.r_equi, self.psin_fevre)
                self.R_fevre = self.Interpolation_2D(self.t_equi, self.t_fevre, self.R_equi, self.psin_fevre)
                self.Bmag_fevre  = self.Interpolation_1D(self.t_equi, self.t_fevre, self.Bmag_equi)
                self.Te_fevre = self.Interpolation_2D(self.t_core, self.t_fevre, self.Te_core, self.psin_fevre)

        if self.bool_metis_computation:
            self.t_cp_metis = np.array(dico_data["t_cp_metis"])
            self.int_number_of_metis_plateau =  int(len(self.t_cp_metis)/5)
            self.rho_tor_norm_core_metis = np.array(dico_data["rho_tor_norm_core_metis"])
            self.Te_core_metis = np.array(dico_data["Te_core_metis"])
            self.ne_core_metis = np.array(dico_data["ne_core_metis"])
            self.r_core_metis = np.array(dico_data["r_core_metis"])
            self.R_core_metis = np.array(dico_data["R_core_metis"])
            self.a_core_metis  = np.array(dico_data["a_core_metis"])



    #####_______________________________________________#####
    #####_______COMPUTATION OF GKDB PARAMETERS__________#####
    #####_______________________________________________#####

    #This computation of R/Lne allow the code reader to understand the basics of the parameter comuptation:
    def R_L_Te_Plateaus(self, shot_number):
        # ** 1 ** :Reception of value useful for the R/LTe computation
        time = self.t_core
        time = time.tolist()
        r = self.r_core
        Te = self.Te_core

        r, Te, time = self.Erase_Nan(r, Te, time) #In case of Nan value
        time_size = np.size(time)

        #Reception of index of plateaus
        indice_plateaus = self.Plateaus_Index(shot_number, time)

        #computation of grad Te
        grad_Te = [] 
        for i in range(time_size):
            grad_Te += [np.gradient(Te[:,i], r[:,i])]
        grad_Te = np.transpose(np.array(grad_Te))

        # ** 2 ** : computation of R/L Te only on the plateaus:
        r_l_te_plateaus = []
        for i in range(self.int_radii_number): #On ne prend que les points sélectionnées par l'utilisateur
            r_l_te_plateaus_r = []#pour le rayon i
            for plat in range(self.int_number_of_plateau):
                    r_l_te_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.Radii_Normalized_Index(r[:,indice_plateaus[plat][0]], self.a_core[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_te_plateaus_r_j += [-self.R_core[ind_rayon[i]][ind_plat]*grad_Te[ind_rayon[i]][ind_plat]/Te[ind_rayon[i]][ind_plat]]
                    r_l_te_plateaus_r += [r_l_te_plateaus_r_j]
            r_l_te_plateaus += [r_l_te_plateaus_r]

        # ** 3 ** : Mean/Std computation of each plateaus
        mean_std_r_l_te  = self.Mean_Std_Computation(r_l_te_plateaus)
        

        return mean_std_r_l_te
    
    def R_L_Te_Plateaus_METIS(self, shot_number):
        # ** 1 ** :Reception of value useful for the R/LTe computation
        time = self.t_cp_metis
        metis_plateau = self.int_number_of_metis_plateau
        
        r = self.r_core_metis
        R_core_metis = self.r_core_metis
        a = self.a_core_metis
        Te = self.Te_core_metis

        #computation of grad Te
        grad_Te = [] 
        for plts in range(metis_plateau):
            grad_Te_plateau = []
            for i in range(5):
                grad_Te_plateau += [np.gradient(Te[:,i,plts], r[:,i,plts])]
            grad_Te += [grad_Te_plateau]
        grad_Te = np.transpose(np.array(grad_Te))

        # ** 2 ** : computation of R/L Te only on the plateaus:
        r_l_te_plateaus = []
        for i in range(self.int_radii_number): #On ne prend que les points sélectionnées par l'utilisateur
            r_l_te_plateaus_r = []#pour le rayon i
            for plat in range(self.int_number_of_metis_plateau):
                    r_l_te_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.Radii_Normalized_Index(r[:,0,plat], a[0,plat]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for j in range(5):
                        r_l_te_plateaus_r_j += [-R_core_metis[ind_rayon[i],j, plat]*grad_Te[ind_rayon[i],j, plat]/Te[ind_rayon[i],j, plat]]
                    r_l_te_plateaus_r += [r_l_te_plateaus_r_j]
            r_l_te_plateaus += [r_l_te_plateaus_r]

        # ** 3 ** : Mean/Std computation of each plateaus
        mean_std_r_l_te  = self.Mean_Std_Computation(r_l_te_plateaus)

        return mean_std_r_l_te

   
    def erase_nan_zeros_multi_vec(self, rho_tor, ne, lst_vec, time): 
        ind_nan = np.where(np.isnan(ne))
        col_nan = np.unique(ind_nan[1]) #permet de ne pas avoir de doublons sur les lignes à nan
        print("col des nan: ", col_nan)
        print(len(time))
        print(np.shape(ne))
        for ind in col_nan:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            for el in lst_vec:
                if np.ndim(el) == 1:
                    el= np.delete(el, ind, 0)
                elif np.ndim(el) ==2:
                    el= np.delete(el, ind, 1)
            time = np.delete(time, ind, 0)
            col_nan -=1
            
        masque_zeros = np.any(ne == 0., axis = 0)
        col_zeros = np.where(masque_zeros)[0]
        print("col des zeros: ", col_zeros)
        for ind in col_zeros:
            rho_tor = np.delete(rho_tor, ind, 1)
            ne= np.delete(ne, ind, 1)
            for el in lst_vec:
                if np.ndim(el) == 1:
                    el= np.delete(el, ind, 0)
                elif np.ndim(el) ==2:
                    el= np.delete(el, ind, 1)
            time = np.delete(time, ind, 0)
            col_zeros -=1
        
        time_size = len(time)
        ne_copy = np.copy(ne)
        col_soustraction = 0
        print(time_size)
        print(np.shape(ne))
        for ind in range(time_size):
            if not np.all(ne_copy[:, ind]):
                rho_tor = np.delete(rho_tor, ind-col_soustraction, 1)
                ne= np.delete(ne, ind-col_soustraction, 1)
                for el in lst_vec:
                    if np.ndim(el) == 1:
                        el= np.delete(el, ind-col_soustraction, 0)
                    elif np.ndim(el) ==2:
                        el= np.delete(el, ind-col_soustraction, 1)
                time = np.delete(time, ind-col_soustraction, 0)
                col_soustraction +=1
        # plt.plot(rho_tor, ne)
        print("tailel de ne à la fin:", np.shape(ne))
        #self.verif_zeros(ne)
        return rho_tor, ne, lst_vec, time
     
                    
    def R_L_ne_Plateaus_FEVRE(self, shot_number):
        #temps du shot pour l'ids utile au calcul du param adimensionnel 
        #time = self.t_refl 
        time = self.t_fevre#c'est le temps utilisé par fèvre normalement
        
        time = time.tolist()
        
        #Rayon normalisé pour ce shot
        r= self.r_fevre
       
        #Récupération des densité électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les densités élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de denss de calcul)
        ne = self.ne_fevre
        a = self.a_fevre
        r, ne, [a], time= self.erase_nan_zeros_multi_vec(r, ne, [a] , time)
        time_size = np.size(time)

        #Récupération des indices des plateaus
        indice_plateaus = self.Plateaus_Index(shot_number, time)
        self.int_number_of_fevre_plateau = len(indice_plateaus)
        grad_ne = []
        
        
        for i in range(time_size):
            grad_ne += [np.gradient(ne[:,i], r[:,i])]
        
        grad_ne = np.transpose(np.array(grad_ne))
        
        #récupération des plateaus pour R/L Te:

        r_l_ne_plateaus = []
        for i in range(self.int_radii_number): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            r_l_ne_plateaus_r = []#pour le rayon i
            for plat in range(self.int_number_of_fevre_plateau):
                    r_l_ne_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.Radii_Normalized_Index(r[:,indice_plateaus[plat][0]], a[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        r_l_ne_plateaus_r_j += [-self.R_fevre[ind_rayon[i]][ind_plat]*grad_ne[ind_rayon[i]][ind_plat]/ne[ind_rayon[i]][ind_plat]]
                    r_l_ne_plateaus_r += [r_l_ne_plateaus_r_j]
            r_l_ne_plateaus += [r_l_ne_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_r_l_ne = self.Mean_Std_Computation(r_l_ne_plateaus)

        return mean_std_r_l_ne

 

    def q_Plateaus(self, shot_number):
        #Temps du shot
        time = self.t_equi
        time = time.tolist()
        time_size = len(time)
        
        #Rayon normalisé pour ce shot
        r = self.r_equi

        #Récupération des indices des plateaus
        indice_plateaus = self.Plateaus_Index(shot_number, time)
    
        #Récupération des températures électroniques du shot pour calculer les gradients qui ne sont pas prédéfinies dans la base des plateaus (il faut donc nous meme les faire)
        #Comme les temppératures élec existent déjà en plateaus, regarder si calculer juste le gradient de ces plateaus n'est pas suffisant  (ça ferait gagner bcp de temps de calcul)
        q_equi = self.q_equi


        #récupération des plateaus pour R/L Te:

        q_plateaus = []
        for i in range(self.int_radii_number): #On ne prend que 10 points radiaux normalisés espacés linéairement 
            q_plateaus_r = []#pour le rayon i
            for plat in range(self.int_number_of_plateau):
                    q_plateaus_r_j = [] #pour le plateau j du rayon i
                    ind_rayon = self.Radii_Normalized_Index(r[:,indice_plateaus[plat][0]], self.a_equi[indice_plateaus[plat][0]]) #on suppose que sur un plateau les coords radiales ne changent pas par
                    # rapport au premier composantes radiales au temps 0 du plateau
                    for ind_plat in indice_plateaus[plat]:
                        q_plateaus_r_j += [np.abs(q_equi[ind_rayon[i]][ind_plat])]
                    q_plateaus_r += [q_plateaus_r_j]
            q_plateaus += [q_plateaus_r]

        #Calcul des moyennes des plateaus
        mean_std_q = self.Mean_Std_Computation(q_plateaus)

        return mean_std_q

    #####_______________________________________________#####
    #####______________DISPLAY FUNCTIONS________________#####
    #####_______________________________________________#####

    def Print_All_Hist(self, dico_mean_std):
        self.int_line_hist, self.int_col_hist = self.Columns_Lines_Cutting(self.int_number_param_computed)
        self.fig, self.axs = plt.subplots(self.int_line_hist, self.int_col_hist, 
                                          gridspec_kw= {'height_ratios' : [1.5 for i in range(self.int_line_hist)]},
                                          figsize = (8,6))
        
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)
        # for GKDB_name_param, GKDB_title_hist in self.dico_name_parameters.items():
        #     self.Print_Hist_Color(dico_mean_std, GKDB_name_param, GKDB_title_hist) 

        self.dico_as_one_shot_mean_std = self.Dico_Reduction_As_One_Shot(dico_mean_std)
        for GKDB_name_param, GKDB_title_hist in self.dico_GKDB_parameters_title.items():
            self.Print_Hist_Color(self.dico_as_one_shot_mean_std, GKDB_name_param, GKDB_title_hist) 
        
        if self.int_line_hist == 1:
            self.axs[0].hist([[] for i in range(self.int_radii_number)], 
                        label= ['$x_{' + str(i) + '}$' for i in range(self.int_radii_number)],
                        color=[self.list_colors[i] for i in range(self.int_radii_number)])
            self.axs[0].hist([], color = 'r', linestyle = '--', label='QLKNN core')
            self.axs[0].hist([], color = 'b', linestyle = '--', label='QLKNN edge')
        else:
            self.axs[0,0].hist([[] for i in range(self.int_radii_number)], 
                        label= ['$x_{' + str(i) + '}$' for i in range(self.int_radii_number)],
                        color=[self.list_colors[i] for i in range(self.int_radii_number)])
            self.axs[0,0].hist([], color = 'r', linestyle = '--', label='QLKNN core')
            self.axs[0,0].hist([], color = 'b', linestyle = '--', label='QLKNN edge')
        
        self.fig.legend(loc = "upper right")
        
        titre = "Plateaus number: " + str(self.int_total_number_of_plateaus) +  " x " +  str(self.int_radii_number) + " radial points = "+ str(self.int_radii_number*self.int_total_number_of_plateaus) + "\nNumber of shot computed: " + str(self.int_total_number_of_shot) 

        if self.bool_fevre_computation:
            titre += "\nNumber of plateaus for Fevre: " + str(self.int_total_number_of_fevre_plateaus * self.int_radii_number)
        if self.bool_metis_computation:
            titre += "\nNumber of plateaus for METIS : " + str(self.int_total_number_of_metis_plateaus * self.int_radii_number)   
        # titre = "Nombre de plateaus: " + str(self.nombre_plateaus_total
        # "bool_fevre_failure" : self.bool_fevre_failure,) +  " x " +  str(self.nbr_rayon_utile) + " points radiaux = "+ str(self.nbr_rayon_utile*self.nombre_plateaus_total) + "\nNombre de shot calculés: " + str(self.nbr_shot) + " nb of shot with issues: " + str(len(self.shot_problematic)) 
        print(titre)
        self.fig.suptitle(titre)
        plt.subplots_adjust(hspace= 0.5)

        plt.show() 

    def Print_Hist_Color(self, mean_std, str_name_GKDB_param, nom_hist):
        # try:
        tot_mean = []
        tot_std = []
        for radius in range(self.int_radii_number):
            radius_name = 'x_' + str(self.list_rayons_norm[radius])
            tot_mean += mean_std[str_name_GKDB_param][radius_name]['mean']
            tot_std += mean_std[str_name_GKDB_param][radius_name]['std']
        
        max_mean = np.max(tot_mean)
        min_mean = np.min(tot_mean)
    
        if self.int_line_hist == 1:
            ax = self.axs[self.int_ind_col_hist]
        else:
            ax = self.axs[self.int_ind_line_hist , self.int_ind_col_hist]
        
        ax.hist([mean_std[str_name_GKDB_param]['x_' + str(self.list_rayons_norm[radius])]['mean']  for radius in range(self.int_radii_number)], stacked = True, 
                histtype = 'barstacked', range = (min_mean, max_mean), bins= self.int_bins_hist, alpha= 1., 
                #label= ['$x_{' + str(i) + '}$' for i in range(self.nbr_rayon_utile)], 
                color=[self.list_colors[i] for i in range(self.int_radii_number)])

        #bound printing:
        self.Print_Bound_QLKNN(ax, str_name_GKDB_param)

            #plt.hist(mean + np.abs(std), bins= 50, alpha= 1., label= '$mean+|std|$', color='pink')
        ax.set_title(nom_hist)
            #ax.legend(loc='upper right')
        # except:
        #     print("pas de valeur pour le param adim: " + str_name_GKDB_param)
        if self.int_ind_col_hist != self.int_col_hist-1:
            self.int_ind_col_hist += 1
        else:
            self.int_ind_line_hist += 1
            self.int_ind_col_hist = 0

    def Print_One_Hist_Color(self, dico_mean_std, hist_title):
        str_name_GKDB_param  = ''
        for GKDB_param, GKDB_hist_title in self.dico_GKDB_parameters_title.items():
            if GKDB_hist_title == hist_title:
                str_name_GKDB_param = GKDB_param
        tot_mean = []
        tot_std = []
        for radius in range(self.int_radii_number):
            radius_name = 'x_' + str(self.list_rayons_norm[radius])
            tot_mean += self.dico_as_one_shot_mean_std[str_name_GKDB_param][radius_name]['mean']
            tot_std += self.dico_as_one_shot_mean_std[str_name_GKDB_param][radius_name]['std']
        max_mean = np.max(tot_mean)
        min_mean = np.min(tot_mean)
        fig, ax = plt.subplots()
        #plot les hist en stackant les datas de chaque rayons
        ax.hist([self.dico_as_one_shot_mean_std[str_name_GKDB_param]['x_' + str(self.list_rayons_norm[i])]['mean'] for i in range(self.int_radii_number)], stacked = True, histtype = 'barstacked', range = (min_mean, max_mean), bins= self.int_bins_hist, alpha= 1., label= ['$x_{' + str(i) + '}$' for i in range(self.int_radii_number)], color=[self.list_colors[i] for i in range(self.int_radii_number)], edgecolor = "k")
        
        #bound printing:
        self.Print_Bound_QLKNN(ax, str_name_GKDB_param)

        # ax.xlabel(self.name_hist[int_adimensional_parameter])
        # ax.ylabel(r'# account')
        fig.suptitle(hist_title)

        ax.legend(loc='upper right')
        # ax.tight_layout() #faudrait trouver l'équivalent de plt.tight_layout avec ax

        def Update_Histograms(event):
            if event.key == 'r':
                x_min, x_max = ax.viewLim.intervalx
                [x_min, x_max] = self.dico_core_bound[str_name_GKDB_param]
                data_filtered = []
                for radius in range(self.int_radii_number):
                    data =  [x for x in self.dico_as_one_shot_mean_std[str_name_GKDB_param]['x_' + str(self.list_rayons_norm[radius])]['mean'] if x_min <= x <= x_max]
                    data_filtered += [data]
                    #print(len(self.dico_as_one_shot_mean_std[str_name_GKDB_param]['x_' + str(self.list_rayons_norm[radius])]['mean']), len(data))
                
                print(x_min, x_max)
                ax.clear()
                ax.hist(data_filtered, stacked = True, histtype = 'barstacked', range = (x_min, x_max), bins= self.int_bins_hist, alpha= 1., label= ['$x_{' + str(i) + '}$' for i in range(self.int_radii_number)], color=[self.list_colors[i] for i in range(self.int_radii_number)], edgecolor = "k")
                self.Print_Bound_QLKNN(ax, str_name_GKDB_param)
                plt.draw()

        self.Print_Bound_QLKNN(ax, str_name_GKDB_param)
        fig.canvas.mpl_connect('key_press_event', Update_Histograms)
        
        plt.show()  

    def Print_Bound_QLKNN(self, ax, str_GKDB_name_parameter):
        if str_GKDB_name_parameter in self.dico_core_bound:
            core_bound = self.dico_core_bound[str_GKDB_name_parameter]
            if np.size(core_bound) == 2:
                min_core_bound = core_bound[0]
                max_core_bound = core_bound[1]

                ax.axvline(min_core_bound, color = 'r', linestyle = '--')
                ax.axvline(max_core_bound, color = 'r', linestyle = '--')
            else: #shape == 1
                ax.axvline(core_bound[0], color = 'r', linestyle = '--')

            # edge_bound = self.dico_edge_bound[str_GKDB_name_parameter]
            # if np.size(edge_bound) == 2:
            #     min_edge_bound = edge_bound[0]
            #     max_edge_bound = edge_bound[1]
                
            #     ax.axvline(min_edge_bound, color = 'b', linestyle = '--')
            #     ax.axvline(max_edge_bound, color = 'b', linestyle = '--')
            # else: #shape == 1
            #     ax.axvline(edge_bound[0], color = 'b', linestyle = '--')

        
    #####_______________________________________________#####
    #####_________MAIN FUNCTIONS OF THE CLASS___________#####
    #####_______________________________________________#####

    def Data_Computation(self, shot_number):
        mean_std = {}
        self.int_number_of_plateau = np.size(self.pd_plateaus_every_shot[shot_number])
        self.int_total_number_of_plateaus += self.int_number_of_plateau
            
        for GKDB_param, GKDB_function in self.dico_GKDB_function_name.items():
            mean_std[GKDB_param] = getattr(self, GKDB_function)(shot_number)
            
            
        print("Number of plateau for this shot:", self.int_number_of_plateau)
        if self.bool_fevre_computation:
            self.int_total_number_of_fevre_plateaus += self.int_number_of_fevre_plateau
            print("Number of FEVRE plateau for this shot:", self.int_number_of_fevre_plateau)
        if self.bool_metis_computation:
            self.int_total_number_of_metis_plateaus += self.int_number_of_metis_plateau
            print("Number of METIS plateau for this shot:", self.int_number_of_metis_plateau)

        self.int_number_param_computed = len(mean_std)
        

        return mean_std
    
    def List_Shot_Hist(self, list_shot,  display = False, save_data = False, name_json_file = "list_shot_data"):
        list_shot_problematic = []
        self.int_total_number_of_shot = len(list_shot)
        int_shot_treatment = 1
        for shot in list_shot:
            print("shot:" + str(int_shot_treatment) + "/" + str(self.int_total_number_of_shot))
            int_shot_treatment += 1
            try:
                self.Receive_Shot_IDS_Data(shot)
                mean_std = self.Data_Computation(shot)
                self.dico_mean_std.update({str(shot) : mean_std})
                self.dico_all_data.update({str(shot) : self.dico_data}) #useful for the full data save
                
            except:
                list_shot_problematic += [shot]
        
        print("Shot problematiques: ", list_shot_problematic)
        print("Number total of plateau: ", self.int_total_number_of_plateaus)
        if self.bool_fevre_computation:
            print("Number total of Fèvre plateau: ", self.int_total_number_of_fevre_plateaus)

        if self.bool_metis_computation:
            print("Number total of METIS plateau: ", self.int_total_number_of_metis_plateaus)

        #print("nombre de shot pb:", len(list_shot_problematic))
        # print("Shot problematiques de Fèvre: ", self.tableau_bool_fevre_failure)
        # print("shot Te pb: ", self.shot_Te_pb)
            
        pd_mean_std = json_normalize(self.dico_mean_std, sep='_')
        

        if display:
            self.Print_All_Hist(self.dico_mean_std)

        if save_data:
            with open(name_json_file, 'w') as f:
                js.dump(self.dico_all_data, f)
                print("ALL DATA SAVED")
                
        return pd_mean_std

    def Random_List_Shot_Hist(self, number_of_shot, display = False, save_data = False, name_json_file = "random_list_shot_data"):
        shot_list = []
        for i in range(number_of_shot):
            shot_list += [self.list_campaign_shot[np.random.randint(0, np.size(self.list_campaign_shot))]]
        return self.List_Shot_Hist(shot_list, display = display, save_data = save_data, name_json_file = name_json_file)

    def Metis_Shot_Hist(self, name_mat_file, display = False, save_data= False, name_json_file = "metis_shot_data"):
        data = loadmat(name_mat_file)
        list_shot = np.unique(data['out']['shots'][0][0][0])
        self.List_Shot_Hist(list_shot, display=display, save_data=save_data, name_json_file=name_json_file)

    def One_Shot_Hist(self, shot_number, display = False, save_data = False, name_json_file = "one_shot_data"):
        print("Shot Number Treatment: ", shot_number)
        self.int_total_number_of_shot += 1
        
        self.Receive_Shot_IDS_Data(shot_number)
        mean_std = self.Data_Computation(shot_number)
        self.dico_mean_std.update({str(shot_number) : mean_std})
        pd_mean_std = json_normalize(self.dico_mean_std, sep='_')
        #pd_mean_std = pd.DataFrame(self.dico_mean_std)
        #print(self.plateau_problematic)
        self.dico_all_data = {str(shot_number) : self.dico_data}

        if display:
            self.Print_All_Hist(self.dico_mean_std)

        if save_data:
            with open(name_json_file, 'w') as f:
                js.dump(self.dico_all_data, f)
                print("ALL DATA SAVED")

        print(self.dico_mean_std)
        print(pd_mean_std)

        return pd_mean_std

    def Saved_Hist(self, display = False, name_saved_file = None):
        with open(name_saved_file) as f:
            self.dico_all_data = js.load(f)

        int_shot_treatment = 1
        self.int_total_number_of_shot = len(self.dico_all_data)

        for shot, dico_data in self.dico_all_data.items():
            print("Shot traité:", int_shot_treatment, "/", self.int_total_number_of_shot)
            int_shot_treatment +=1
            self.Receive_Shot_JSON_Data(shot, dico_data)
            mean_std = self.Data_Computation(int(shot))
            self.dico_mean_std.update({str(shot) : mean_std})

        if display:
            self.Print_All_Hist(self.dico_mean_std)

        return self.dico_mean_std

    
########################################################
#____________________MAIN____________________________
########################################################

shot_list = [55107, 54368, 54562]
name_mat_file = 'Fonghetti_metis_shot.mat'
Hist = GKDatabaseConstruction(campaign_name='C4', list_rayons_norm=[0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8], fevre_computation=False, metis_computation=False)
#Hist.Metis_Shot_Hist(name_mat_file = "Fonghetti_metis_shot.mat", display=True, save_data=True, name_json_file="METIS_THEO_WITHOUT_FEVRE")
panda_mean_std = Hist.One_Shot_Hist(54524, display=True, save_data=True, name_json_file="shot_test")
#panda_mean_std = Hist.List_Shot_Hist(shot_list, display=True, save_data=True, name_json_file= "TEST_JSON_FILE")
#Hist.Saved_Hist(display=True, name_saved_file="shot_test")