# FASTER

## Description:

This is the home of the FASTER project aiming at delivering fast and accurate transport models for tokamak plasmas.  
Here, 'fast' means 'real-time capable' and 'accurate' means 'comparable to non-linear flux-tube delta-f gyrokinetic simulations'. 

Stay tuned, more to come...




## Useful links :



## Project structure: 

```bash
- project_name/
    ├── config/        # Main configuration folder
    │   ├── config.yaml
    │   └── logging.yaml
    ├── data/           # Save all the data phases
    │   ├── raw/
    │   ├── intermediate/ 
    │   ├── processed/
    │   └── ...
    ├── docs/       # Project documentation 
    ├── models/     
    ├── notebooks/  # Store jupyter notebook
    ├── reports/
    ├── scripts/
    ├── src/         # Store scripts
    ├── tests/       # Store tests
    ├── requirements.txt
    ├── LICENSE
    └── README.md
```
